//
//  PackagesVC.h
//  HandasahUser
//
//  Created by Apple on 5/18/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackagesVC : UIViewController
- (IBAction)singleBtnAction:(id)sender;
- (IBAction)packageBtnAction:(id)sender;
- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn;

@property (weak, nonatomic) IBOutlet UIButton *singleBtn;
@property (weak, nonatomic) IBOutlet UIButton *packageBtn;

@property NSString *packString;
@end
