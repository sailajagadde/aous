//
//  FirstVisitViewController.m
//  HandasahUser
//
//  Created by Apple on 5/13/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "FirstVisitViewController.h"
#import "AdressViewController.h"
#import <FSCalendar/FSCalendar.h>

@interface FirstVisitViewController (){
    
    UIDatePicker *datePicker;
    UIToolbar *toolbar;
    
    NSString *dateFormatString;
    NSString *todayDate;
    
    UITextField * current_TF;
    
    NSString *selDateStr;
    NSMutableArray * newFinalArray;
    NSMutableArray *visitingArray;
    
}

@end
//62 180 213
@implementation FirstVisitViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
//    self.calender_view.delegate = self;
//    self.calender_view.dataSource = self;
    
   
    [self loadAttributes];
    // Do any additional setup after loading the view.
}


-(void)loadAttributes{

    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Single Visit"];
    self.desc_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Description"];
    self.time_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Please Enter Time"];
    [self.submit_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"REQUEST"] forState:UIControlStateNormal];
    
    _PleaseEnterDescLabel.text = [[SharedClass sharedInstance] languageSelectedString:@"Please Enter Description"];
    
    self.desc_TV.layer.borderColor = [UIColor grayColor].CGColor;
    self.desc_TV.layer.borderWidth = 1.0;
    self.desc_TV.layer.cornerRadius = 2;
    
    
    UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_wh"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    back.tintColor = [UIColor whiteColor];
    [self.navigationItem setLeftBarButtonItem:back];
    [self dateAndTime];
   
    self.heightcons.constant = 300;
    FSCalendarScope selectedScope = FSCalendarScopeWeek;
    [self.calender_view setScope:selectedScope animated:YES];
    
    //self.desc_TV.text = self.descStr;
    
}

-(void)dismissKeyboard{
    
    [self.desc_TV resignFirstResponder];
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    self.PleaseEnterDescLabel.hidden = YES;
    return YES;
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    self.heightcons.constant = CGRectGetHeight(bounds);
    // Do other updates here
    [self.view layoutIfNeeded];
}

-(void)dateAndTime{
    
    datePicker = [[UIDatePicker alloc]init];
    
    toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Done"] style: UIBarButtonItemStyleDone target: self action: @selector(doneBtn)];
    [doneButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style: UIBarButtonItemStyleDone target: self action: @selector(cancelBtnClicked)];
    [cancelButton setTintColor:[UIColor whiteColor]];
    
    toolbar.items = [NSArray arrayWithObjects:cancelButton,flexibleSpace,doneButton, nil];
    
}

-(void)doneBtn{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatString];
    
     if (current_TF == self.time_TF){
        
        NSString *stringForTime = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:datePicker.date]];
        if([stringForTime isEqualToString:@"00:00"]){
            
            self.time_TF.text = @"12:00";
        }
        else{
            self.time_TF.text = [dateFormatter stringFromDate:datePicker.date];
        }
        
    }
    
    [self.time_TF resignFirstResponder];
    
}

-(void)cancelBtnClicked{
    [self.time_TF resignFirstResponder];
}

-(void)dismissKeyboardMethod{
    
    [self.time_TF resignFirstResponder];
    
}
//Textfield delegates


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == self.time_TF){
        
        datePicker.datePickerMode = UIDatePickerModeTime;
        dateFormatString = @"hh:mm a";
        textField.inputView = datePicker;
        [textField setInputAccessoryView:toolbar];
        
        //self.scroll_view.scrollEnabled = YES;
        
    }
    current_TF = textField;
    return YES;
}



- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatString = @"YYYY-MM-dd";
    [dateFormatter setDateFormat:dateFormatString];
    
    NSLog(@"selected date %@",date);
    selDateStr = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    NSLog(@"seledatestr is %@",selDateStr);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btn_Back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submit_btnAction:(id)sender {
    
    //visits = [ {  visitingtime ,visitingdate }]
    
    newFinalArray = [NSMutableArray new];
    
    NSMutableDictionary*detailsDict=[[NSMutableDictionary alloc]init];
    [detailsDict setObject:[NSString stringWithFormat:@"%@",selDateStr] forKey:@"visitingdate"];
    [detailsDict setObject:[NSString stringWithFormat:@"%@",self.time_TF.text] forKey:@"visitingtime"];
    //[detailsDict setObject:[NSString stringWithFormat:@"%@",self.desc_TV.text] forKey:@"description"];
    
    [newFinalArray addObject:detailsDict];
    NSLog(@"my final array is %@",newFinalArray);
    
    if([selDateStr isEqualToString:@""]||([self.time_TF.text isEqualToString:@""])){
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Select Date and Time"] OnViewController:self completion:nil];
    }
    else{
        AdressViewController *adress = [self.storyboard instantiateViewControllerWithIdentifier:@"AdressViewController"];
        adress.descStr = self.desc_TV.text;
        adress.newfinalArr=newFinalArray;
        adress.serIdStr=self.serIDStr;
        [self.navigationController pushViewController:adress animated:YES];
    }
    
    
    
    
}
@end


/*
 visit backup
 
 //
 //  VisitFirstVC.m
 //  HandasahUser
 //
 //  Created by MuraliKrishna on 07/08/18.
 //  Copyright © 2018 volivesolutions. All rights reserved.
 //
 
 #import "VisitFirstVC.h"
 #import "AdressViewController.h"
 #import <FSCalendar/FSCalendar.h>
 
 @interface VisitFirstVC (){
 UIDatePicker *datePicker;
 UIToolbar *toolbar;
 
 NSString *dateFormatString;
 NSString *todayDate;
 
 UITextField * current_TF;
 
 NSString *selDateStr;
 NSMutableArray * newFinalArray;
 NSMutableArray *visitingArray;
 }
 
 @end
 
 @implementation VisitFirstVC
 
 - (void)viewDidLoad {
 [super viewDidLoad];
 
 // [self loadAttributes];
 
 self.heightConstraint.constant =200;
 FSCalendarScope selectedScope = FSCalendarScopeWeek;
 [self.calView setScope:selectedScope animated:YES];
 
 self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Visit"];
 self.descLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Description"];
 self.time_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Please Choose Time"];
 [self.request_Btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"REQUEST"] forState:UIControlStateNormal];
 
 self.desc_TV.layer.borderColor = [UIColor grayColor].CGColor;
 self.desc_TV.layer.borderWidth = 1.0;
 self.desc_TV.layer.cornerRadius = 2;
 
 self.desc_TV.returnKeyType = UIReturnKeyDone;
 
 datePicker = [[UIDatePicker alloc]init];
 toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 44)];
 toolbar.barStyle = UIBarStyleBlackOpaque;
 toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
 
 UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Done"] style: UIBarButtonItemStyleDone target: self action: @selector(doneBtn)];
 [doneButton setTintColor:[UIColor whiteColor]];
 UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
 UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style: UIBarButtonItemStyleDone target: self action: @selector(cancelBtnClicked)];
 [cancelButton setTintColor:[UIColor whiteColor]];
 toolbar.items = [NSArray arrayWithObjects:cancelButton,flexibleSpace,doneButton, nil];
 
 
 
 NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
 [dateFormatter setDateFormat:@"YYYY-MM-dd"];
 // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
 NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
 selDateStr = [dateFormatter stringFromDate:[NSDate date]];
 NSLog(@"sel %@",selDateStr);
 //    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
 //    [self. addGestureRecognizer:gesture];
 
 }
 
 - (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
 
 
 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
 dateFormatString = @"YYYY-MM-dd";
 [dateFormatter setDateFormat:dateFormatString];
 
 NSLog(@"selected date %@",date);
 selDateStr = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
 //[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]]? [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]]:@"";
 //
 NSLog(@"seledatestr is %@",selDateStr);
 
 
 }
 
 - (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
 {
 self.heightConstraint.constant = CGRectGetHeight(bounds);
 // Do other updates here
 [self.view layoutIfNeeded];
 }
 
 -(void)loadAttributes{
 
 UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
 [self.view addGestureRecognizer:gesture];
 
 self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Visit"];
 self.descLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Description"];
 self.time_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Please Enter Time"];
 [self.request_Btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"REQUEST"] forState:UIControlStateNormal];
 
 //_PleaseEnterDescLabel.text = [[SharedClass sharedInstance] languageSelectedString:@"Please Enter Description"];
 
 self.desc_TV.layer.borderColor = [UIColor grayColor].CGColor;
 self.desc_TV.layer.borderWidth = 1.0;
 self.desc_TV.layer.cornerRadius = 2;
 
 
 UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_wh"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
 back.tintColor = [UIColor whiteColor];
 [self.navigationItem setLeftBarButtonItem:back];
 [self dateAndTime];
 
 
 //self.desc_TV.text = self.descStr;
 
 }
 
 -(void)dismissKeyboard{
 
 [self.desc_TV resignFirstResponder];
 
 }
 
 -(void)backAction
 {
 [self.navigationController popViewControllerAnimated:YES];
 }
 -(void)dateAndTime{
 
 
 
 }
 
 -(void)doneBtn{
 
 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
 [dateFormatter setDateFormat:dateFormatString];
 
 if (current_TF == self.time_TF){
 
 NSString *stringForTime = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:datePicker.date]];
 if([stringForTime isEqualToString:@"00:00"]){
 
 self.time_TF.text = @"12:00";
 }
 else{
 self.time_TF.text = [dateFormatter stringFromDate:datePicker.date];
 }
 
 }
 
 [self.time_TF resignFirstResponder];
 
 }
 
 -(void)cancelBtnClicked{
 [self.time_TF resignFirstResponder];
 }
 
 -(void)dismissKeyboardMethod{
 
 [self.time_TF resignFirstResponder];
 
 }
 //Textfield delegates
 
 
 -(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
 
 if(textField == self.time_TF){
 
 
 //[self dateAndTime];
 datePicker.datePickerMode = UIDatePickerModeTime;
 dateFormatString = @"hh:mm a";
 textField.inputView = datePicker;
 [textField setInputAccessoryView:toolbar];
 
 //self.scroll_view.scrollEnabled = YES;
 
 }
 current_TF = textField;
 return YES;
 }
 
 
 
 - (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
 
 if([text isEqualToString:@"\n"]) {
 [textView resignFirstResponder];
 return NO;
 }
 
 return YES;
 }
 
 - (void)didReceiveMemoryWarning {
 [super didReceiveMemoryWarning];
 // Dispose of any resources that can be recreated.
 }
 
 

- (IBAction)reqBtnAction:(id)sender {
    //visits = [ {  visitingtime ,visitingdate }]
    
    
    
    if(self.time_TF.text.length<1){
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Choose Time"] OnViewController:self completion:nil];
    }else if(self.desc_TV.text.length <1){
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Enter Description"] OnViewController:self completion:nil];
    }
    else{
        
        
        //newFinalArray = [NSMutableArray new];
        
        // NSMutableDictionary*detailsDict=[[NSMutableDictionary alloc]init];
        //[detailsDict setObject:[NSString stringWithFormat:@"%@",selDateStr] forKey:@"requested_date"];
        //[detailsDict setObject:[NSString stringWithFormat:@"%@",self.time_TF.text] forKey:@"requested_time"];
        //[detailsDict setObject:[NSString stringWithFormat:@"%@",self.desc_TV.text] forKey:@"description"];
        
        //[newFinalArray addObject:detailsDict];
        //NSLog(@"my final array is %@",newFinalArray);
        
        AdressViewController *adress = [self.storyboard instantiateViewControllerWithIdentifier:@"AdressViewController"];
        adress.descStr = self.desc_TV.text;
        adress.reqDateStr=selDateStr;
        adress.reqTimeStr=self.time_TF.text;
        
        //adress.newfinalArr=newFinalArray;
        
        adress.serIdStr=self.serIDStr;
        [self.navigationController pushViewController:adress animated:YES];
    }
    
    
    
    
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end

 
 
 */




