//
//  ServiceProviderListVC.m
//  HandasahUser
//
//  Created by MuraliKrishna on 21/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "ServiceProviderListVC.h"
#import "SPListCell.h"
#import "SPDetailsVC.h"

@interface ServiceProviderListVC (){
    SPListCell *cell;
    
    NSMutableArray *serviceNamesArray;
    NSMutableArray *serviceImagesArray;
    NSMutableArray *serviceIdArray;
    NSMutableArray *serviceCostArray;
    NSMutableArray *serviceDescArray;
    NSMutableArray *emailArray;
    NSMutableArray *mobileArray;
    NSMutableArray *SpUserIdArray;
    NSMutableArray *userNameArray;
}

@end

@implementation ServiceProviderListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [[SharedClass sharedInstance] languageSelectedString:@"ServiceProviders"];
    [self SPList];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return serviceNamesArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    SPListCell *cell = [self.listTV dequeueReusableCellWithIdentifier:@"SPListCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[SPListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SPListCell"];
    }
    
    
//    [cell.makePayment_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"MAKE PAYMENT"] forState:UIControlStateNormal];
//    [cell.viewDetails_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"VIEW DETAILS"] forState:UIControlStateNormal];
//
    cell.serviceStatic.text = [[SharedClass sharedInstance] languageSelectedString:@"Service"];
    cell.costStatic.text = [[SharedClass sharedInstance] languageSelectedString:@"Cost"];
    
    cell.emailStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Email :"];
    cell.mobileStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Mobile Number :"];
    
    cell.emailLabel.text = [emailArray objectAtIndex:indexPath.row];
    cell.mobileLabel.text = [NSString stringWithFormat:@"%@",[mobileArray objectAtIndex:indexPath.row]];
    
//    cell.dateStatic_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Date & Time"];
//    cell.statusStatic_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Status"];
//
//    NSString *reqStatusStr = [NSString stringWithFormat:@"%@",[reqStatusArray objectAtIndex:indexPath.row]];
//
//    if ([reqStatusStr isEqualToString:@"0"]) {
//        cell.status_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Waiting"];
//        cell.makePayment_btn.hidden = YES;
//
//
//    }
//    else if ([reqStatusStr isEqualToString:@"1"]){
//        cell.status_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Accepted"];
//        cell.makePayment_btn.hidden = NO;
//    }
//    else if ([reqStatusStr isEqualToString:@"3"]){
//        cell.status_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Completed"];
//        cell.makePayment_btn.hidden = YES;
//    }
    cell.nameLabel.text = [userNameArray objectAtIndex:indexPath.row];
    cell.costLabel.text = [NSString stringWithFormat:@"%@%@",@": ",[serviceCostArray objectAtIndex:indexPath.row]];
    //cell.service_label.text = [NSString stringWithFormat:@"%@",[serviceNameArray objectAtIndex:indexPath.row]];


    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];

    if ([str isEqualToString:@"2"]) {
        cell.serviceNameLabel.text = [NSString stringWithFormat:@"%@%@",[serviceNamesArray objectAtIndex:indexPath.row],@" :"];
    } else{

        cell.serviceNameLabel.text = [NSString stringWithFormat:@"%@%@",@": ",[serviceNamesArray objectAtIndex:indexPath.row]];
    }
    
    cell.backView.layer.cornerRadius = 10;
    cell.backView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.backView.clipsToBounds = YES;

    [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:[serviceImagesArray objectAtIndex:indexPath.row]]];

    cell.profileImage.layer.cornerRadius = cell.profileImage.frame.size.width/2.0;
    cell.profileImage.clipsToBounds = YES;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 165;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
        SPDetailsVC * forVisit = [self.storyboard instantiateViewControllerWithIdentifier:@"SPDetailsVC"];
        forVisit.spUserIDStr =[SpUserIdArray objectAtIndex:indexPath.row];
    
     forVisit.imageStr=[serviceImagesArray objectAtIndex:indexPath.row];
     forVisit.nameStr=[userNameArray objectAtIndex:indexPath.row];
     forVisit.emailStr=[emailArray objectAtIndex:indexPath.row];
     forVisit.mobileStr=[mobileArray objectAtIndex:indexPath.row];
     forVisit.sernameStr=[serviceNamesArray objectAtIndex:indexPath.row];
     forVisit.costStr=[serviceCostArray objectAtIndex:indexPath.row];
    
    
     [[NSUserDefaults standardUserDefaults]setObject:[SpUserIdArray objectAtIndex:indexPath.row] forKey:@"SPUID"];
      //[[NSUserDefaults standardUserDefaults]objectForKey:@"serID"];
        [self.navigationController pushViewController:forVisit animated:YES];
    
}
-(void)SPList{
    
    // http://volive.in/aous/user_services/choose_sp
    //lang, service_id
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
    } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"serID"] forKey:@"service_id"];
    
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"choose_sp" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            
            NSArray * servicesArray = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"data"]];
                                                       //objectForKey:@"services"]];
            
            serviceNamesArray = [[NSMutableArray alloc]init];
            serviceImagesArray = [[NSMutableArray alloc]init];
            SpUserIdArray = [[NSMutableArray alloc]init];
            serviceCostArray = [[NSMutableArray alloc]init];
            serviceDescArray = [[NSMutableArray alloc]init];
            emailArray = [[NSMutableArray alloc]init];
            mobileArray = [[NSMutableArray alloc]init];
            userNameArray = [[NSMutableArray alloc]init];
            for (int i = 0; i<servicesArray.count; i++) {
                
                [serviceNamesArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"service_name"]];
                [serviceImagesArray addObject:[NSString stringWithFormat:@"%s%@",basePath,[[servicesArray objectAtIndex:i]objectForKey:@"image"]]];
                //[serviceIdArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"service_id"]];
                [serviceCostArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"service_cost"]];
                [serviceDescArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"service_description"]];
                [emailArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"email"]];
                [mobileArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"phone_number"]];
                [userNameArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"username"]];
                [SpUserIdArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"sp_user_id"]];
            }
            
            

            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.listTV reloadData];
                [SVProgressHUD dismiss];
            });
            
        }
        
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
            });
            
        }
        
    }];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
