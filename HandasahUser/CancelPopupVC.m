//
//  CancelPopupVC.m
//  HandasahUser
//
//  Created by MuraliKrishna on 23/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "CancelPopupVC.h"
#import "RESideMenu.h"
#import "HomeViewController.h"

@interface CancelPopupVC ()<RESideMenuDelegate>{
    RESideMenu * sideMenu;
}

@end

@implementation CancelPopupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    self.cancelStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Cancel"];
    
    self.reasonForStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Enter Reason For Cancel"];
    [self.closeBtn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"CLOSE"] forState:UIControlStateNormal];
    [self.selectBtn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"SELECT"] forState:UIControlStateNormal];
    
    self.reasonForCancelTV.layer.borderColor = [UIColor grayColor].CGColor;
    self.reasonForCancelTV.layer.borderWidth = 1.0;
    self.reasonForCancelTV.layer.cornerRadius = 2;
    
    // Do any additional setup after loading the view.
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    self.reasonForStaticLabel.hidden = YES;
    return YES;
}
-(void)dismissKeyboard{
    
    [self.reasonForCancelTV resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)ServiceCallForCancelRequest{
    
    //http://volive.in/engineering/user_services/cancel
    //lang, user_id, request_id
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"reqID"] forKey:@"request_id"];
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
    [valuesDictionary setObject:self.reasonForCancelTV.text forKey:@"reason"];
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"cancel" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                //[self viewWillAppear:YES];
                TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                
                RESideMenu *sideMenuViewController;
                
                sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
                
                HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
                sideMenuViewController.delegate = self;
                
                [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
                
                [self.navigationController pushViewController:tab animated:YES];
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
                                             message:[jsonDict objectForKey:@"message"]
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    //                                    SWRevealViewController *revealVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SW_Provider"];
                    //                                    [self presentViewController:revealVC animated:YES completion:nil];
                    
                }];
                
                [alert addAction:yesButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                
            });
            
        }
        
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}

- (IBAction)selectBtnAction:(id)sender {
    [self ServiceCallForCancelRequest];
}

- (IBAction)closeBtnAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
