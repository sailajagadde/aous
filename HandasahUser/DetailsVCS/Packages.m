//
//  Packages.m
//  HandasahUser
//
//  Created by MuraliKrishna on 22/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "Packages.h"
#import "PackageTVCell.h"
#import "VisitsVC.h"

@interface Packages (){
    PackageTVCell *cell;
    NSMutableArray *visitTimeArray;
    NSMutableArray *visitDateArray;
    NSMutableArray *visitNumberArray;
    NSMutableArray *reqIDArray;
    NSMutableArray *visitStatusArray;
}

@end

@implementation Packages

- (void)viewDidLoad {
    [super viewDidLoad];
//    "Package Details" = "Package Details";
//    "Visit Details" = "Visit Details";
//    "CHECK PDF" = "CHECK PDF";
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Package Details"];
    self.statustatticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Status"];
    
    self.userImageview.layer.cornerRadius = self.userImageview.frame.size.width/2.0;
    self.userImageview.clipsToBounds = YES;
    [self loadAttributes];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadAttributes{
    
    self.mainView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.mainView.layer.cornerRadius = 10;
    self.mainView.clipsToBounds = YES;
    self.mainView.layer.borderWidth = 0.5;
    
   // self.navigationItem.title = @"Package Details";
    
    [self ServiceCallForPackageDetails];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return visitTimeArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    cell = [self.package_tableview dequeueReusableCellWithIdentifier:@"PackageTVCell" forIndexPath:indexPath];
    
    
    if (cell == nil) {
        cell = [[PackageTVCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PackageTVCell"];
        
    }
    
    cell.visitDate_label.text = [NSString stringWithFormat:@"%@%@%@",[visitDateArray objectAtIndex:indexPath.row],@",",[visitTimeArray objectAtIndex:indexPath.row]];
//    cell.visitName_label.text = [NSString stringWithFormat:@"%@%@",@"Visit ",[visitNumberArray objectAtIndex:indexPath.row]];
    cell.visitName_label.text = [NSString stringWithFormat:@"%@%@",[[SharedClass sharedInstance]languageSelectedString:@"Visit "],[visitNumberArray objectAtIndex:indexPath.row]];
    cell.back_view.layer.cornerRadius = 10;
   
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 92;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VisitsVC *visitVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VisitsVC"];
    visitVC.visitIdStr = [visitNumberArray objectAtIndex:indexPath.row];
    visitVC.reqIdStr = [reqIDArray objectAtIndex:indexPath.row];
    //visitVC.fileString = self.fileStr;
    [self.navigationController pushViewController:visitVC animated:YES];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (IBAction)back_btnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


//service call for resq details

-(void)ServiceCallForPackageDetails{
    //http://volive.in/engineering/user_services/requests_details
    //lang , user_id , request_type =  ( 1 for Single Visit  or  2 for Package Visit )
    
    //    NSString *requestType;
    //    if ([checkStr isEqualToString:@"package"]) {
    //        requestType=@"2";
    //    }else{
    //        requestType=@"1";
    //    }
    
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    //[valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
    [valuesDictionary setObject:self.reqIdStr forKey:@"request_id"];
    [valuesDictionary setObject:self.reqTypeStr forKey:@"request_type"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"requests_details" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            //num_vists
            
            NSDictionary *dict = [jsonDict objectForKey:@"data"];
            NSArray * dataArray = [[NSArray alloc]initWithArray:[dict objectForKey:@"num_vists"]];
            
            visitTimeArray = [[NSMutableArray alloc]init];
            visitDateArray = [[NSMutableArray alloc]init];
            visitNumberArray = [[NSMutableArray alloc]init];
            reqIDArray = [[NSMutableArray alloc]init];
            visitStatusArray = [[NSMutableArray alloc]init];
            
            for (int i=0; i<dataArray.count; i++) {
                [visitTimeArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"visiting_time"]];
                [visitDateArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"visiting_date"]];
                [visitNumberArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"visiting_number"]];
                [reqIDArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"request_id"]];
                [visitStatusArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"visit_status"]];
            }
                                   
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                self.userName_label.text = [[jsonDict objectForKey:@"data"]objectForKey:@"username"];
                self.serName_label.text = [[jsonDict objectForKey:@"data"]objectForKey:@"service_type"];
               // self.cost_label.text = [[jsonDict objectForKey:@"data"]objectForKey:@"request_cost"];
                //[self.userImageview sd_setImageWithURL:[NSURL URLWithString:[[jsonDict objectForKey:@"data"]objectForKey:@"image"]]];
                [self.userImageview sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%s%@",basePath,[[jsonDict objectForKey:@"data"]objectForKey:@"image"]]]];
                
                self.emailLabel.text = [[jsonDict objectForKey:@"data"]objectForKey:@"email"];
                self.mobileNumberLabel.text = [[jsonDict objectForKey:@"data"]objectForKey:@"phone_number"];
                //self.fileStr = [NSString stringWithFormat:@"%s%@",basePath,[[jsonDict objectForKey:@"data"]objectForKey:@"file"]];
                
                NSString *statusStr = [[jsonDict objectForKey:@"data"]objectForKey:@"request_status"];
                if ([statusStr isEqualToString:@"0"]) {
                    //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Waiting"];
                    self.statusChangeLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Waiting"];
                }
                else if ([statusStr isEqualToString:@"1"]){
                    //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Accepted"];
                    self.statusChangeLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Accepted"];
                }
                else if ([statusStr isEqualToString:@"3"]){
                    //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Completed"];
                    self.statusChangeLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Completed"];
                }
                
                               
                //request_status
                [self.package_tableview reloadData];
                
            });
            
        }
        
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}




@end
