//
//  OrderDetailsVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailsVC : UIViewController
- (IBAction)btn_MakePayment:(id)sender;
- (IBAction)btn_Back:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *makePayment_btn;

@property (weak, nonatomic) IBOutlet UILabel *statusStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusChangeLabel;



@property (weak, nonatomic) IBOutlet UIImageView *userimageView;

@property (weak, nonatomic) IBOutlet UILabel *userName_label;
@property (weak, nonatomic) IBOutlet UILabel *emailStatic;
@property (weak, nonatomic) IBOutlet UILabel *email;

@property (weak, nonatomic) IBOutlet UILabel *mobileNumberStatic;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumber;



@property (strong, nonatomic) IBOutlet UITextView *serviceNameTV;

@property (weak, nonatomic) IBOutlet UILabel *serviceName_label;

@property (weak, nonatomic) IBOutlet UILabel *serviceStatic_label;
@property (weak, nonatomic) IBOutlet UILabel *costStatic_label;
@property (weak, nonatomic) IBOutlet UILabel *dateStatic_label;

@property (weak, nonatomic) IBOutlet UILabel *serType_label;
@property (weak, nonatomic) IBOutlet UILabel *cost_label;
@property (weak, nonatomic) IBOutlet UILabel *date_label;
@property (weak, nonatomic) IBOutlet UILabel *staticOrderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;

@property (weak, nonatomic) IBOutlet UILabel *landStatic;
@property (weak, nonatomic) IBOutlet UILabel *landType;

@property (weak, nonatomic) IBOutlet UILabel *deviceStatic;
@property (weak, nonatomic) IBOutlet UITextView *deviceTV;

@property (weak, nonatomic) IBOutlet UILabel *reasonStatic;
@property (weak, nonatomic) IBOutlet UITextView *reasonTV;



@property (weak, nonatomic) IBOutlet UILabel *descStatic_label;
@property (weak, nonatomic) IBOutlet UITextView *desc_TV;

@property NSString *reqIdStr;
@property NSString *reqTypeStr;

@property NSString *fileStr;
@property NSString *checkBtnString;
@property (strong, nonatomic) IBOutlet UIButton *viewPhotoBtn;
- (IBAction)viewPhotoVideoAction:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *deviceHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *reasonHeightConstraint;

@end
