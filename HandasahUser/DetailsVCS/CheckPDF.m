//
//  CheckPDF.m
//  HandasahUser
//
//  Created by MuraliKrishna on 26/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "CheckPDF.h"

@interface CheckPDF ()

@end

@implementation CheckPDF

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back-1"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    back.tintColor = [UIColor whiteColor];
    [self.navigationItem setLeftBarButtonItem:back];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Regular" size:18] }];
    
    self.navigationItem.title = @"Check";
    
    
    NSLog(@"file %@",self.fileStr);
        NSURL* url = [NSURL URLWithString:self.fileStr];
        NSURLRequest* request = [NSURLRequest requestWithURL:url];
    [self.webViewPdf loadRequest:request];
    
    
    
    // Do any additional setup after loading the view.
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
