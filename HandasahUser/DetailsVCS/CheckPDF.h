//
//  CheckPDF.h
//  HandasahUser
//
//  Created by MuraliKrishna on 26/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckPDF : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webViewPdf;


@property NSString *fileStr;
@end
