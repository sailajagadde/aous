//
//  VisitsVC.m
//  HandasahUser
//
//  Created by MuraliKrishna on 26/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "VisitsVC.h"
#import "CheckPDF.h"

@interface VisitsVC ()

@end

@implementation VisitsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Visit Details"];
    self.dateStatic_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Date & Time"];
    self.desc_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Description"];
    [self.checkBtn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"CHECK PDF"] forState:UIControlStateNormal];
    
    
    self.userImageview.layer.cornerRadius = self.userImageview.frame.size.width/2.0;
    self.userImageview.clipsToBounds = YES;
    [self ServiceCallForVisitDetails];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back_btnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


//service call for resq details

-(void)ServiceCallForVisitDetails{
    //http://volive.in/engineering/user_services/visit_details
    //lang, request_id, visit_number
    
    //    NSString *requestType;
    //    if ([checkStr isEqualToString:@"package"]) {
    //        requestType=@"2";
    //    }else{
    //        requestType=@"1";
    //    }
    
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    //[valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
    [valuesDictionary setObject:self.reqIdStr forKey:@"request_id"];
    [valuesDictionary setObject:self.visitIdStr forKey:@"visit_number"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"visit_details" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                self.orderIdLabel.text = [NSString stringWithFormat:@"%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"order_id"]];
                
                
                self.emailLabel.text = [[jsonDict objectForKey:@"data"]objectForKey:@"email"];
                self.mobileLabel.text = [[jsonDict objectForKey:@"data"]objectForKey:@"phone_number"];
                
                self.landType.text = [NSString stringWithFormat:@"%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"type_of_land"]];
                
                if ([str isEqualToString:@"2"]) {
                    self.serType_label.text= [NSString stringWithFormat:@"%@%@",[[jsonDict objectForKey:@"data"]objectForKey:@"service_type"],@" :"];
                    //[NSString stringWithFormat:@"%@%@",[serviceNameArray objectAtIndex:indexPath.row],@" :"];
                } else{
                    
                    self.serType_label.text = [NSString stringWithFormat:@"%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"service_type"]];
                }
                
                self.cost_label.text = [NSString stringWithFormat:@"%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"request_cost"]];

                
                
                self.userName_label.text = [[jsonDict objectForKey:@"data"]objectForKey:@"username"];
                self.date_label.text = [NSString stringWithFormat:@"%@%@%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_date"],@",",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_time"]];
                self.visitNumber_label.text = [NSString stringWithFormat:@"%@%@",[[SharedClass sharedInstance]languageSelectedString:@"Visit "],self.visitIdStr];
                self.date_label.text =
                //[NSString stringWithFormat:@"%@%@%@",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_date"],@",",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_time"]];
                
                [NSString stringWithFormat:@"%@%@%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_date"],@",",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_time"]];
                //self.desc_TV.text = [[jsonDict objectForKey:@"data"]objectForKey:@"description"];
                self.desc_TV.text = [[jsonDict objectForKey:@"data"]objectForKey:@"sp_description"];
                
                //[self.userImageview sd_setImageWithURL:[NSURL URLWithString:[[jsonDict objectForKey:@"data"]objectForKey:@"image"]]];
                [self.userImageview sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%s%@",basePath,[[jsonDict objectForKey:@"data"]objectForKey:@"image"]]]];
                
                 self.fileString = [NSString stringWithFormat:@"%s%@",basePath,[[jsonDict objectForKey:@"data"]objectForKey:@"file"]];
                
                //1 enable 0 disable
                
                NSString *statusStr = [[jsonDict objectForKey:@"data"]objectForKey:@"visit_status"];
                if ([statusStr isEqualToString:@"0"]) {
                    self.checkBtn.enabled = NO;
                    self.checkBtn.hidden = YES;
                    //self.statusChange.text = [NSString stringWithFormat:@"%@",[[SharedClass sharedInstance]languageSelectedString:@":Accepted"]];
                }
                else if ([statusStr isEqualToString:@"1"]){
                    self.checkBtn.enabled = YES;
                    self.checkBtn.hidden = NO;
                   // self.statusChange.text = [NSString stringWithFormat:@"%@",[[SharedClass sharedInstance]languageSelectedString:@":Accepted"]];
                }
                
                NSString *reqstatusStr = [[jsonDict objectForKey:@"data"]objectForKey:@"request_status"];
                
                if ([reqstatusStr isEqualToString:@"0"]) {
                    //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Waiting"];
                    self.statusChange.text = [NSString stringWithFormat:@"%@",[[SharedClass sharedInstance]languageSelectedString:@":Waiting"]];
                    
                }
                else if ([reqstatusStr isEqualToString:@"1"]){
                    //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Accepted"];
                    self.statusChange.text = [NSString stringWithFormat:@"%@",[[SharedClass sharedInstance]languageSelectedString:@":Accepted"]];
                    
                }
                else if ([reqstatusStr isEqualToString:@"3"]){
                    //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Completed"];
                    self.statusChange.text = [NSString stringWithFormat:@"%@%@",@": ",[[SharedClass sharedInstance]languageSelectedString:@"Completed"]];
                }
                
            });
            
        }
        
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}


- (IBAction)checkBtnAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.fileString]];
//    CheckPDF *checkVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckPDF"];
//    checkVC.fileStr = self.fileString;
//    [self.navigationController pushViewController:checkVC animated:YES];
}
@end
