//
//  SPDetailsVC.m
//  HandasahUser
//
//  Created by MuraliKrishna on 21/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "SPDetailsVC.h"
#import "VisitFirstVC.h"

@interface SPDetailsVC ()

@end

@implementation SPDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_wh"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    back.tintColor = [UIColor whiteColor];
    [self.navigationItem setLeftBarButtonItem:back];
    
//REQUEST NOW
    self.title = [[SharedClass sharedInstance]languageSelectedString:@"Provider Details"];
    
    self.serviceStatic_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Service"];
    self.costStatic_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Cost"];
    self.emailStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Email :"];
    self.mobileNumberStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Mobile Number :"];
    
    self.userName_label.text = self.nameStr;
    self.email.text = self.emailStr;
    self.mobileNumber.text = self.mobileStr;
    self.serType_label.text = [NSString stringWithFormat:@"%@%@",@" :",self.sernameStr];
    self.cost_label.text = [NSString stringWithFormat:@"%@%@",@" :",self.costStr];
    [self.userimageView sd_setImageWithURL:[NSURL URLWithString:self.imageStr]];
    
    [self.requestBtn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"REQUEST NOW"] forState:UIControlStateNormal];
    // Do any additional setup after loading the view.
}
-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)requestBtnAction:(id)sender {
    
    VisitFirstVC * forVisit = [self.storyboard instantiateViewControllerWithIdentifier:@"VisitFirstVC"];
    forVisit.serIDStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"serID"];
    forVisit.spIDStr =[[NSUserDefaults standardUserDefaults]objectForKey:@"SPUID"];
    [self.navigationController pushViewController:forVisit animated:YES];
}
@end
