//
//  SPDetailsVC.h
//  HandasahUser
//
//  Created by MuraliKrishna on 21/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPDetailsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *userimageView;

@property (weak, nonatomic) IBOutlet UILabel *userName_label;
@property (weak, nonatomic) IBOutlet UILabel *emailStatic;
@property (weak, nonatomic) IBOutlet UILabel *email;

@property (weak, nonatomic) IBOutlet UILabel *mobileNumberStatic;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumber;

@property (weak, nonatomic) IBOutlet UILabel *serviceStatic_label;
@property (weak, nonatomic) IBOutlet UILabel *serType_label;

@property (weak, nonatomic) IBOutlet UILabel *costStatic_label;
@property (weak, nonatomic) IBOutlet UILabel *cost_label;


@property (weak, nonatomic) IBOutlet UIButton *requestBtn;

@property NSString *spUserIDStr;

- (IBAction)requestBtnAction:(id)sender;

@property NSString *imageStr;
@property NSString *nameStr;
@property NSString *emailStr;
@property NSString *mobileStr;
@property NSString *sernameStr;
@property NSString *costStr;


@end
