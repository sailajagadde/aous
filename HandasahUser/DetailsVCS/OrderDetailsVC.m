//
//  OrderDetailsVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "OrderDetailsVC.h"
#import "PackagesVC.h"
#import "PaymentVC.h"
#import "CheckPDF.h"

@interface OrderDetailsVC (){
    NSString *checkStr;
    }

@end

@implementation OrderDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Order Details"];
    self.serviceStatic_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Service"];
    self.costStatic_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Cost"];
    self.dateStatic_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Date & Time"];
    self.staticOrderIdLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Order ID"];
    self.descStatic_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Description"];
    

    self.deviceStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Device Features"];
    self.reasonStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Reason For Taking"];
    
    self.statusStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Status"];
    
     //[self.makePayment_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"CHECK PDF"] forState:UIControlStateNormal];
    
     self.emailStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Email :"];
     self.mobileNumberStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Mobile Number :"];
    
   self.landStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Type Of Land"];
    
      [self.viewPhotoBtn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"VIEW PHOTO/VIDEO"] forState:UIControlStateNormal];
    
    self.userimageView.layer.cornerRadius = self.userimageView.frame.size.width/2.0;
    self.userimageView.clipsToBounds = YES;
    [self ServiceCallForReqDetails];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_MakePayment:(id)sender {
 
    [[NSUserDefaults standardUserDefaults]objectForKey:@"file"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"file"]]];
    
//        CheckPDF *checkVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckPDF"];
//        checkVC.fileStr = self.fileStr;
//        [self.navigationController pushViewController:checkVC animated:YES];
 
}

- (IBAction)btn_Back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)ServiceCallForReqDetails{
    //http://volive.in/engineering/user_services/requests_details
    //lang , user_id , request_type =  ( 1 for Single Visit  or  2 for Package Visit )
    
//    NSString *requestType;
//    if ([checkStr isEqualToString:@"package"]) {
//        requestType=@"2";
//    }else{
//        requestType=@"1";
//    }
 
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    [valuesDictionary setObject:self.reqIdStr forKey:@"request_id"];
    //[valuesDictionary setObject:self.reqTypeStr forKey:@"request_type"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
     [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"request_details" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                [SVProgressHUD dismiss];
           self.userName_label.text = [[jsonDict objectForKey:@"data"]objectForKey:@"username"];
          // self.serviceName_label.text = [[jsonDict objectForKey:@"data"]objectForKey:@"service_type"];
           self.serviceNameTV.text= [[jsonDict objectForKey:@"data"]objectForKey:@"service_type"];
                 [self.userimageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%s%@",basePath,[[jsonDict objectForKey:@"data"]objectForKey:@"image"]]]];
//           self.orderIdLabel.text = [NSString stringWithFormat:@"%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"order_id"]];
                 
                 
                 self.email.text = [[jsonDict objectForKey:@"data"]objectForKey:@"email"];
                 self.mobileNumber.text = [[jsonDict objectForKey:@"data"]objectForKey:@"phone_number"];
                 
                 //self.landType.text = [NSString stringWithFormat:@"%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"type_of_land"]];
                 
                 if ([str isEqualToString:@"2"]) {
                     self.serType_label.text= [NSString stringWithFormat:@"%@%@",@": ",
                                               [[jsonDict objectForKey:@"data"]objectForKey:@"service_type"]];
                     self.desc_TV.textAlignment=NSTextAlignmentRight;
                     //[NSString stringWithFormat:@"%@%@",[serviceNameArray objectAtIndex:indexPath.row],@" :"];
                     
                     self.cost_label.text = [NSString stringWithFormat:@"%@%@",[[jsonDict objectForKey:@"data"]objectForKey:@"request_cost"],@": "];
                     NSString *dateTimeStr = [NSString stringWithFormat:@"%@%@%@%@",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_date"],@",",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_time"],@": "];
                     self.date_label.text = dateTimeStr;
                     
                     self.orderIdLabel.text = [NSString stringWithFormat:@"%@%@",[[jsonDict objectForKey:@"data"]objectForKey:@"order_id"],@": "];
                     
                     
                 } else{
                     
                     self.serType_label.text = [NSString stringWithFormat:@"%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"service_type"]];
                     self.desc_TV.textAlignment=NSTextAlignmentLeft;
                     
                     self.cost_label.text = [NSString stringWithFormat:@"%@%@",@": ",
                                             [[jsonDict objectForKey:@"data"]objectForKey:@"request_cost"]];
                     
                     NSString *dateTimeStr = [NSString stringWithFormat:@"%@%@%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_date"],@",",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_time"]];
                     self.date_label.text = dateTimeStr;
                     self.orderIdLabel.text = [NSString stringWithFormat:@"%@%@",@": ",
                                               [[jsonDict objectForKey:@"data"]objectForKey:@"order_id"]];
                     
                 }
          
//           self.cost_label.text = [NSString stringWithFormat:@"%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"request_cost"]];
//           NSString *dateTimeStr = [NSString stringWithFormat:@"%@%@%@%@",@": ",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_date"],@",",[[jsonDict objectForKey:@"data"]objectForKey:@"requested_time"]];
//           self.date_label.text = dateTimeStr;
                 
                 
                 
           self.desc_TV.text = [[jsonDict objectForKey:@"data"]objectForKey:@"description"];
            
                 self.fileStr = [NSString stringWithFormat:@"%s%@",basePath,[[jsonDict objectForKey:@"data"]objectForKey:@"file"]];
                 
                 [[NSUserDefaults standardUserDefaults]setObject:self.fileStr forKey:@"file"];
                 
                 NSString *statusStr = [[jsonDict objectForKey:@"data"]objectForKey:@"request_status"];
                 if ([statusStr isEqualToString:@"0"]||([statusStr isEqualToString:@"5"])) {
                     //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Waiting"];
//                     self.statusChangeLabel.text = [NSString stringWithFormat:@"%@",[[SharedClass sharedInstance]languageSelectedString:@":Waiting"]];
                     self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",[[SharedClass sharedInstance]languageSelectedString:@"Waiting"]];
                     self.makePayment_btn.hidden = YES;
                     self.deviceTV.hidden=YES;
                     self.reasonTV.hidden=YES;
                     self.deviceStatic.hidden = YES;
                     self.reasonStatic.hidden=YES;
                     self.deviceHeightConstraint.constant=0;
                     self.reasonHeightConstraint.constant=0;
                 }
                 else if ([statusStr isEqualToString:@"1"]){
                     //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Accepted"];
                     //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@",[[SharedClass sharedInstance]languageSelectedString:@":Accepted"]];
                     self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",[[SharedClass sharedInstance]languageSelectedString:@"Accepted"]];
                     self.makePayment_btn.hidden = YES;
                     self.deviceTV.hidden=YES;
                     self.reasonTV.hidden=YES;
                     self.deviceStatic.hidden = YES;
                     self.reasonStatic.hidden=YES;
                     self.deviceHeightConstraint.constant=0;
                     self.reasonHeightConstraint.constant=0;
                 }
                 else if ([statusStr isEqualToString:@"3"]){
                     //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Completed"];
                     self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",[[SharedClass sharedInstance]languageSelectedString:@"Completed"]];
                    // self.makePayment_btn.hidden = NO;
                     self.makePayment_btn.hidden = YES;
                     
                     self.deviceTV.hidden=YES;
                     self.reasonTV.hidden=YES;
                     self.deviceStatic.hidden = YES;
                     self.reasonStatic.hidden=YES;
                     self.deviceHeightConstraint.constant=0;
                     self.reasonHeightConstraint.constant=0;
                     
                 }else if ([statusStr isEqualToString:@"4"]){
                     //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Completed"];
                     //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@",
                                                    //[[SharedClass sharedInstance]languageSelectedString:@": Canceled"]];
                     self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",
                                                    [[SharedClass sharedInstance]languageSelectedString:@"Canceled"]];
                     self.makePayment_btn.hidden = YES;
                     self.deviceTV.hidden=YES;
                     self.reasonTV.hidden=YES;
                     self.deviceStatic.hidden = YES;
                     self.reasonStatic.hidden=YES;
                     self.deviceHeightConstraint.constant=0;
                     self.reasonHeightConstraint.constant=0;
                 }
                 else if ([statusStr isEqualToString:@"6"]){
                     //self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",@"Completed"];
                     self.statusChangeLabel.text = [NSString stringWithFormat:@"%@%@",@": ",[[SharedClass sharedInstance]languageSelectedString:@"Under Repair"]];
                     self.makePayment_btn.hidden = YES;
                     
                     
                     if ([str isEqualToString:@"2"]) {
                         UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
                         UITextView.appearance.semanticContentAttribute= UISemanticContentAttributeForceRightToLeft;
                         self.deviceTV.textAlignment = NSTextAlignmentRight;
                         self.reasonTV.textAlignment = NSTextAlignmentRight;
                     } else{
                         
                         UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
                         UITextView.appearance.semanticContentAttribute= UISemanticContentAttributeForceLeftToRight;
                         self.deviceTV.textAlignment = NSTextAlignmentLeft;
                         self.reasonTV.textAlignment = NSTextAlignmentLeft;
                         
                     }
                     
                     self.deviceTV.text = [[jsonDict objectForKey:@"data"]objectForKey:@"device_details"];
                      self.reasonTV.text = [[jsonDict objectForKey:@"data"]objectForKey:@"repair_details"];
                     self.deviceStatic.hidden = NO;
                     self.reasonStatic.hidden=NO;
                     self.deviceHeightConstraint.constant=80;
                     self.reasonHeightConstraint.constant=80;
                 }
                 
                 NSString *url = [NSString stringWithFormat:@"%s%@",basePath,[[jsonDict objectForKey:@"data"]objectForKey:@"url"]];
                 [[NSUserDefaults standardUserDefaults]setObject:url forKey:@"urlPhoto"];
                 
        });
            
            }
        
       else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}




- (IBAction)viewPhotoVideoAction:(id)sender {
    
   NSString *urlstring = [[NSUserDefaults standardUserDefaults]objectForKey:@"urlPhoto"];
    NSLog(@"url %@",urlstring);
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"urlPhoto"]]];
}
@end
