//
//  VisitsVC.h
//  HandasahUser
//
//  Created by MuraliKrishna on 26/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VisitsVC : UIViewController

- (IBAction)back_btnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *userImageview;
@property (weak, nonatomic) IBOutlet UILabel *userName_label;

@property (weak, nonatomic) IBOutlet UILabel *emailStatic;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileStatic;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
//




@property (weak, nonatomic) IBOutlet UILabel *serviceStatic_label;
@property (weak, nonatomic) IBOutlet UILabel *costStatic_label;
@property (weak, nonatomic) IBOutlet UILabel *dateStatic_label;

@property (weak, nonatomic) IBOutlet UILabel *serType_label;
@property (weak, nonatomic) IBOutlet UILabel *cost_label;
@property (weak, nonatomic) IBOutlet UILabel *date_label;

@property (weak, nonatomic) IBOutlet UILabel *statusStatic;
@property (weak, nonatomic) IBOutlet UILabel *statusChange;

@property (weak, nonatomic) IBOutlet UILabel *staticOrderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;

@property (weak, nonatomic) IBOutlet UILabel *landStatic;
@property (weak, nonatomic) IBOutlet UILabel *landType;

//



@property (weak, nonatomic) IBOutlet UILabel *visitNumber_label;
//@property (weak, nonatomic) IBOutlet UILabel *time_label;
@property (weak, nonatomic) IBOutlet UILabel *desc_label;
@property (weak, nonatomic) IBOutlet UITextView *desc_TV;

@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
- (IBAction)checkBtnAction:(id)sender;


@property NSString *fileString;
@property NSString *visitIdStr;
@property NSString *reqIdStr;
@end
