//
//  Packages.h
//  HandasahUser
//
//  Created by MuraliKrishna on 22/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Packages : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *package_tableview;
@property (weak, nonatomic) IBOutlet UIView *mainView;

- (IBAction)back_btnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *userImageview;
@property (weak, nonatomic) IBOutlet UILabel *userName_label;
@property (weak, nonatomic) IBOutlet UILabel *serName_label;
//@property (weak, nonatomic) IBOutlet UILabel *cost_label;
@property (weak, nonatomic) IBOutlet UILabel *statustatticLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusChangeLabel;

@property (weak, nonatomic) IBOutlet UILabel *emailStatic;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileStatic;

@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;


@property NSString *reqIdStr;
@property NSString *reqTypeStr;

@property NSString *fileStr;
@end
