//
//  TermsCondVC.h
//  HandasahUser
//
//  Created by MuraliKrishna on 12/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsCondVC : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *back_btn;
- (IBAction)back_action:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *terms_TV;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property NSString *sideStr;

@end
