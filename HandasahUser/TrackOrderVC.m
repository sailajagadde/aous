//
//  TrackOrderVC.m
//  HandasahUser
//
//  Created by Apple on 5/13/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "TrackOrderVC.h"
#import "RESideMenu.h"
#import "HomeViewController.h"


@interface TrackOrderVC ()<RESideMenuDelegate>
{
    RESideMenu * sideMenu;
    CLLocationManager * locationManager;
    CLLocationCoordinate2D center;
    
    CLLocation *currentLocation;
}

@end

@implementation TrackOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Track Order"];

    self.tracklabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Track Order"];
    
     self.ordeReq.text = [[SharedClass sharedInstance]languageSelectedString:@"Order Requested"];
     self.orderAccept.text = [[SharedClass sharedInstance]languageSelectedString:@"Order Accepted"];
     self.paymentComplete.text = [[SharedClass sharedInstance]languageSelectedString:@"Payment Completed"];
     self.orderComplete.text = [[SharedClass sharedInstance]languageSelectedString:@"Order Completed"];
    
    
    [self.homeBtn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"HOME"] forState:UIControlStateNormal];
    locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization];
    // Do any additional setup after loading the view.
}

-(void)loadMapView{
    
    center.latitude = [self.lat doubleValue];
    center.longitude = [self.lng doubleValue];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.lat doubleValue]  longitude:[self.lng doubleValue] zoom:16];
    [_googleMapView setCamera:camera];
    _googleMapView.myLocationEnabled = YES;
    _googleMapView.delegate = self;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([self.lat doubleValue],[self.lng doubleValue]);
    marker.icon=[UIImage imageNamed:@"marker"];
    // marker.title = @"Sydney";
    //  marker.snippet = @"Australia";
    marker.map = _googleMapView;
}
-(void)locationManager:(CLLocationManager* )manager didUpdateLocations:(NSArray<CLLocation *  > *)locations
{
    
    // User location
    
    //    currentLocation = [locations lastObject];
    //
    //    [locationManager stopUpdatingLocation];
    //
    //    if (self.lat == NULL && self.lng == NULL)
    //    {
    //        self.lat = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
    //        self.lng  = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    //
    //        [self loadViewWithAttributes];
    //
    //    }
    // User location
    
    currentLocation = [locations lastObject];
    
    [locationManager stopUpdatingLocation];
    
    if (self.lat == NULL && self.lng == NULL)
    {
        self.lat = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
        self.lng  = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [self loadMapView];
                       });
        
    }
    
    NSLog(@"delegate method is calling");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (IBAction)btn_Bak1:(id)sender {
    //[self dismissViewControllerAnimated:true completion:nil];
     [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)homeBtnAction:(id)sender {
    
    TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
    
    RESideMenu *sideMenuViewController;
    
    sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
    
    HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
    sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
    sideMenuViewController.delegate = self;
    
    [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
    
    [self.navigationController pushViewController:tab animated:YES];
    
}
@end
