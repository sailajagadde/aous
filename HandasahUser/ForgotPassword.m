//
//  ForgotPassword.m
//  HandasahUser
//
//  Created by MuraliKrishna on 25/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "ForgotPassword.h"
#import "LogInVC.h"

@interface ForgotPassword ()

@end

@implementation ForgotPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back-1"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    back.tintColor = [UIColor whiteColor];
    [self.navigationItem setLeftBarButtonItem:back];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Regular" size:18] }];
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Forgot Password"];
    self.email_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Enter EmailID"];
    [self.submitBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"SUBMIT"] forState:UIControlStateNormal];
    
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    [self SetTextFieldBorder:self.email_TF];
    
}

-(void)SetTextFieldBorder :(UITextField *)textField{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.8;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
}

-(void)dismissKeyboard{
    
    [self.email_TF resignFirstResponder];
   
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.email_TF) {
        [self.email_TF resignFirstResponder];
    }
    
    return YES;
    
}

-(void)serviceCallForForgotPassword{
    
    //http://volive.in/aous/user_services/forgot_password
    //lang,email
    
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
   
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    [valuesDictionary setObject:self.email_TF.text forKey:@"email"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
    } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"forgot_password" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                LogInVC *logIn = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInVC"];
                [self.navigationController pushViewController:logIn animated:YES];
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
                                             message:[jsonDict objectForKey:@"message"]
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                }];
                
                [alert addAction:yesButton];
                
                [self presentViewController:alert animated:YES completion:nil];
        
            });
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                            
            });
            
        }
        
    }];
    
}



- (IBAction)submitBtnAction:(id)sender {
    [self serviceCallForForgotPassword];
}
@end
