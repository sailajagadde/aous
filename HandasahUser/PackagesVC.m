//
//  PackagesVC.m
//  HandasahUser
//
//  Created by Apple on 5/18/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "PackagesVC.h"

#import "RESideMenu.h"
#import "HomeViewController.h"
#import "ProceedPackagesVC.h"

#import "TabBar.h"
#import "AppDelegate.h"


@interface PackagesVC ()<RESideMenuDelegate>
{
    RESideMenu * sideMenu;
    AppDelegate *appDelegate;
}

@end

@implementation PackagesVC

- (void)viewDidLoad {
    [super viewDidLoad];

    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if ([self.packString isEqualToString:@"fromAppDel"]) {
        [self.backBtn setTintColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]];
    }else{
        [self.backBtn setTintColor:[UIColor whiteColor]];
    }
    
    [self.singleBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Single"] forState:UIControlStateNormal];
    [self.packageBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Package"] forState:UIControlStateNormal];
    // Do any additional setup after loading the view.
    
}
//
//-(void)viewWillAppear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden = NO;
//
//}
//-(void)viewWillDisappear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden = YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)singleBtnAction:(id)sender {
    
    //[[NSUserDefaults standardUserDefaults]setObject:@"single" forKey:@"type"];
    [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"typeStr"];
    
    appDelegate.selectTabItem =@"0";
    HomeViewController * home=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
    
        RESideMenu *sideMenuViewController;
    
        sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
    
        HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
        sideMenuViewController.delegate = self;
    
        [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
    
        [self.navigationController pushViewController:tab animated:YES];
}

- (IBAction)packageBtnAction:(id)sender {
    
    //[[NSUserDefaults standardUserDefaults]setObject:@"package" forKey:@"type"];
    [[NSUserDefaults standardUserDefaults]setObject:@"2" forKey:@"typeStr"];
    
    ChoosePackagesVC *packVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChoosePackagesVC"];
     [self presentViewController:packVC animated:YES completion:nil];
    

    
  
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
