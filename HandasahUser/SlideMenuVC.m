//
//  SlideMenuVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "SlideMenuVC.h"
#import "SlideMenuCell.h"
#import "HomeViewController.h"
#import "RESideMenu.h"
#import "Notifications.h"
#import "MyRequestVC.h"
#import "CompleteOrderVC.h"
#import "AccountVC.h"
#import "TabBar.h"
#import "AppDelegate.h"
#import "CancelRequests.h"
#import "AppDelegate.h"
#import "TabBar.h"

@interface SlideMenuVC ()<RESideMenuDelegate>

{
     RESideMenu * sideMenu;
    HomeViewController * home;
    Notifications * noti;
    MyRequestVC * request;
    CompleteOrderVC * completeOrder;
    AccountVC * account;
    TabBar * tab ;
    AppDelegate *appDelegate;
    PrivacyPolicyVC * privacy;
    TermsCondVC *terms;
    ContactUsVC *contactUs;
    CancelRequests *cancel;
}
@end

@implementation SlideMenuVC

- (void)viewDidLoad {
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [super viewDidLoad];
    
    
//    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
//
//    if ([str isEqualToString:@"2"]) {
//        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
//    } else{
//
//        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
//
//    }
    
    menuImg = @[@"m home",@"m notification",@"mu request",@"Completed Order color",@"Completed Order color",@"Account color",@"privacy",@"terms",@"contact",@"language",@"logout"];
    menuNames = @[[[SharedClass sharedInstance]languageSelectedString:@"Home"],[[SharedClass sharedInstance]languageSelectedString:@"Notifications"],[[SharedClass sharedInstance]languageSelectedString:@"My Requests"],[[SharedClass sharedInstance]languageSelectedString:@"Completed Orders"],[[SharedClass sharedInstance]languageSelectedString:@"Cancel Orders"],[[SharedClass sharedInstance]languageSelectedString:@"Account"],[[SharedClass sharedInstance]languageSelectedString:@"Privacy Policy"],[[SharedClass sharedInstance]languageSelectedString:@"Terms&Conditions"],[[SharedClass sharedInstance]languageSelectedString:@"Contact Us"],[[SharedClass sharedInstance]languageSelectedString:@"Change Language"],[[SharedClass sharedInstance]languageSelectedString:@"Logout"]];

    
    home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    noti = [self.storyboard instantiateViewControllerWithIdentifier:@"Notifications1"];
    request = [self.storyboard instantiateViewControllerWithIdentifier:@"MyRequestVC"];
    completeOrder = [self.storyboard instantiateViewControllerWithIdentifier:@"CompleteOrderVC"];
    account = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountVC"];
    privacy = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyVC1"];
    terms = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsCondVC1"];
    contactUs = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC1"];
    cancel = [self.storyboard instantiateViewControllerWithIdentifier:@"CancelRequests1"];
    
//    self.name_label.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"username"]];
//    self.email_label.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"email"]];
//    [self.user_ImageView sd_setImageWithURL:[[NSUserDefaults standardUserDefaults] objectForKey:@"image"] placeholderImage:[UIImage imageNamed:@"profile"]];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMainMenu:)
                                                 name:@"login" object:nil];
    
    //[self.slideTableObj reloadData];
    // Do any additional setup after loading the view.
}
//-(void)viewWillAppear:(BOOL)animated{
//    [self viewDidLoad];
//    self.name_label.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
//    self.email_label.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
//    [self.user_ImageView sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"image"]]];
//}


- (void)showMainMenu:(NSNotification *)note {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self->_name_label.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
        self->_email_label.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        //[self.user_ImageView sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"profile"]];
    });
   
}


-(void)viewWillAppear:(BOOL)animated {
    
    //[self viewDidLoad];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
        
        
    } else{
        
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
        
    }
    
//    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"email"] isEqualToString:@""]) {
//        self.name_label.text = @"User";
//        self.email_label.text = @"Email";
//    }
//    else{
//    self.name_label.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
//    self.email_label.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
//    }
    
    self.name_label.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
    self.email_label.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    //[self.user_ImageView sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"profile"]];
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];   //it hides
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //Animation
        
        [UIView animateWithDuration:0 animations:^{
            
            //self.user_ImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
        }completion:^(BOOL finished){
            
            // Finished scaling down imageview, now resize it
            
            [UIView animateWithDuration:0.4 animations:^{
                
                self.user_ImageView.transform = CGAffineTransformIdentity;
                
               // self.user_ImageView.layer.cornerRadius = self.user_ImageView.frame.size.height/2.0;
               // self.user_ImageView.clipsToBounds = YES;
                
            }completion:^(BOOL finished){
                
                
            }];
            
            
        }];
        
        
        
        //self.cust_ImageView.layer.cornerRadius = self.cust_ImageView.frame.size.width/2.0;
        //self.cust_ImageView.clipsToBounds = YES;
        
        
        
    });
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return menuNames.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    SlideMenuCell *cell = [self.slideTableObj dequeueReusableCellWithIdentifier:@"SlideCell" forIndexPath:indexPath];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    if ([str isEqualToString:@"2"]) {
        cell.imgLeadingConstarint.constant=85;
    }
    else{
        cell.imgLeadingConstarint.constant=40;
    }
  
    cell.slideNames.text =[menuNames objectAtIndex:indexPath.row];
    cell.slideImg.image = [UIImage imageNamed:[menuImg objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 65;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row == 0) {
         tab = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        appDelegate.selectTabItem=@"0";
        [self.sideMenuViewController setContentViewController :tab];
        [self.sideMenuViewController hideMenuViewController];
        
    }else if (indexPath.row == 1)
            {
    //    appDelegate.selectTabItem=@"1";
                [self.sideMenuViewController setContentViewController:noti];
                [self.sideMenuViewController hideMenuViewController];

    }else if (indexPath.row ==2)
                   {
                        tab = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                       appDelegate.selectTabItem=@"1";
                     // UINavigationController *navi =[[UINavigationController alloc]initWithRootViewController:request];
                        [self.sideMenuViewController setContentViewController:tab];
                        [self.sideMenuViewController hideMenuViewController];

    }else if (indexPath.row ==3)
    {
         tab = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        appDelegate.selectTabItem=@"2";
        [self.sideMenuViewController setContentViewController:tab];
        [self.sideMenuViewController hideMenuViewController];
    }else if (indexPath.row ==4)
    {
        
        [self.sideMenuViewController setContentViewController:cancel];
        [self.sideMenuViewController hideMenuViewController];
    }else if (indexPath.row ==5)
           {
                tab = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
               appDelegate.selectTabItem=@"3";
                [self.sideMenuViewController setContentViewController:tab];
                [self.sideMenuViewController hideMenuViewController];
    }else if (indexPath.row == 6)
           {
               
//               privacy = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyVC"];
//               UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:privacy];
//                 [self.sideMenuViewController setContentViewController:nav]; //push your viewcontroller
//              // [self sideMenuViewController hideRightViewAnimated:YES completionHandler:nil]; //hide the menu
//               [self.sideMenuViewController hideMenuViewController];
               
               [self.sideMenuViewController setContentViewController:privacy];
               [self.sideMenuViewController hideMenuViewController];
               
    }else if (indexPath.row ==7)
           {
               [self.sideMenuViewController setContentViewController:terms];
               [self.sideMenuViewController hideMenuViewController];
    }else if (indexPath.row ==8)
           {
               [self.sideMenuViewController setContentViewController:contactUs];
               [self.sideMenuViewController hideMenuViewController];
    }else if (indexPath.row ==9){
        [self changeLanguage];
           }
    else if (indexPath.row ==10)
           {
               [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"user_id"];
               UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
               UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
               window.rootViewController = [storyboard instantiateInitialViewController];
           }
    
    
    }

-(void)changeLanguage{
    
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] message:[[SharedClass sharedInstance]languageSelectedString:@"Select Language"] preferredStyle:UIAlertControllerStyleActionSheet];
    
    [ac addAction:[UIAlertAction actionWithTitle:@"English" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"currentLanguage"];
        [self viewDidLoad];
        
        [self.slideTableObj reloadData];
         UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
        [self redirectToHome];
        //[self setLanguage];
        
    }]];
    
    [ac addAction:[UIAlertAction actionWithTitle:@"عربى" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"2" forKey:@"currentLanguage"];
        [self viewDidLoad];
        [self.slideTableObj reloadData];
         UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
        [self redirectToHome];
        //[self setLanguage];
    }]];
    
    [ac addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [ac dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:ac animated:YES completion:nil];
    
    
}

-(void)redirectToHome{
    TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
    appDelegate.selectTabItem=@"0";
    RESideMenu *sideMenuViewController;
    
    sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
    
    HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
    sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
    sideMenuViewController.delegate = self;
    
    [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
    
    [self.navigationController pushViewController:tab animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
