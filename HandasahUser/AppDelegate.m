//
//  AppDelegate.m
//  HandasahUser
//
//  Created by Apple on 5/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "AppDelegate.h"
@import GoogleMaps;
#import "RatingView.h"

#import "RESideMenu.h"
#import "SlideMenuVC.h"
#import "PackagesVC.h"
#import "LogInVC.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface AppDelegate ()<RESideMenuDelegate>{
    RESideMenu *sideMenu;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
  //  UIView.appearance().semanticContentAttribute = .forceRightToLeft
   // UIView.appearance.semanticContentAttribute = UIUserInterfaceLayoutDirectionRightToLeft;
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
       UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
        UITextView.appearance.semanticContentAttribute= UISemanticContentAttributeForceRightToLeft;
    } else{
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
        UITextView.appearance.semanticContentAttribute=  UISemanticContentAttributeForceLeftToRight;
    }
   
    [self registerForRemoteNotifications];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"email"] isEqualToString:@""]) {
        [[NSUserDefaults standardUserDefaults]setObject:@"show" forKey:@"checking"];
    }
    else{
        [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"checking"];
    }
    
    
    
    
    
    
    [self autoLogin];
    self.strCheek =@"check";
    [GMSServices provideAPIKey:@"AIzaSyAMXbwZC-5GV_BfafIIUMFcy5ftaZU7rbw"];

    return YES;
}


- (void)registerForRemoteNotifications{
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
    
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
        if(!error){
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [[UIApplication sharedApplication] registerForRemoteNotifications];
                           });
        }
    }];
    
}

else {
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
}
}
-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
//    [self.progTimer invalidate];
//     [SVProgressHUD dismiss];
    // iOS 10 will handle notifications through other methods
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0"))      {
        NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
        // set a member variable to tell the new delegate that this is background
        return;
    }
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
    // custom code to handle notification content
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        NSLog( @"INACTIVE" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {
        NSLog( @"BACKGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else
    {
        NSLog( @"FOREGROUND" );
        
         NSLog( @"HANDLE PUSH, did receive service provider accept: %@", userInfo );
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        RESideMenu *sideMenuViewController;
//        TabBar* tab=[storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
//        
        RESideMenu *sideMenuViewController1;
        
        sideMenu=[storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
        
        HomeViewController *menu =[storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        sideMenuViewController1 = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
        sideMenuViewController1.delegate = self;
        
        [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController1;
        [self.window makeKeyAndVisible];
        completionHandler( UIBackgroundFetchResultNewData );
    }
    
}

//push notification delegate methods
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification  withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler API_AVAILABLE(ios(10.0)){
    
//    [self.progTimer invalidate];
//     [SVProgressHUD dismiss];
    NSLog(@"User Info foreground : %@",notification.request.content.userInfo);
    NSDictionary * userInfo =notification.request.content.userInfo;
    NSString *infoDict = [[userInfo objectForKey:@"aps"]objectForKey:@"alert"];
    NSLog(@"info dict %@",infoDict);
    NSDictionary *infoDict1 = [[userInfo objectForKey:@"aps"]objectForKey:@"info"];
    NSLog(@"info dict %@",infoDict);
    NSString *title = [infoDict1 objectForKey:@"title"];
    NSString *type = [infoDict1 objectForKey:@"type"];
    
    if ([type isEqualToString:@"RA"]) {
    UIViewController *topRootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topRootViewController.presentedViewController)
    {
        topRootViewController = topRootViewController.presentedViewController;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message: [[userInfo objectForKey:@"aps"]objectForKey:@"alert"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        RESideMenu *sideMenuViewController1;
        
        sideMenu=[storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
        
        HomeViewController *menu =[storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        sideMenuViewController1 = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
        sideMenuViewController1.delegate = self;
        
        [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController1;
        [self.window makeKeyAndVisible];
        [SVProgressHUD dismiss];

    }];
    [alert addAction:ok];
    
    [topRootViewController presentViewController:alert animated:YES completion:nil];
        completionHandler(UNNotificationPresentationOptionSound);
    }else{
         completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionBadge | UNNotificationPresentationOptionAlert);
    }
    
   // [[NSUserDefaults standardUserDefaults]setObject:infoDict forKey:@"alert"];
    
   
//    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[userInfo objectForKey:@"aps"]objectForKey:@"alert"] OnViewController:self completion:nil];
   
    
    
    //    NSDictionary * alertDetails = [[NSDictionary alloc]initWithDictionary:notification.request.content.userInfo];
    //    NSDictionary*someData=[[alertDetails objectForKey:@"aps"]objectForKey:@"alert"];
    //    NSLog(@"somedata values are :%@",someData);
    

    
//    message
//    type
//username
    
    //completionHandler();
    //Called when a notification is delivered to a foreground app.
    
    //NSLog(@"Userinfo %@",response.notification.request.content.userInfo);
    
    // completionHandler(UNNotificationPresentationOptionAlert);
    
    
 
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler  API_AVAILABLE(ios(10.0)){
    
    NSLog( @"for handling push in background" );
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", response.notification.request.content.userInfo);
    
    
    
    
//    NSDictionary * userInfo = response.notification.request.content.userInfo;
//
//    NSDictionary *infoDict = [[userInfo objectForKey:@"aps"]objectForKey:@"info"];
//    NSLog(@"info dict %@",infoDict);
    
    NSDictionary * userInfo =response.notification.request.content.userInfo;
    
    NSDictionary *infoDict = [[userInfo objectForKey:@"aps"]objectForKey:@"info"];
    NSLog(@"info dict %@",infoDict);
    
    NSString *reqIdStr = [infoDict objectForKey:@"request_id"];
    NSString *reqTypeStr = [infoDict objectForKey:@"type"];
    NSString *usernameStr = [infoDict objectForKey:@"username"];
    NSString *msgStr = [infoDict objectForKey:@"message"];
    NSString *image = [infoDict objectForKey:@"image"];
    
    [[NSUserDefaults standardUserDefaults]setObject:reqIdStr forKey:@"reqID"];
    [[NSUserDefaults standardUserDefaults]setObject:reqTypeStr forKey:@"reqTypeStr"];
    [[NSUserDefaults standardUserDefaults]setObject:usernameStr forKey:@"nameStr"];
    [[NSUserDefaults standardUserDefaults]setObject:msgStr forKey:@"msgStr"];
    [[NSUserDefaults standardUserDefaults]setObject:image forKey:@"imageStr"];
    
    //NSString *reqIdStr = [infoDict objectForKey:@"request_id"];
    
    // if ([[infoDict objectForKey:@"sender_type"] isEqualToString:@"user"]) {
    
    
    if ([reqTypeStr isEqualToString:@"WC"]) {
       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     RatingView*rate = [storyboard instantiateViewControllerWithIdentifier:@"RatingView"];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:rate];
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    }
       completionHandler();
    
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
    NSString *tokenString = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    tokenString = [tokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"DeviceToken For User: %@", tokenString);
    
    [[NSUserDefaults standardUserDefaults]setObject:tokenString forKey:@"device_token"];
    NSLog(@" device_token %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"device_token"]);
    
    
    //    [[NSNotificationCenter defaultCenter]postNotificationName:@"messageSent" object:nil];
    //    [[NSNotificationCenter defaultCenter]postNotificationName:@"showMsgCount" object:nil];
 
    
}

-(void)autoLogin{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RESideMenu *sideMenuViewController;
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"]!=NULL) {
       
//        PackagesVC *packvc=[storyboard instantiateViewControllerWithIdentifier:@"PackagesVC"];
//        packvc.packString=@"fromAppDel";
//       // UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:rate];
//        self.window.rootViewController = packvc;
//        [self.window makeKeyAndVisible];
    
        TabBar* tab=[storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        
        RESideMenu *sideMenuViewController;
        
        sideMenu=[storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
        
        HomeViewController *menu =[storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
        sideMenuViewController.delegate = self;
        
        [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
        [self.window makeKeyAndVisible];
        //[self.navigationController pushViewController:tab animated:YES];
        
        
    }
    else{
        LogInVC *loginvc=[storyboard instantiateViewControllerWithIdentifier:@"LogInVC"];
        sideMenuViewController.contentViewController =[[UINavigationController alloc]initWithRootViewController:loginvc];
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


/*
 
 User Info : {
 aps =     {
 alert = "Work Completed by andspggg";
 badge = 1;
 info =         {
 body = "Work Completed by andspggg";
 date = "2018-08-09 14:23:04 PM";
 image = "http://volive.in/engineering/assets/uploads/sp_profile_images/364_1533384334.jpg";
 message = "Work Completed by andspggg";
 "request_id" = 162;
 title = "Work Completed ";
 type = WC;
 "user_id" = 364;
 username = andspggg;
 };
 sound = default;
 };
 }

 
 
 */

@end

//com.volivesolutions.Technique
