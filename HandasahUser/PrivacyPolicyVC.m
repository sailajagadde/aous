//
//  PrivacyPolicyVC.m
//  HandasahUser
//
//  Created by MuraliKrishna on 11/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "PrivacyPolicyVC.h"
#import "RESideMenu.h"
#import "SlideMenuVC.h"

@interface PrivacyPolicyVC ()<RESideMenuDelegate>

@end

@implementation PrivacyPolicyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Privacy Policy"];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self ServiceCallForPrivacy];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back_action:(id)sender {
    
    self.back_btn.target =self;
    self.back_btn.action = @selector(presentLeftMenuViewController:);
    
}


-(void)ServiceCallForPrivacy{
    // http://volive.in/engineering/user_services/privacy
    //lang
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
   
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"privacy" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
//                self.privacy_TV.text = [[jsonDict objectForKey:@"data"]objectForKey:@"privacy"];
                NSString *privacyStr = [[[jsonDict objectForKey:@"data"]objectForKey:@"privacy"]objectForKey:@"content_en"];
                
                NSAttributedString *attString = [[NSAttributedString alloc] initWithData:[privacyStr dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                
                NSLog(@"des str %@",privacyStr);
                
                if ([str isEqualToString:@"2"]) {
                     self.privacy_TV.textAlignment = NSTextAlignmentRight;
                } else{
                     self.privacy_TV.textAlignment = NSTextAlignmentLeft;
                    
                }
                
                self.privacy_TV.attributedText = attString;
                
                //self.privacy_TV.text = [[jsonDict objectForKey:@"data"]objectForKey:@"privacy"];
                
                
            });
            
        }
        
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}


@end
