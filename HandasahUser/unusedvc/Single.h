//
//  Single.h
//  HandasahUser
//
//  Created by MuraliKrishna on 22/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Single : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *single_tableView;

- (IBAction)makePayment_btnAction:(id)sender;
- (IBAction)viewDetails_btnAction:(id)sender;


@end
