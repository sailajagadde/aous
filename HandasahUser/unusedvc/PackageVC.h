//
//  PackageVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageVC : UIViewController
- (IBAction)btn_Back:(id)sender;
- (IBAction)btn_1stPackage:(id)sender;
- (IBAction)btn_Request:(id)sender;

@end
