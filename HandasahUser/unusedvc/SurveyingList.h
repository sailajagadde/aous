//
//  SurveyingList.h
//  HandasahUser
//
//  Created by Apple on 5/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveyingList : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSArray * surveyingNamesArr;

}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *backBtn;
@property (strong, nonatomic) IBOutlet UILabel *surveyingLbl;
@property (strong, nonatomic) IBOutlet UITableView *surveyingTableObj;
- (IBAction)backBtnAction:(id)sender;

@end
