//
//  Single.m
//  HandasahUser
//
//  Created by MuraliKrishna on 22/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "Single.h"
#import "MyRequestCell.h"

#import "OrderDetailsVC.h"

@interface Single (){
    MyRequestCell *cell;
}


@end

@implementation Single

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
   cell = [_single_tableView dequeueReusableCellWithIdentifier:@"MyRequestCell" forIndexPath:indexPath];
    
    
//    if (cell == nil) {
//        cell = [[MyRequestCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyRequestCell"];
//        
//    }
    cell.back_view.layer.cornerRadius = 10;
    cell.back_view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.back_view.clipsToBounds = YES;
    //
    //    [cell.makePaymentBtn  addTarget:self action:@selector(gotoViewDetails) forControlEvents:UIControlEventTouchUpInside];
    //
    //    [cell.viewDetailsBtn  addTarget:self action:@selector(gotoViewDetails1) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    
    
    cell.makePaymentBtn.layer.cornerRadius = 4;
    //  self.english_Btn.layer.shadowOffset = CGSizeMake(1, 1);
    // self.english_Btn.layer.shadowRadius = 3.0;
    // self.english_Btn.layer.shadowOpacity = 0.6;
    cell.makePaymentBtn.layer.masksToBounds = YES;
    cell.makePaymentBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    cell.makePaymentBtn.layer.borderWidth = 1.0;
    cell.makePaymentBtn.layer.cornerRadius =20;
    
    
    
    
    cell.viewDetailsBtn.layer.cornerRadius = 4;
    //  self.english_Btn.layer.shadowOffset = CGSizeMake(1, 1);
    // self.english_Btn.layer.shadowRadius = 3.0;
    // self.english_Btn.layer.shadowOpacity = 0.6;
    cell.viewDetailsBtn.layer.masksToBounds = YES;
    cell.viewDetailsBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    cell.viewDetailsBtn.layer.borderWidth = 1.0;
    cell.viewDetailsBtn.layer.cornerRadius =20;
    
    //  cell.textLabel.text=[engineerNammesArr objectAtIndex:indexPath.row];
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 276;
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)makePayment_btnAction:(id)sender {
    
    OrderDetailsVC *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailsVC"];
    [self.navigationController pushViewController:detailsVC animated:YES];
}

- (IBAction)viewDetails_btnAction:(id)sender {
    
    OrderDetailsVC *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailsVC"];
    [self.navigationController pushViewController:detailsVC animated:YES];
    
}
@end
