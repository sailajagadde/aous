//
//  SingleVisitVC.m
//  HandasahUser
//
//  Created by Suman Guntuka on 16/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "SingleVisitVC.h"
#import "AdressViewController.h"
#import "FSCalendar.h"


@interface SingleVisitVC ()

@end

@implementation SingleVisitVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_btn:(id)sender {
    
   [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submitAction:(id)sender {
    
    AdressViewController *adress = [self.storyboard instantiateViewControllerWithIdentifier:@"AdressViewController"];
    
    [self.navigationController pushViewController:adress animated:YES];
    
    
}
@end
