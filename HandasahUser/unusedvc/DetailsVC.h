//
//  DetailsVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsVC : UIViewController
- (IBAction)backAction:(id)sender;
- (IBAction)btn_Package:(id)sender;

- (IBAction)btn_SingleVisit:(id)sender;
@end
