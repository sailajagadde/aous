//
//  PackageVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "PackageVC.h"
#import "FirstVisitViewController.h"
#import "AdressViewController.h"

@interface PackageVC ()

@end

@implementation PackageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)viewWillAppear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden = YES;
//}
//
//-(void)viewWillDisappear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden = NO;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_Back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_1stPackage:(id)sender {
    
    FirstVisitViewController *gotoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FirstVisitViewController"];
    [self.navigationController pushViewController:gotoVC animated:YES];
    
}

- (IBAction)btn_Request:(id)sender {
    
    AdressViewController *gotoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AdressViewController"];
    [self.navigationController pushViewController:gotoVC animated:YES];
}
@end
