//
//  MobileNumberVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MobileNumberVC : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIScrollView *scroll_view;

@property (weak, nonatomic) IBOutlet UITextField *countryCode_TF;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber_TF;

@property (weak, nonatomic) IBOutlet UIButton *next_btn;

@property (weak, nonatomic) IBOutlet UILabel *receiveSms_label;
@end
