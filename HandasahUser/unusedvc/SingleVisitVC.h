//
//  SingleVisitVC.h
//  HandasahUser
//
//  Created by Suman Guntuka on 16/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleVisitVC : UIViewController
- (IBAction)btn_back:(id)sender;
- (IBAction)submitAction:(id)sender;

@end
