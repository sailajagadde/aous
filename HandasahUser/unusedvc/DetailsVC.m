//
//  DetailsVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "DetailsVC.h"
#import "PackageVC.h"
#import "SingleVisitVC.h"

@interface DetailsVC ()

@end

@implementation DetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_Package:(id)sender {
    
    
    PackageVC *packageRequest = [self.storyboard instantiateViewControllerWithIdentifier:@"PackageVC"];
    [self.navigationController pushViewController:packageRequest animated:TRUE];
}

- (IBAction)btn_SingleVisit:(id)sender {
    
    SingleVisitVC *packageRequest = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleVisitVC"];
    [self.navigationController pushViewController:packageRequest animated:TRUE];
}


@end
