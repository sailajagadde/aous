//
//  MobileNumberVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "MobileNumberVC.h"
#import "PhoneVerificationVC.h"


@interface MobileNumberVC (){
    UIToolbar *pickerToolBar;
    UIPickerView *pickerView;
    NSMutableArray *cityNamesArray;
    NSMutableArray *pickerViewItems_Array;

}

@end

@implementation MobileNumberVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadAttributes];
    
}

-(void)loadAttributes{
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];

    UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back-1"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    back.tintColor = [UIColor blackColor];
    [self.navigationItem setLeftBarButtonItem:back];
}

-(void)loadPickerData{
    
    pickerView = [[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    
    self.countryCode_TF.inputView = pickerView;
    
    pickerView.showsSelectionIndicator = YES;
    
    [pickerView setFrame:CGRectMake(0, self.view.frame.size.height-162, self.view.frame.size.width, 162)];
    
    pickerToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolBar.barStyle = UIBarStyleBlackOpaque;
    
    [pickerToolBar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc]init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    //  UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDoneClicked)];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Done"] style: UIBarButtonItemStyleDone target: self action: @selector(pickerDoneClicked)];
    
    [doneBtn setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style: UIBarButtonItemStyleDone target: self action: @selector(pickerCancelClicked)];
    
    //UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pickerCancelClicked)];
    
    [cancelBtn setTintColor:[UIColor whiteColor]];
    
    [barItems addObject:cancelBtn];
    [barItems addObject:flexSpace];
    [barItems addObject:doneBtn];
    
    [pickerToolBar setItems:barItems];
    
    [self.countryCode_TF setInputAccessoryView:pickerToolBar];
    
    
    
}

-(void)pickerDoneClicked{
    self.countryCode_TF.text = [pickerViewItems_Array objectAtIndex:[pickerView selectedRowInComponent:0]];
    [self.countryCode_TF resignFirstResponder];
}

-(void)pickerCancelClicked{
    
    
    [self.countryCode_TF resignFirstResponder];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return pickerViewItems_Array.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.countryCode_TF.text = [pickerViewItems_Array objectAtIndex:row];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return pickerViewItems_Array[row];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}



-(void)dismissKeyboard{
    
    [self.countryCode_TF resignFirstResponder];
    [self.phoneNumber_TF resignFirstResponder];
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == self.countryCode_TF){
        
        self.scroll_view.scrollEnabled = YES;
        [self.scroll_view setContentOffset:CGPointMake(0, textField.frame.origin.y+100) animated:YES];
        
        BOOL checkNetwork = [[SharedClass sharedInstance]connected];
        if(checkNetwork == false){
            
            NSLog(@"unreachable");
            
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Check Your Network Connection"] OnViewController:self completion:nil];
            
        }
        else{
            NSLog(@"reachable");
            //[self getCitysList];
        }
        
    }
    
     else if (textField == self.phoneNumber_TF){
        
        self.scroll_view.scrollEnabled = YES;
        [self.scroll_view setContentOffset:CGPointMake(0, textField.frame.origin.y+160) animated:YES];
         
    }
    
    return YES;
    
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    BOOL returnValue = NO;
    
    if (textField == self.countryCode_TF) {
        [self.phoneNumber_TF becomeFirstResponder];
        returnValue = YES;
    }
    if (textField == self.phoneNumber_TF) {
        [self.phoneNumber_TF resignFirstResponder];
        returnValue = YES;
    }
    [self.scroll_view setContentOffset:CGPointMake(0, - self.scroll_view.contentInset.top) animated:YES];
    return returnValue;
    
}



- (IBAction)nextAction:(id)sender {
    PhoneVerificationVC *verification = [self.storyboard instantiateViewControllerWithIdentifier:@"PhoneVerificationVC"];
    [self.navigationController pushViewController:verification animated:YES];
    
}
-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



//-(void)getCitysList{
//
//    //http://voliveafrica.com/beyond/api/Mobile_Services/citys
//
//    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
//
//    [[SharedClass sharedInstance]showProgressFor:@"Please Wait"];
//
//    [[SharedClass sharedInstance]getResponseFromServerMethod:@"citys" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
//
//        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
//        NSLog(@"json data %@",jsonDict);
//
//        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
//
//        if([status boolValue] == true){
//
//            NSArray * citysArray = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"citys"]];
//
//            cityNamesArray = [[NSMutableArray alloc]init];
//
//            for (int i = 0; i<citysArray.count; i++) {
//
//
//                NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
//
//                if ([str isEqualToString:@"2"]) {
//
//                    [cityNamesArray addObject:[[citysArray objectAtIndex:i]objectForKey:@"name_ar"]];
//
//                } else{
//
//                    [cityNamesArray addObject:[[citysArray objectAtIndex:i]objectForKey:@"name"]];
//
//                }
//
//                //[cityNamesArray addObject:[[citysArray objectAtIndex:i] objectForKey:@"name"]];
//
//            }
//
//            pickerViewItems_Array = [[NSMutableArray alloc]initWithArray:cityNamesArray];
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                [pickerView reloadAllComponents];
//                [pickerView reloadInputViews];
//                [SVProgressHUD dismiss];
//            });
//
//        }
//
//        else{
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                [SVProgressHUD dismiss];
//                // [[SharedClass sharedInstance]showProgressFor:[jsonDict objectForKey:@"message"]];
//                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:[[SharedClass sharedInstance]languageSelectedString:@"message"]] OnViewController:self completion:nil];
//            });
//
//        }
//
//    }];
//
//}
//
//




@end
