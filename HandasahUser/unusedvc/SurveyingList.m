//
//  SurveyingList.m
//  HandasahUser
//
//  Created by Apple on 5/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "SurveyingList.h"
#import "SurveyingTableViewCell.h"
#import "FiltersVC.h"
#import "DetailsVC.h"


@interface SurveyingList ()

@end

@implementation SurveyingList

- (void)viewDidLoad {
    [super viewDidLoad];
    
    surveyingNamesArr=@[@"Alhusain Shamoon",@"Alhusain Shamoon ",@"Alhusain Shamoon",@"Alhusain Shamoon"];

    // Do any additional setup after loading the view.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 6;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    SurveyingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SurveyingCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[SurveyingTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SurveyingCell"];
    }
    
    
    cell.viewBtn.layer.cornerRadius = 12;
    
    cell.viewBtn.layer.masksToBounds = YES;
    cell.viewBtn.layer.borderColor = [[UIColor colorWithRed:62.0f/255.0f green:180.0f/255.0f blue:213.0f/255.0f alpha:1.0] CGColor];
    cell.viewBtn.layer.borderWidth = 1.0;
    
    
     [cell.viewBtn addTarget:self action:@selector(gotoViewDetails) forControlEvents:UIControlEventTouchUpInside];
    
    //  cell.textLabel.text=[engineerNammesArr objectAtIndex:indexPath.row];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 150;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)gotoViewDetails
{
    DetailsVC *details = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsVC"];
    [self.navigationController pushViewController:details animated:TRUE];
}


- (IBAction)filterAction:(id)sender {
    
    FiltersVC *filter = [self.storyboard instantiateViewControllerWithIdentifier:@"FiltersVC"];
    [self.navigationController pushViewController:filter animated:YES];
}

- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
