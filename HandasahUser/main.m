//
//  main.m
//  HandasahUser
//
//  Created by Apple on 5/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
