//
//  VisitFirstVC.m
//  HandasahUser
//
//  Created by MuraliKrishna on 07/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "VisitFirstVC.h"
#import "AdressViewController.h"
#import <FSCalendar/FSCalendar.h>

#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>

#import <MobileCoreServices/MobileCoreServices.h>
#import <QBImagePickerController/QBImagePickerController.h>

@interface VisitFirstVC ()<RESideMenuDelegate>{
    UIDatePicker *datePicker;
    UIToolbar *toolbar;
    
    NSString *dateFormatString;
    NSString *todayDate;
    
    UITextField * current_TF;
    
    NSString *selDateStr;
    NSMutableArray * newFinalArray;
    NSMutableArray *visitingArray;
    
    
    CLLocationManager * locationManager;
    CLLocationCoordinate2D center;
    
    CLLocation *currentLocation;
    //NSDictionary * jsonBodyDict;
    
    NSString *latitudeStr;
    NSString *longitudeStr;
    BOOL map;
    GMSCameraPosition *camera;
    
    RESideMenu * sideMenu;
    UIToolbar *pickerToolBar;
    UIPickerView *pickerView;
    NSMutableArray *nameArray;
    NSMutableArray *idArray;
    NSMutableArray *subservicecostArray;
    NSString *landTypeID;
    NSString *costStr;
    int value;
    NSMutableArray *images;
    NSString *langStr;
    
    UITextField *nameField;
    UITextField *emailField;
    UITextField *passwordField;
    int pickerCheckValue;
    int _selectedPickerCheckValue;
    
    UIImage *getImage;
    NSURL *pickVideoUrl;
   
    AVAudioMix *getMix;
    AVAsset *videoAsset;
    
    NSString *getVideo;
}

@end

@implementation VisitFirstVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.requestOptions = [[PHImageRequestOptions alloc] init];
    self.requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    self.requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    self.requestOptions.synchronous = YES;
    
    self.mediarequestOptions = [[PHVideoRequestOptions alloc]init];
    self.mediarequestOptions.deliveryMode = PHVideoRequestOptionsVersionCurrent;
    
   
    
    [self loadAttributes];
    value = 1;
   
    if ([costStr intValue]>1) {
        _minusOutlet.userInteractionEnabled = YES;
        _plusOutlet.userInteractionEnabled = YES;
    }
    else{
        _minusOutlet.userInteractionEnabled = NO;
        _plusOutlet.userInteractionEnabled = NO;
    }
    
    
    if ([self.subServiceFlagStr intValue] == 1) {
        self.chooseServiceTF.hidden=NO;
        self.stackViewIncrement.hidden=NO;
        self.dropDownBtn.hidden=NO;
        //self.chooseServiceTFTopConstraint.constant=15;
        self.descTopConstraint.constant=15;
    }
    else{
        self.chooseServiceTF.hidden=YES;
        self.stackViewIncrement.hidden=YES;
        self.dropDownBtn.hidden=YES;
        //self.chooseServiceTFTopConstraint.constant=0;
        self.descTopConstraint.constant=-40;
    }
    
    self.quantityLabel.text = [NSString stringWithFormat:@"%d",value];
    self.heightConstraint.constant =200;
    FSCalendarScope selectedScope = FSCalendarScopeWeek;
    [self.calView setScope:selectedScope animated:YES];
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Visit"];
    self.descLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Description"];
    self.time_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Please Choose Time"];
    [self.request_Btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"REQUEST"] forState:UIControlStateNormal];
    
    self.noteStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Note:Price not included with delivery charges"];
    
    self.desc_TV.layer.borderColor = [UIColor grayColor].CGColor;
    self.desc_TV.layer.borderWidth = 1.0;
    self.desc_TV.layer.cornerRadius = 2;
    
    self.desc_TV.returnKeyType = UIReturnKeyDone;
    
    datePicker = [[UIDatePicker alloc]init];
    toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Done"] style: UIBarButtonItemStyleDone target: self action: @selector(doneBtn)];
    [doneButton setTintColor:[UIColor whiteColor]];
    UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style: UIBarButtonItemStyleDone target: self action: @selector(cancelBtnClicked)];
    [cancelButton setTintColor:[UIColor whiteColor]];
    toolbar.items = [NSArray arrayWithObjects:cancelButton,flexibleSpace,doneButton, nil];
    
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    selDateStr = [dateFormatter stringFromDate:[NSDate date]];
    NSLog(@"sel %@",selDateStr);
//    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
//    [self. addGestureRecognizer:gesture];

}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
    

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatString = @"YYYY-MM-dd";
    [dateFormatter setDateFormat:dateFormatString];
    
    NSLog(@"selected date %@",date);
    selDateStr = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    //[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]]? [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]]:@"";
    //
    NSLog(@"seledatestr is %@",selDateStr);
    
    
}

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    self.heightConstraint.constant = CGRectGetHeight(bounds);
    // Do other updates here
    [self.view layoutIfNeeded];
}

-(void)loadAttributes{
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Visit"];
    self.descLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Description"];
    self.time_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Please Enter Time"];
    [self.request_Btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"REQUEST"] forState:UIControlStateNormal];
    
    self.chooseDate_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Choose Date"];
      self.chooseTime_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Choose Time"];
    self.chooseServiceTF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Choose Service"];
    [self.uploadPhotoButton setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Upload Photo/Video"] forState:UIControlStateNormal];
    //_PleaseEnterDescLabel.text = [[SharedClass sharedInstance] languageSelectedString:@"Please Enter Description"];
    
    self.desc_TV.layer.borderColor = [UIColor grayColor].CGColor;
    self.desc_TV.layer.borderWidth = 1.0;
    self.desc_TV.layer.cornerRadius = 2;
    
    
    UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_wh"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    back.tintColor = [UIColor whiteColor];
    [self.navigationItem setLeftBarButtonItem:back];
    [self dateAndTime];
   
    if ([self.subServiceFlagStr intValue] == 1) {
        [self loadPickerData];
    }
    else{
        
    }
    
    
    //self.desc_TV.text = self.descStr;
    
}

-(void)dismissKeyboard{
    
    [self.descriptionTV resignFirstResponder];
    
    
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


//-(void)doneBtn{
//
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:dateFormatString];
//
//    if (current_TF == self.time_TF){
//
//        NSString *stringForTime = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:datePicker.date]];
//        if([stringForTime isEqualToString:@"00:00"]){
//
//            self.time_TF.text = @"12:00";
//        }
//        else{
//            self.time_TF.text = [dateFormatter stringFromDate:datePicker.date];
//        }
//
//    }
//
//    [self.time_TF resignFirstResponder];
//
//}





//new

-(void)dateAndTime{
    
    datePicker = [[UIDatePicker alloc]init];
    
    toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style: UIBarButtonItemStyleDone target: self action: @selector(doneBtn)];
    [doneButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style: UIBarButtonItemStyleDone target: self action: @selector(cancelBtnClicked)];
    [cancelButton setTintColor:[UIColor whiteColor]];
    
    toolbar.items = [NSArray arrayWithObjects:cancelButton,flexibleSpace,doneButton, nil];
    
}


-(void)doneBtn{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatString];
    
    if(current_TF == self.chooseDate_TF){
        
        self.chooseDate_TF.text = [dateFormatter stringFromDate:datePicker.date];
        
    }  else if (current_TF == self.chooseTime_TF){
        
        NSString *stringForTime = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:datePicker.date]];
        if([stringForTime isEqualToString:@"00:00"]){
            
            self.chooseTime_TF.text = @"12:00";
        }
        else{
            self.chooseTime_TF.text = [dateFormatter stringFromDate:datePicker.date];
        }
        
    }
    
    [self.chooseDate_TF resignFirstResponder];
    [self.chooseTime_TF resignFirstResponder];
    
}

-(void)cancelBtnClicked{
    [self.chooseDate_TF resignFirstResponder];
    [self.chooseTime_TF resignFirstResponder];
}


-(void)viewWillAppear:(BOOL)animated{
    
    latitudeStr = NULL;
    longitudeStr = NULL;
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.adressTextview setContentOffset:CGPointZero animated:NO];
    [self.descriptionTV setContentOffset:CGPointZero animated:NO];
}
//-(void)mapsCalling
//{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[NSString stringWithFormat:@"%@",self.lat] floatValue]
//                                                                longitude:[[NSString stringWithFormat:@"%@",self.lng]floatValue]
//                                                                     zoom:14];
//
//        GMSMapView *mapView = [GMSMapView mapWithFrame:self.mapView.bounds camera:camera];
//        mapView.myLocationEnabled = YES;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [_mapView addSubview:mapView];
//
//        });
//        mapView.delegate= self;
//        mapView.myLocationEnabled = YES;
//
//
//        map=YES;
//    });
//
//
//}

-(void)loadMapView{
    
    //    CLLocationCoordinate2D center;
    center.latitude = [latitudeStr doubleValue];
    center.longitude = [longitudeStr doubleValue];
    //camera = [GMSCameraPosition cameraWithLatitude:[self.lat doubleValue]  longitude:[self.lng doubleValue] zoom:16];
    camera = [GMSCameraPosition cameraWithLatitude:center.latitude longitude:center.longitude zoom:16];
    [_gmsGoogleMapView setCamera:camera];
    _gmsGoogleMapView.myLocationEnabled = YES;
    _gmsGoogleMapView.delegate = self;
    
    //self.house_TF.text = @"7-1-28/4/6";
    
}

-(void)locationManager:(CLLocationManager* )manager didUpdateLocations:(NSArray<CLLocation *  > *)locations
{
    
    //    let userLocation:CLLocation = locations[0] as CLLocation
    //    self.currentLocation = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
    //    let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation.latitude, longitude:currentLocation.longitude, zoom: 15)
    //
    //    let position = CLLocationCoordinate2D(latitude:  currentLocation.latitude, longitude: currentLocation.longitude)
    //    print(position)
    //
    //    //self.setupLocationMarker(coordinate: position)
    //
    //    DispatchQueue.main.async {
    //
    //        self.gmapView.camera = camera
    //
    //    }
    //
    //    self.gmapView?.animate(to: camera)
    //    manager.stopUpdatingLocation()
    currentLocation = [locations lastObject];
    //    center = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
    //    camera = [GMSCameraPosition cameraWithLatitude:center.latitude longitude:center.longitude zoom:15];
    [locationManager stopUpdatingLocation];
    
    if (latitudeStr == NULL && longitudeStr == NULL)
    {
        
        latitudeStr = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
        longitudeStr = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
        
        NSLog(@"lat and long %@%@",latitudeStr,longitudeStr);
        //. dispatch_async(dispatch_get_main_queue(), ^{
        [self loadMapView];
        //});
        
    }
    else{
        
        
        
        
    }
}

- (void) mapView: (GMSMapView *)mapView didChangeCameraPosition: (GMSCameraPosition *)position {
    
    double latitude = mapView.camera.target.latitude;
    double longitude = mapView.camera.target.longitude;
    
    latitudeStr = [NSString stringWithFormat:@"%f",mapView.camera.target.latitude];
    longitudeStr = [NSString stringWithFormat:@"%f",mapView.camera.target.longitude];
    
    CLLocationCoordinate2D addressCoordinates = CLLocationCoordinate2DMake(latitude,longitude);
    
    //[[NSUserDefaults standardUserDefaults]setObject:latitudeStr forKey:@"lat"];
    //[[NSUserDefaults standardUserDefaults]setObject:longitudeStr forKey:@"lang"];
    
    GMSGeocoder* coder = [[GMSGeocoder alloc] init];
    [coder reverseGeocodeCoordinate:addressCoordinates completionHandler:^(GMSReverseGeocodeResponse *results, NSError *error) {
        if (error) {
            
        } else {
            GMSAddress* address = [results firstResult];
            NSLog(@"address is %@",address);
            
            
            
            //            self.country = address.country ? address.country : @"";
            // self.house_TF.text = address.lines ? address.subLocality : @"";
            //            self.pinCode = address.postalCode ? address.postalCode : @"";
            //            self.state = address.administrativeArea ? address.administrativeArea : @"";
            NSArray *arr = [address valueForKey:@"lines"];
            NSString *str1 = [NSString stringWithFormat:@"%lu",(unsigned long)[arr count]];
            
            if ([str1 isEqualToString:@"0"]) {
                self.adressTextview.text = @"";
            }
            else if ([str1 isEqualToString:@"1"]) {
                NSString *str2 = [arr objectAtIndex:0];
                self.adressTextview.text = str2;
            }
            else if ([str1 isEqualToString:@"2"]) {
                NSString *str2 = [arr objectAtIndex:0];
                NSString *str3 = [arr objectAtIndex:1];
                if (str2.length > 1 ) {
                    self.adressTextview.text = [NSString stringWithFormat:@"%@,%@",str2,str3];
                }
                else {
                    self.adressTextview.text = [NSString stringWithFormat:@"%@",str3];
                }
                NSLog(@"address is %@",self.adressTextview.text);
                //self.landMark_TF.text = address.subLocality;
                //self.house_TF.text = address.thoroughfare;
                //self.house_TF.text = str2;
            }
        }
    }];
}


- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


//textfield delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == self.chooseDate_TF){
        
        datePicker.datePickerMode = UIDatePickerModeDate;
        
        NSDate *date = [NSDate date];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierIndian];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        components.day = 0;
        NSDate *newDate = [calendar dateByAddingComponents:components toDate:date options:0];
        
        datePicker.minimumDate = newDate;
        dateFormatString = @"YYYY-MM-dd";
        textField.inputView = datePicker;
        [textField setInputAccessoryView:toolbar];
        
        // self.scroll_view.scrollEnabled = YES;
        
        
        
    }  else if(textField == self.chooseTime_TF){
        
        datePicker.datePickerMode = UIDatePickerModeTime;
        //dateFormatString = @"HH:mm a";
        dateFormatString = @"hh:mm a";
        textField.inputView = datePicker;
        [textField setInputAccessoryView:toolbar];
        
        //self.scroll_view.scrollEnabled = YES;
        
    }
    
   else if(textField == self.chooseServiceTF){
        
        //self.scroll_view.scrollEnabled = YES;
        //[self.scroll_view setContentOffset:CGPointMake(0, textField.frame.origin.y+100) animated:YES];
        
        BOOL checkNetwork = [[SharedClass sharedInstance]connected];
        if(checkNetwork == false){
            
            NSLog(@"unreachable");
            
            //[[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Check Your Network Connection"] OnViewController:self completion:nil];
            
        }
        else{
            NSLog(@"reachable");
            [self ServiceCallForSubServices];
            self.chooseServiceTF.inputView = pickerView;
            
        }
        
    }
    current_TF = textField;
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if(textField == self.chooseDate_TF){
        
        //self.scroll_view.scrollEnabled = NO;
        
    }
   
    else if (textField == self.chooseTime_TF){
        
        //self.scroll_view.scrollEnabled = NO;
        
    }
    
    //[self.scroll_view setContentOffset:CGPointMake(0, -self.scroll_view.contentInset.top) animated:YES];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    BOOL returnValue = NO;
    
    if (textField == self.chooseDate_TF) {
        
        [self.chooseTime_TF becomeFirstResponder];
        
        returnValue = YES;
        
    }
    else if (textField == self.chooseTime_TF) {
        
        [self.chooseTime_TF resignFirstResponder];
        //[self.scroll_view setContentOffset:CGPointMake(0, textField.frame.origin.y+140) animated:YES];
        //[self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];
        
        returnValue = YES;
    }
    
    return returnValue;
}


//new picker


-(void)loadPickerData{
    
    pickerView = [[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    
    //    self.countryCode_TF.inputView = pickerView;
    
    pickerView.showsSelectionIndicator = YES;
    
    [pickerView setFrame:CGRectMake(0, self.view.frame.size.height-162, self.view.frame.size.width, 162)];
    
    pickerToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolBar.barStyle = UIBarStyleBlackOpaque;
    
    [pickerToolBar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc]init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    //  UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDoneClicked)];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Done"] style: UIBarButtonItemStyleDone target: self action: @selector(pickerDoneClicked)];
    
    [doneBtn setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style: UIBarButtonItemStyleDone target: self action: @selector(pickerCancelClicked)];
    
    //UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pickerCancelClicked)];
    
    [cancelBtn setTintColor:[UIColor whiteColor]];
    
    [barItems addObject:cancelBtn];
    [barItems addObject:flexSpace];
    [barItems addObject:doneBtn];
    
    [pickerToolBar setItems:barItems];
    
    [self.chooseServiceTF setInputAccessoryView:pickerToolBar];
    [self ServiceCallForSubServices];
    
    
}

-(void)pickerDoneClicked{
    
    self.chooseServiceTF.text = [nameArray objectAtIndex:[pickerView selectedRowInComponent:0]];
    landTypeID = [idArray objectAtIndex:[pickerView selectedRowInComponent:0]];
    costStr = [subservicecostArray objectAtIndex:[pickerView selectedRowInComponent:0]];
    self.costLabel.text = costStr;
    
    
    if ([costStr intValue]>1) {
        _minusOutlet.userInteractionEnabled = YES;
        _plusOutlet.userInteractionEnabled = YES;
        NSLog(@"cost");
    }
    else{
        _minusOutlet.userInteractionEnabled = NO;
        _plusOutlet.userInteractionEnabled = NO;
        NSLog(@"no cost");
    }
   
    [self.chooseServiceTF resignFirstResponder];
}

-(void)pickerCancelClicked{
    
    self.chooseServiceTF.text =@"";
    if ([self.chooseServiceTF.text isEqualToString:@""]){
        self.minusOutlet.userInteractionEnabled=NO;
        self.plusOutlet.userInteractionEnabled=NO;
        self.quantityLabel.text = @"1";
        self.costLabel.text = @"";
    }
    else{
        self.minusOutlet.userInteractionEnabled=YES;
        self.plusOutlet.userInteractionEnabled=YES;
        
    }
    
    
    [self.chooseServiceTF resignFirstResponder];
    
    
    
    
    
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return nameArray.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.chooseServiceTF.text = [nameArray objectAtIndex:row];
    landTypeID = [idArray objectAtIndex:row];
    costStr = [subservicecostArray objectAtIndex:row];
    self.costLabel.text = costStr;

    
    
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return nameArray[row];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

//MARK:- SERVICE CALL Subservices
-(void)ServiceCallForSubServices{
    
    //http://volive.in/aous/user_services/get_sub_services
    //lang, service_id
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    [valuesDictionary setObject:self.serIDStr forKey:@"service_id"];
    
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"get_sub_services" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            NSArray * dataArray = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"sub_services"]];
            
            nameArray = [[NSMutableArray alloc]init];
            idArray = [[NSMutableArray alloc]init];
            subservicecostArray = [[NSMutableArray alloc]init];
            
            for (int i = 0; i<dataArray.count; i++) {
                
                [nameArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"sub_service"]];
                [idArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"sub_service_id"]];
                [subservicecostArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"sub_service_cost"]];
                //sub_service_cost,main_service_id
            }
            
            //pickerViewItems_Array = [[NSMutableArray alloc]initWithArray:phoneCodeArray];
            //pickerViewItems_Array = [[NSMutableArray alloc]initWithArray:phonecode_idArray];
            
            
            NSLog(@"id array %@",nameArray);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [pickerView reloadAllComponents];
                [pickerView reloadInputViews];
                [SVProgressHUD dismiss];
            });
            
        }
        
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                // [[SharedClass sharedInstance]showProgressFor:[jsonDict objectForKey:@"message"]];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
            });
            
        }
        
    }];
    
}

//calculate cost


-(void)ServiceCallForCalculateCost{
    
    //http://volive.in/aous/user_services/calculate_cost
    //lang, quantity, cost
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    [valuesDictionary setObject:costStr forKey:@"cost"];
    [valuesDictionary setObject:self.quantityLabel.text forKey:@"quantity"];
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"calculate_cost" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
            self.costLabel.text = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"total_cost"]];
                });
            //total_cost
            //pickerViewItems_Array = [[NSMutableArray alloc]initWithArray:phoneCodeArray];
            //pickerViewItems_Array = [[NSMutableArray alloc]initWithArray:phonecode_idArray];
            
        }
       
        
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                // [[SharedClass sharedInstance]showProgressFor:[jsonDict objectForKey:@"message"]];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
            });
            
        }
        
    }];
    
}




//Textfield delegates


//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//
//    if(textField == self.chooseTime_TF){
//
//
//        //[self dateAndTime];
//        datePicker.datePickerMode = UIDatePickerModeTime;
//        dateFormatString = @"hh:mm a";
//        textField.inputView = datePicker;
//        [textField setInputAccessoryView:toolbar];
//
//        //self.scroll_view.scrollEnabled = YES;
//
//    }
//    current_TF = textField;
//    return YES;
//}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)reqBtnAction:(id)sender {
    if ([_chooseDate_TF.text isEqualToString:@""]) {
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Choose Date"] OnViewController:self completion:nil];
        
    }else if ([_chooseTime_TF.text isEqualToString:@""]){
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Choose Time"] OnViewController:self completion:nil];
        // if ([self.subServiceFlagStr intValue] == 1) {
    }else if([self.subServiceFlagStr intValue] == 1){
        if ([self.chooseServiceTF.text isEqualToString:@""]) {
             [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Choose Service"] OnViewController:self completion:nil];
            //self.chooseServiceTF.hidden=NO;
        }
        else{
            NSLog(@"textfield");
            [self ServiceCallForSendRequest];
            //self.chooseServiceTF.hidden=YES;
        }
    }else{
        //self.chooseServiceTF.hidden=YES;
        NSLog(@"flag");
        [self ServiceCallForSendRequest];
    }
    
   
    
    //visits = [ {  visitingtime ,visitingdate }]
    
    
    
//    if(self.chooseTime_TF.text.length<1){
//        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Choose Time"] OnViewController:self completion:nil];
//    }else if(self.desc_TV.text.length <1){
//          [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Enter Description"] OnViewController:self completion:nil];
//    }
//    else{
//
//
//        //newFinalArray = [NSMutableArray new];
//
//       // NSMutableDictionary*detailsDict=[[NSMutableDictionary alloc]init];
//        //[detailsDict setObject:[NSString stringWithFormat:@"%@",selDateStr] forKey:@"requested_date"];
//        //[detailsDict setObject:[NSString stringWithFormat:@"%@",self.time_TF.text] forKey:@"requested_time"];
//        //[detailsDict setObject:[NSString stringWithFormat:@"%@",self.desc_TV.text] forKey:@"description"];
//
//        //[newFinalArray addObject:detailsDict];
//        //NSLog(@"my final array is %@",newFinalArray);
//
//        AdressViewController *adress = [self.storyboard instantiateViewControllerWithIdentifier:@"AdressViewController"];
//        adress.descStr = self.desc_TV.text;
//        adress.reqDateStr=selDateStr;
//        adress.reqTimeStr=self.time_TF.text;
//
//        //adress.newfinalArr=newFinalArray;
//
//        adress.serIdStr=self.serIDStr;
//        [self.navigationController pushViewController:adress animated:YES];
//    }
//
    //[self ServiceCallForSendRequest];
    
    
}
- (IBAction)back_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)minusAction:(id)sender {
    
    
    
    //_quantityLabel.text = [NSString stringWithFormat:@"%d",value];
    
        if ([_quantityLabel.text isEqualToString:@"1"]) {
            _minusOutlet.userInteractionEnabled = NO;
        }
        else{
            value --;
             _quantityLabel.text = [NSString stringWithFormat:@"%d",value];
            [self ServiceCallForCalculateCost];
        }
//    if (value == 1) {
//
//        _minusOutlet.userInteractionEnabled = NO;
//    }
    
}

- (IBAction)plusAction:(id)sender {
    
    
    value ++;
    _quantityLabel.text = [NSString stringWithFormat:@"%d",value];
    [self ServiceCallForCalculateCost];
    if (value > 1) {
        _minusOutlet.userInteractionEnabled = YES;
    }
    
}
- (IBAction)uploadPhotoOrVideoAction:(id)sender {
    
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:nil
//                                  message:nil
//                                  preferredStyle:UIAlertControllerStyleActionSheet];
//    UIAlertAction* Video = [UIAlertAction
//                            actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Video"]
//                            style:UIAlertActionStyleDefault
//                            handler:^(UIAlertAction * action)
//                            {
//                                //_selectedPickerCheckValue = 2;
//
////                                UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
////                                videoPicker.delegate = self; // ensure you set the delegate so when a video is chosen the right method can be called
////
////                                videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
////                                // This code ensures only videos are shown to the end user
////                                videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
////
////                                videoPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
////                                [self presentViewController:videoPicker animated:YES completion:nil];
//
//
//                                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//                                picker.delegate = self;
//                                picker.allowsEditing = YES;
//                               // picker.sourceType = uimagep;
//
//                                [self presentViewController:picker animated:YES completion:nil];
//
//
//                            }];
//
//
//    UIAlertAction* cancel = [UIAlertAction
//                             actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"]
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//                                 [alert dismissViewControllerAnimated:YES completion:nil];
//
//                             }];
//
//
//    [alert addAction:Video];
//    [alert addAction:cancel];
//
//    [self presentViewController:alert animated:YES completion:nil];
    
  /*
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection=YES;
    imagePickerController.assetCollectionSubtypes = @[
                                                      @(PHAssetCollectionSubtypeSmartAlbumUserLibrary), // Camera Roll
                                                      //@(PHAssetCollectionSubtypeAlbumMyPhotoStream), // My Photo Stream
                                                      //                                                      @(PHAssetCollectionSubtypeSmartAlbumPanoramas), // Panoramas
                                                      //                                                      @(PHAssetCollectionSubtypeSmartAlbumVideos), // Videos
                                                      //                                                      @(PHAssetCollectionSubtypeSmartAlbumBursts) // Bursts
                                                      ];
    [self presentViewController:imagePickerController animated:YES completion:NULL];
    */
    
    
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"" message:[[SharedClass sharedInstance]languageSelectedString:@"Choose Photo/Video"] preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* gallery = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Photo"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        _selectedPickerCheckValue = 1;
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:nil];
        
        
        
        
        //[self setLanguage];
        
    }];
    
    UIAlertAction* videoPicker = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Video"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        _selectedPickerCheckValue = 2;
        
        UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
        videoPicker.delegate = self; // ensure you set the delegate so when a video is chosen the right method can be called
        
        videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
        // This code ensures only videos are shown to the end user
        videoPicker.mediaTypes =
  @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
        videoPicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        videoPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
        [self presentViewController:videoPicker animated:YES completion:nil];
        
        
        //[self setLanguage];
    }];
    
    [ac addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [ac dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [ac addAction:gallery];
    [ac addAction:videoPicker];
    [self presentViewController:ac animated:YES completion:nil];
    
    
    
}



#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    if (_selectedPickerCheckValue == 1) // Gallery
    {
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        pickerCheckValue = 1;
        getImage = chosenImage;
        //videoImageBool = true;
        NSLog(@"getimage %@",getImage);
        NSLog(@"File Attached");
        
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        //[self photoVideoServer];
    }
    else if (_selectedPickerCheckValue == 2) // Video
    {
        pickerCheckValue = 2;
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        NSLog(@"VideoURL = %@", videoURL);
        pickVideoUrl = videoURL;
        //videoImageBool = true;
         NSLog(@"getimage %@",pickVideoUrl);
        NSLog(@"File Attached");
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        //[self photoVideoServer];
    }
    
    
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



/*- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
    // for (PHAsset *asset in assets) {
    // Do something with the asset
    
    
    PHImageManager *manager = [PHImageManager defaultManager];
    images = [NSMutableArray arrayWithCapacity:[assets count]];
    
    // assets contains PHAsset objects.
    __block UIImage *ima;
    __block AVAudioMix *mix;
//    for (PHAsset *asset in assets) {
//        // Do something with the asset
//
//        [manager requestImageForAsset:asset
//                           targetSize:PHImageManagerMaximumSize
//                          contentMode:PHImageContentModeDefault
//                              options:self.requestOptions
//                        resultHandler:^void(UIImage *image, NSDictionary *info) {
//                            ima = image;
//
//                            [self->images addObject:ima];
//                        }];
//
//        manager requestAVAssetForVideo:asset options:self.mediarequestOptions resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
//
//        }
//
//        //   }
//        NSLog(@"sel images %@",images);
//        NSLog(@"images selected");
//
        for (PHAsset *asset in assets)
        {
            if (asset.mediaType == PHAssetMediaTypeImage)
            {
                pickerCheckValue=1;
                [manager requestImageForAsset:asset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeDefault options:self.requestOptions resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                    NSLog(@"imag  %@",info);
                    ima = result;
                    getImage = ima;
                    NSLog(@"image %@",ima);
                    
                    
                }];
//                [[PHImageManager defaultManager] requestImageDataForAsset:asset options:nil resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
//                    NSURL *urlimage = [info objectForKey:@"PHImageFileURLKey"];
//                    NSLog(@"info %@",info);
//                    // do what you want with it
//                    NSLog(@"image selected %@",urlimage);
//
////                 UIImage * aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlimage]];
////                    getImage = aImage;
////                    NSLog(@"sel image %@",getImage);
//
//                    NSData *imageselData = [NSData dataWithContentsOfURL:urlimage];
//                 UIImage *image1 = [UIImage imageWithData:imageselData];
//                    NSLog(@"sel image %@",image1);
//                }];
            }
            
//            else if (asset.mediaType == PHAssetMediaTypeVideo)
//                            {
//                                pickerCheckValue=2;
//                                [manager requestAVAssetForVideo:asset options:_mediarequestOptions resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
//
//                                    NSURL *url = [(AVURLAsset*)asset URL];
//                                    NSLog(@"url %@",url);
//                                    //NSData *webData = [NSData dataWithContentsOfURL:url];
//                                    getVideo = [NSString stringWithFormat:@"%@",url];
//                                    NSLog(@"video %@",getVideo);
////                                  videoAsset = [AVAsset assetWithURL:url];
////                                    getVideo = [NSString stringWithFormat:@"%@",videoAsset];
////                                    NSLog(@"video %@",getVideo);
////                                    mix = audioMix;
////                                    NSLog(@"video %@",videoAsset);
////                                    getMix = mix;
////                                    NSLog(@"video %@",getMix);
//                                }];
//
//                            }
            
            
            
            else if (asset.mediaType == PHAssetMediaTypeVideo)
            {
                pickerCheckValue=2;
                [manager requestAVAssetForVideo:asset options:_mediarequestOptions resultHandler:^(AVAsset *asset, AVAudioMix *audioMix, NSDictionary *info)
                 {
                     if ([asset isKindOfClass:[AVURLAsset class]])
                     {
                         // use URL to get file content
                         
                         pickVideoUrl = [(AVURLAsset *)asset URL];
                         NSData* videoData=[NSData dataWithContentsOfURL:pickVideoUrl];
                         NSLog(@"video data %@",videoData);
                         NSLog(@"File size is : %.2f MB",(float)videoData.length/1024.0f/1024.0f);
                         NSNumber *fileSizeValue = nil;
                         [pickVideoUrl getResourceValue:&fileSizeValue forKey:NSURLFileSizeKey error:nil];
                         
                     }
                 }];
            }
//            else if (asset.mediaType == PHAssetMediaTypeVideo)
//            {
//                pickerCheckValue=2;
//                [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset *asset, AVAudioMix *audioMix, NSDictionary *info)
//                 {
//                     if ([asset isKindOfClass:[AVURLAsset class]])
//                     {
//                          NSURL *url = [(AVURLAsset*)asset URL];
//                         // do what you want with it
//                         NSLog(@"video selected %@",url);
//                         pickVideoUrl = url;
//                         //videodata = [NSData dataWithContentsOfURL:url];
//                         NSLog(@"video %@",pickVideoUrl);
//
//                     }
//                 }];
//            }
        }
        
        
   // }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:[[SharedClass sharedInstance] languageSelectedString:@"Alert"]
                                              message:[[SharedClass sharedInstance] languageSelectedString:@"Image Uploaded"]
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* OkAction = [UIAlertAction
                                   actionWithTitle:[[SharedClass sharedInstance] languageSelectedString:@"OK"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       dispatch_async(dispatch_get_main_queue(), ^
                                                      {
                                                          
                                                          
                                                          
                                                      });
                                       
                                   }];
        [alertController addAction:OkAction];
        [self presentViewController:alertController animated:YES completion:nil];
    });
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
*/

//MARK:- SERVICE CALL TO SEND REQUEST
-(void)ServiceCallForSendRequest
{
    //lang,address,house,land_mark,latitude,longitude,service_id,sub_service_id,quantity,user_id,type_of_land,description,url,requested_time,requested_date
    
    NSString *emailText = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSLog(@"email text %@",emailText);
    
    // if(!([emailText length] == 0))  {
    //http://volive.in/engineering/user_services/sending_request
    //       //lang, address, house, land_mark,  latitude, longitude, service_id, user_id, request_type, package_id, visits = [ {  visiting_time ,visiting_date, description  } ]
    
    // lang,address,house,land_mark,latitude,longitude,service_id,sp_user_id,user_id,type_of_land,description,requested_time,requested_date
    
    //NSString *reqTypeStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"typeStr"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        langStr = @"ar";
        
    } else if([str isEqualToString:@"1"]){
        
        langStr = @"en";
        
    }
    else{
        
    }
    
    NSLog(@"reqdate  and request time %@ %@ %@ %@",self.chooseTime_TF.text,self.chooseDate_TF.text,[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"],[[NSUserDefaults standardUserDefaults]objectForKey:@"SPUID"]);
    
    NSLog(@"subserviceflag %@",self.subServiceFlagStr);
    NSMutableDictionary*jsonBodyDict=[[NSMutableDictionary alloc]init];
    
    if ([self.subServiceFlagStr intValue] == 1) {
    [jsonBodyDict setObject:APIKEY forKey:@"API-KEY"];
    [jsonBodyDict setObject:@"" forKey:@"house"];
    [jsonBodyDict setObject:self.adressTextview.text forKey:@"address"];
    [jsonBodyDict setObject:@"" forKey:@"land_mark"];
    [jsonBodyDict setObject:latitudeStr forKey:@"latitude"];
    [jsonBodyDict setObject:longitudeStr forKey:@"longitude"];
    [jsonBodyDict setObject:self.serIDStr forKey:@"service_id"];
    [jsonBodyDict setObject:landTypeID forKey:@"sub_service_id"];
    [jsonBodyDict setObject:self.quantityLabel.text forKey:@"quantity"];
    [jsonBodyDict setObject:self.descriptionTV.text forKey:@"description"];
    [jsonBodyDict setObject:self.chooseTime_TF.text forKey:@"requested_time"];
    [jsonBodyDict setObject:self.chooseDate_TF.text forKey:@"requested_date"];
    [jsonBodyDict setObject:@"" forKey:@"sp_user_id"];
    [jsonBodyDict setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
    [jsonBodyDict setObject:langStr forKey:@"lang"];
    [jsonBodyDict setObject:@"" forKey:@"type_of_land"];
    }
    else{
        [jsonBodyDict setObject:APIKEY forKey:@"API-KEY"];
        [jsonBodyDict setObject:@"" forKey:@"house"];
        [jsonBodyDict setObject:self.adressTextview.text forKey:@"address"];
        [jsonBodyDict setObject:@"" forKey:@"land_mark"];
        [jsonBodyDict setObject:latitudeStr forKey:@"latitude"];
        [jsonBodyDict setObject:longitudeStr forKey:@"longitude"];
        [jsonBodyDict setObject:self.serIDStr forKey:@"service_id"];
        [jsonBodyDict setObject:@"" forKey:@"sub_service_id"];
        [jsonBodyDict setObject:@"" forKey:@"quantity"];
        [jsonBodyDict setObject:self.descriptionTV.text forKey:@"description"];
        [jsonBodyDict setObject:self.chooseTime_TF.text forKey:@"requested_time"];
        [jsonBodyDict setObject:self.chooseDate_TF.text forKey:@"requested_date"];
        [jsonBodyDict setObject:@"" forKey:@"sp_user_id"];
        [jsonBodyDict setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
        [jsonBodyDict setObject:langStr forKey:@"lang"];
        [jsonBodyDict setObject:@"" forKey:@"type_of_land"];
    }
    
//    if ([self.subServiceFlagStr intValue] == 1) {
//        jsonBodyDict = @{
//                         @"API-KEY":APIKEY,
//                         @"house":@"",
//                         @"address":self.adressTextview.text,
//                         @"land_mark":@"",
//                         @"latitude":latitudeStr,
//                         @"longitude":longitudeStr,
//                         @"service_id":self.serIDStr,
//                         @"sub_service_id":landTypeID,
//                         @"quantity":self.quantityLabel.text,
//                         @"description":self.descriptionTV.text,
//                         //@"visits":self.newfinalArr,
//                         @"requested_time":self.chooseTime_TF.text,
//                         @"requested_date":self.chooseDate_TF.text,
//                         @"sp_user_id":@"",
//                         //self.spIDStr,
//                         //[[NSUserDefaults standardUserDefaults]objectForKey:@"SPUID"],
//                         @"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"],
//                         @"lang":langStr,
//                         @"type_of_land":@""
//                         };
//    }
//    else{
//        jsonBodyDict = @{
//                         @"API-KEY":APIKEY,
//                         @"house":@"",
//                         @"address":self.adressTextview.text,
//                         @"land_mark":@"",
//                         @"latitude":latitudeStr,
//                         @"longitude":longitudeStr,
//                         @"service_id":self.serIDStr,
//                         @"sub_service_id":@"",
//                         @"quantity":@"",
//                         @"description":self.descriptionTV.text,
//                         //@"visits":self.newfinalArr,
//                         @"requested_time":self.chooseTime_TF.text,
//                         @"requested_date":self.chooseDate_TF.text,
//                         @"sp_user_id":@"",
//                         //self.spIDStr,
//                         //[[NSUserDefaults standardUserDefaults]objectForKey:@"SPUID"],
//                         @"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"],
//                         @"lang":langStr,
//                         @"type_of_land":@""
//                         };
//    }
//    jsonBodyDict = @{
//                     @"API-KEY":APIKEY,
//                     @"house":@"",
//                     @"address":self.adressTextview.text,
//                     @"land_mark":@"",
//                     @"latitude":latitudeStr,
//                     @"longitude":longitudeStr,
//                     @"service_id":self.serIDStr,
//                     @"sub_service_id":landTypeID,
//                     @"quantity":self.quantityLabel.text,
//                     @"description":self.descriptionTV.text,
//                     //@"visits":self.newfinalArr,
//                     @"requested_time":self.chooseTime_TF.text,
//                     @"requested_date":self.chooseDate_TF.text,
//                     @"sp_user_id":@"",
//                     //self.spIDStr,
//                     //[[NSUserDefaults standardUserDefaults]objectForKey:@"SPUID"],
//                     @"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"],
//                     @"lang":langStr,
//                     @"type_of_land":@""
//                     };
    
    
    
    NSLog(@"my dict is %@",jsonBodyDict);
    
    
    NSString *urlString = @"http://volive.in/aous/user_services/sending_request";
    //[self start];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedString:@"Wait for service provider to accept your request"]];
    
    /*NSData *jsonBodyData = [NSJSONSerialization dataWithJSONObject:jsonBodyDict options:kNilOptions error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    request.HTTPMethod = @"POST";
    
    // for alternative 1:
    [request setURL:[NSURL URLWithString:urlString1]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:jsonBodyData];*/
    
    
    //NSString * urlString = [NSString stringWithFormat:@"%@%@",BaseUrl,@"chat"];
    
    NSString * FileParamConstant = @"url";
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
   // NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
     NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in jsonBodyDict) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [jsonBodyDict objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
    }
    
    
    NSData *imageData;
    if (pickerCheckValue == 1)
    {
        //UIImage *aImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlimage]];
        imageData = UIImageJPEGRepresentation(getImage,1.0); //For image
    }
    else if (pickerCheckValue == 2)
    {
       // imageData = [NSData dataWithContentsOfURL:pickVideoUrl]; // Video
//        NSString *videostring=[NSString stringWithFormat:@"%@",pickVideoUrl];
//        imageData = [NSData dataWithContentsOfFile:videostring];
        
        imageData = [[NSData alloc]initWithContentsOfURL:pickVideoUrl];
        //imageData = [NSData dataWithContentsOfFile:getVideo];
        //videoAsset
        //imageData = [NSData dataWithContentsOfURL:pickVideoUrl];
        // imageData=[NSData dataWithContentsOfURL:pickVideoUrl];
       // imageData = [getVideo dataUsingEncoding:NSUTF8StringEncoding];
      
        //NSData* data = [str dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    //   NSData *data = [[NSData alloc] initWithContentsOfURL:recorder.url]; // Audio Record
    
    if (imageData) {
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        if (pickerCheckValue == 1)
        {
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        
        else  if (pickerCheckValue == 2)
        {
            //[body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"register.mov\"\r\n",FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"register.mov\"\r\n",FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
           // [body appendData:[@"Content-Type: video/mp4\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
//            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\".mp4\"\r\n",FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
    }
    //[request setHTTPBody:body];
    
    
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"MyAudioMemo.m4a\"\r\n\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//    // [body appendData:[@"Content-Type: audio/m4a\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:data];
//    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
//
//    [self.urlRequest setHTTPBody:body];
    
    
    
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSLog(@"The request is %@",request);
    
    
    
    
    
    
    //NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:config
//                                                          delegate:nil
//                                                     delegateQueue:[NSOperationQueue mainQueue]];
//
    
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData * _Nullable data,
                                                                NSURLResponse * _Nullable response,
                                                                NSError * _Nullable error) {
                                                
                                                
                                                NSDictionary *statusDict = [[NSDictionary alloc]init];
                                                statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                
                                                NSLog(@"Images from server %@", statusDict);
                                                
                                                NSString* status=[statusDict objectForKey:@"status"];
                                                int result=[status intValue];
                                                [SVProgressHUD dismiss];
                                                
                                                
                                                NSString *flagstr;
                                                NSLog(@"flag %@",flagstr);
                                                if(result == 1){
                                                    
                                                    //                                                        [timer invalidate];
                                                    //                                                        [SVProgressHUD dismiss];
                                                    flagstr= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"pass_flag"]];
                                                    NSLog(@"flag sttt %@",flagstr);
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        [SVProgressHUD dismiss];
                                                        
                                                        TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                        
                                                        RESideMenu *sideMenuViewController;
                                                        
                                                        sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
                                                        
                                                        HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                        sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
                                                        sideMenuViewController.delegate = self;
                                                        
                                                        [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
                                                        
                                                        [self.navigationController pushViewController:tab animated:YES];
                                                        
                                                        
                                                        
                                                        UIAlertController * alert = [UIAlertController
                                                                                     alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
                                                                                     message:[statusDict objectForKey:@"message"]
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                                        
                                                        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                            
                                                            if ([flagstr isEqualToString:@"1"]) {
                                                                NSLog(@"update ur pwd");
                                                                
                                                                
                                                                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@""
                                                                                                                                          message:[[SharedClass sharedInstance]languageSelectedString:@"Update Your Details"]
                                                                                                                                   preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                                                    // textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Old Password"];
                                                                    textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Enter Password"];
                                                                    textField.textColor = [UIColor blackColor];
                                                                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                                                                    textField.borderStyle = UITextBorderStyleRoundedRect;
                                                                    textField.secureTextEntry = YES;
                                                                    
                                                                }];
                                                               
                                                                
                                                                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                                                    // textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Old Password"];
                                                                    textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Enter Email"];
                                                                    textField.textColor = [UIColor blackColor];
                                                                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                                                                    textField.borderStyle = UITextBorderStyleRoundedRect;
                                                                    //textField.secureTextEntry = YES;
                                                                    
                                                                }];
                                                                
                                                                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                                                    // textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Old Password"];
                                                                    textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Enter UserName"];
                                                                    textField.textColor = [UIColor blackColor];
                                                                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                                                                    textField.borderStyle = UITextBorderStyleRoundedRect;
                                                                    //textField.secureTextEntry = YES;
                                                                    
                                                                }];
                                                                
                                                                alertController.view.tintColor = [UIColor colorWithRed:106.0/255.0 green:189.0/255.0 blue:239.0/255.0 alpha:1];
                                                                [alertController addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"SUBMIT"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                                    
                                                                    NSArray * textfields = alertController.textFields;
                                                                    
                                                                    nameField = textfields[0];
                                                                    passwordField = textfields[1];
                                                                    emailField = textfields[2];
                                                                    
                                                                    NSLog(@"%@",nameField.text);
                                                                    
                                                                    [self forUpdatePassword];
                                                                    //                                                                    if (nameField.text.length>0) {
                                                                    //
                                                                    //                                                                        [self forUpdatePassword];
                                                                    //                                                                    }
                                                                    //                                                                    else{
                                                                    //
                                                                    //                                                                        NSLog(@"ghhgh");
                                                                    //
                                                                    //                                                                    }
                                                                }]];
                                                                
                                                                alertController.view.tintColor = [UIColor colorWithRed:106.0/255.0 green:189.0/255.0 blue:239.0/255.0 alpha:1];
                                                                [self presentViewController:alertController animated:YES completion:nil];
                                                                
                                                                
                                                            }
                                                            else{
                                                                
                                                            }
                                                            
                                                        }];
                                                        
                                                        [alert addAction:yesButton];
                                                        
                                                        [self presentViewController:alert animated:YES completion:nil];
                                                        
                                                        
                                                    });
                                                }
                                                else{
                                                    
                                                    //[self start];
                                                    //                                                        [timer invalidate];
                                                    //                                                        [SVProgressHUD dismiss];
                                                    flagstr= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"pass_flag"]];
                                                    
                                                    TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                    
                                                    RESideMenu *sideMenuViewController;
                                                    
                                                    sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
                                                    
                                                    HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                    sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
                                                    sideMenuViewController.delegate = self;
                                                    
                                                    [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
                                                    
                                                    [self.navigationController pushViewController:tab animated:YES];
                                                    
                                                    
                                                    
                                                    UIAlertController * alert = [UIAlertController
                                                                                 alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
                                                                                 message:[statusDict objectForKey:@"message"]
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                                    
                                                    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                        
                                                        if ([flagstr isEqualToString:@"1"]) {
                                                            NSLog(@"update ur pwd");
                                                            
                                                            
                                                            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@""
                                                                                                                                      message:[[SharedClass sharedInstance]languageSelectedString:@"Update Password"]
                                                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                                                // textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Old Password"];
                                                                textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Enter Password"];
                                                                textField.textColor = [UIColor blackColor];
                                                                textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                                                                textField.borderStyle = UITextBorderStyleRoundedRect;
                                                                textField.secureTextEntry = YES;
                                                                
                                                            }];
                                                            
                                                            
                                                            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                                                // textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Old Password"];
                                                                textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Enter Email"];
                                                                textField.textColor = [UIColor blackColor];
                                                                textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                                                                textField.borderStyle = UITextBorderStyleRoundedRect;
                                                                //textField.secureTextEntry = YES;
                                                                
                                                            }];

                                                            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                                                // textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Old Password"];
                                                                textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Enter UserName"];
                                                                textField.textColor = [UIColor blackColor];
                                                                textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                                                                textField.borderStyle = UITextBorderStyleRoundedRect;
                                                                //textField.secureTextEntry = YES;
                                                                
                                                            }];
                                                            
                                                            alertController.view.tintColor = [UIColor colorWithRed:106.0/255.0 green:189.0/255.0 blue:239.0/255.0 alpha:1];
                                                            [alertController addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"SUBMIT"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                                
                                                                NSArray * textfields = alertController.textFields;
                                                                
                                                                nameField = textfields[0];
                                                                passwordField = textfields[1];
                                                                emailField = textfields[2];
                                                                NSLog(@"%@",nameField.text);
                                                                
                                                                [self forUpdatePassword];
                                                                //                                                                    if (nameField.text.length>0) {
                                                                //
                                                                //                                                                        [self forUpdatePassword];
                                                                //                                                                    }
                                                                //                                                                    else{
                                                                //
                                                                //                                                                        NSLog(@"ghhgh");
                                                                //
                                                                //                                                                    }
                                                            }]];
                                                            
                                                            alertController.view.tintColor = [UIColor colorWithRed:106.0/255.0 green:189.0/255.0 blue:239.0/255.0 alpha:1];
                                                            [self presentViewController:alertController animated:YES completion:nil];
                                                            
                                                            
                                                        }
                                                        else{
                                                            
                                                        }
                                                        
                                                    }];
                                                    
                                                    [alert addAction:yesButton];
                                                    
                                                    [self presentViewController:alert animated:YES completion:nil];
                                                    
                                                    
                                                }
                                                
                                            }];
//    }] resume];
    [task resume];
}


-(void)forUpdatePassword{
    
    //http://volive.in/aous/user_services/create_password
    // lang, password, user_id,email,username
    
    
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    //[[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    [valuesDictionary setObject:nameField.text forKey:@"password"];
    [valuesDictionary setObject:emailField.text forKey:@"username"];
    [valuesDictionary setObject:passwordField.text forKey:@"email"];
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
    
    //[valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"language"] forKey:@"language"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
    } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"create_password" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)data];
        NSLog(@"json dict %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        NSLog(@"sta %@",status);
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[NSUserDefaults standardUserDefaults]setObject:[[jsonDict objectForKey:@"user_info"]objectForKey:@"username"] forKey:@"username"];
                [[NSUserDefaults standardUserDefaults]setObject:[[jsonDict objectForKey:@"user_info"]objectForKey:@"email"] forKey:@"email"];
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:^{ }];
                // [[SharedClass sharedInstance]showProgressForSuccess:[jsonDict objectForKey:@"message"]];
                
            });
            
            
        } else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:^{ }];
                
                [SVProgressHUD dismiss];
                
            });
            
        }
        
    }];
    
    
}



@end



/*
 
 "API-KEY" = 963254;
 address = "Laxmi Niwas, 7-1-28/4/6, Leelanagar, Ameerpet, Hyderabad, Telangana 500016, India";
 description = "rm liber te conscient to factor tum poen legum odioque civiuda.";
 house = "";
 "land_mark" = "";
 lang = en;
 latitude = "17.437503";
 longitude = "78.453194";
 quantity = "";
 "requested_date" = "2018-11-05";
 "requested_time" = "12:28 PM";
 "service_id" = 4;
 "sp_user_id" = "";
 "sub_service_id" = "";
 "type_of_land" = "";
 "user_id" = 514;
 
 
 
 */
