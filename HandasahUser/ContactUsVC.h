//
//  ContactUsVC.h
//  HandasahUser
//
//  Created by MuraliKrishna on 12/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsVC : UIViewController


@property (weak, nonatomic) IBOutlet UIBarButtonItem *back_btn;
- (IBAction)back_action:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *mobileNumber_TF;
@property (weak, nonatomic) IBOutlet UITextField *emailID_TF;
@property (weak, nonatomic) IBOutlet UITextField *address_TF;


@property (weak, nonatomic) IBOutlet UILabel *mobileNumberStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailIDStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressStaticalbel;

@end
