//
//  SingleDetailsVC.h
//  HandasahUser
//
//  Created by MuraliKrishna on 08/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleDetailsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *serImageView;
@property (weak, nonatomic) IBOutlet UILabel *serName;
@property (weak, nonatomic) IBOutlet UILabel *priceStatic;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UITextView *descTV;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
- (IBAction)closeBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
- (IBAction)selectBtnAction:(id)sender;

@end
