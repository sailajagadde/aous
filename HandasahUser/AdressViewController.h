//
//  AdressViewController.h
//  HandasahUser
//
//  Created by Apple on 5/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>


@interface AdressViewController : UIViewController<CLLocationManagerDelegate,GMSMapViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIView *mapView;
@property (strong, nonatomic) IBOutlet GMSMapView *googleMapView;
@property (weak, nonatomic) IBOutlet GMSMapView *gmsGoogleMapView;

@property (strong, nonatomic) NSString *lat;
@property (strong, nonatomic) NSString *lng;
- (IBAction)btn_Back:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *map_marker;

- (IBAction)btn_SendRequest:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *address_TV;

@property (weak, nonatomic) IBOutlet UITextField *house_TF;
@property (weak, nonatomic) IBOutlet UITextField *landMark_TF;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll_view;

@property (weak, nonatomic) IBOutlet UILabel *locationStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *adressLabel;
@property (weak, nonatomic) IBOutlet UILabel *houselabel;
@property (weak, nonatomic) IBOutlet UILabel *landmarklabel;
@property (weak, nonatomic) IBOutlet UIButton *sendReqBtn;

@property (weak, nonatomic) IBOutlet UILabel *landStaticLabel;
@property (weak, nonatomic) IBOutlet UITextField *land_TF;

- (IBAction)landBtnAction:(id)sender;

@property NSMutableDictionary*detailsDict;

//@property NSMutableURLRequest *urlRequest;
@property NSMutableArray * newfinalArr;
@property NSString *serIdStr;
@property NSString *descStr;


@property NSString *packIdStr;
@property NSMutableArray *packageFinalArr;

@property NSString *reqDateStr;
@property NSString *reqTimeStr;

@property NSString *spIDStr;
@end
