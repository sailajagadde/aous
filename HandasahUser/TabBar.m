//
//  TabBar.m
//  HandasahUser
//
//  Created by Apple on 5/13/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "TabBar.h"
#import "AppDelegate.h"

@interface TabBar (){
    AppDelegate*tabBarAppDelegate;
}

@end

@implementation TabBar

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    tabBarAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    /*[[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSFontAttributeName:[UIFont fontWithName:@"FrutigerLTArabic-55Roman" size:10.0f]
                                                        } forState:UIControlStateNormal];*/
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//
//
//        [[self.tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"msg"]]];
//
//
//
//    });
    
    // [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
   // [[UITabBar appearance] setTintColor:[UIColor redColor]];
    
    
    if ([tabBarAppDelegate.selectTabItem isEqualToString:@"0"]) {
        
        [self setSelectedIndex:0];
    }
    else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"1"]) {
        
        [self setSelectedIndex:1];
        
    } else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"2"]) {
        
        [self setSelectedIndex:2];
}
    else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"3"]) {
        
        [self setSelectedIndex:3];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
