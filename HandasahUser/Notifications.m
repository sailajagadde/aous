//
//  Notifications.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "Notifications.h"
#import "NotificationCell.h"
#import "RESideMenu.h"
#import "SlideMenuVC.h"
#import "HomeViewController.h"

@interface Notifications ()<RESideMenuDelegate>{
    NSMutableArray *msgArray;
    NSMutableArray *timeArray;
}

@end

@implementation Notifications

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Notifications"];
 //   [self ServiceCallForNotifications];
  
    
//    self.backbtn_outlet.target =self;
//    self.backbtn_outlet.action = @selector(presentLeftMenuViewController:);
    
//    self.sideMenuBtn.target =self;
//    self.sideMenuBtn.action = @selector(presentLeftMenuViewController:);
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self ServiceCallForNotifications];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return msgArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationCell" forIndexPath:indexPath];
    
    cell.loremLbl.text = [msgArray objectAtIndex:indexPath.row];
    cell.timeLbl.text = [timeArray objectAtIndex:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 126;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_Back:(id)sender {
    
   // [self ServiceCallForNotifications];
    
    if ([self.strCheck isEqualToString:@"Yes"])
    {
        HomeViewController *gotoVc =[self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
        [self.navigationController pushViewController:gotoVc animated:YES];
    }else{
        self.backbtn_outlet.target =self;
        self.backbtn_outlet.action = @selector(presentLeftMenuViewController:);
    }
    
}

-(void)ServiceCallForNotifications{
    
    //http://volive.in/engineering/user_services/notifications
    //lang, user_id
    
//    NSString *requestType;
//    if ([checkStr isEqualToString:@"package"]) {
//        requestType=@"2";
//    }else if([checkStr isEqualToString:@"single"]){
//        requestType=@"1";
//    }
//
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
    //[valuesDictionary setObject:@"154" forKey:@"user_id"];
   
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"notifications" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            NSArray * dataArray = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"data"]];
            
            msgArray = [[NSMutableArray alloc]init];
            timeArray = [[NSMutableArray alloc]init];
            
            
            if (dataArray.count == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message: [[SharedClass sharedInstance]languageSelectedString:@"No Data Found"] OnViewController:self completion:nil];
                    [self.notificationTableObj reloadData];
                });
            } else{
                
                for (int i = 0; i<dataArray.count; i++) {
                    
                    
                    [msgArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"message"]];
                    [timeArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"days_ago"]];
                    
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.notificationTableObj reloadData];
                });
                
            }
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedClass sharedInstance]showAlertWithTitle: [[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                
            });
        }
        
    }];
}



@end
