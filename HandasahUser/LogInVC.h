//
//  LogInVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogInVC : UIViewController<UITextFieldDelegate>

- (IBAction)signInBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *signIn_btn;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll_View;

//@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scroll_view;
//@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIButton *signUpAction;

@property (weak, nonatomic) IBOutlet UITextField *emailID_TF;
@property (weak, nonatomic) IBOutlet UITextField *password_TF;

@property (weak, nonatomic) IBOutlet UIButton *forgotPwd_btn;
- (IBAction)forgotPassword_btnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *dontHaveAccount_label;
@property (weak, nonatomic) IBOutlet UIButton *signUP_btn;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_view;
@property (weak, nonatomic) IBOutlet UIButton *changeLanguageBtn;
- (IBAction)changeLanguageAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *makeSerStaticLabel;



@end
