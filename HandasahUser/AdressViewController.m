//
//  AdressViewController.m
//  HandasahUser
//
//  Created by Apple on 5/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "AdressViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>

#import "HomeViewController.h"
#import "TabBar.h"
#import "RESideMenu.h"
#import "SharedClass.h"
#import "AccountVC.h"
#import "MyRequestVC.h"

@interface AdressViewController ()<RESideMenuDelegate>{
    
    RESideMenu * sideMenu;
    CLLocationManager * locationManager;
    CLLocationCoordinate2D center;
    
    CLLocation *currentLocation;
    NSDictionary * jsonBodyDict;
    
    NSString *latitudeStr;
    NSString *longitudeStr;
    BOOL map;
    GMSCameraPosition *camera;
    
    UIToolbar *pickerToolBar;
    UIPickerView *pickerView;
    NSMutableArray *nameArray;
    NSMutableArray *idArray;
    NSMutableArray *pickerViewItems_Array;
    
    NSString *langStr;
    NSString *landTypeID;
    UITextField *nameField;
    //Timer
    UILabel *progress1;
    NSTimer *timer;
    int currMinute;
    int currSeconds;
    int stepperValue;
    int mainItemCount;
     NSString *progress;
    
}
@end

@implementation AdressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    [self SetTextFieldBorder:self.house_TF];
    [self SetTextFieldBorder:self.landMark_TF];
    [self SetTextFieldBorder:self.land_TF];
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Address"];
    self.locationStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Service Location"];
    self.adressLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"ADDRESS"];
    self.houselabel.text = [[SharedClass sharedInstance]languageSelectedString:@"HOUSE/FLAT NO"];
    self.landmarklabel.text = [[SharedClass sharedInstance]languageSelectedString:@"LANDMARK"];
    self.landStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"TYPE OF LAND"];
    self.landMark_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Landmark"];
    self.land_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Enter Type Of Land"];
    [self.sendReqBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"SEND REQUEST"] forState:UIControlStateNormal];
    
   /* locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    */
    currMinute=1;
    currSeconds=00;
    
    NSLog(@"single vis arr %@",self.newfinalArr);
    NSLog(@"package vis arr %@",self.packageFinalArr);
    //[self loadPickerData];
    
}


-(void)SetTextFieldBorder :(UITextField *)textField{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.8;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
}


-(void)dismissKeyboard{
    
    [self.house_TF resignFirstResponder];
    [self.landMark_TF resignFirstResponder];
    [self.land_TF resignFirstResponder];
}

-(void)loadPickerData{
    
    pickerView = [[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    
    //    self.countryCode_TF.inputView = pickerView;
    
    pickerView.showsSelectionIndicator = YES;
    
    [pickerView setFrame:CGRectMake(0, self.view.frame.size.height-162, self.view.frame.size.width, 162)];
    
    pickerToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolBar.barStyle = UIBarStyleBlackOpaque;
    
    [pickerToolBar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc]init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    //  UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDoneClicked)];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Done"] style: UIBarButtonItemStyleDone target: self action: @selector(pickerDoneClicked)];
    
    [doneBtn setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style: UIBarButtonItemStyleDone target: self action: @selector(pickerCancelClicked)];
    
    //UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pickerCancelClicked)];
    
    [cancelBtn setTintColor:[UIColor whiteColor]];
    
    [barItems addObject:cancelBtn];
    [barItems addObject:flexSpace];
    [barItems addObject:doneBtn];
    
    [pickerToolBar setItems:barItems];
    
    [self.land_TF setInputAccessoryView:pickerToolBar];
    [self ServiceCallTypeOfLand];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    latitudeStr = NULL;
    longitudeStr = NULL;
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization];
    
}

-(void)pickerDoneClicked{
    
    self.land_TF.text = [nameArray objectAtIndex:[pickerView selectedRowInComponent:0]];
    landTypeID = [idArray objectAtIndex:[pickerView selectedRowInComponent:0]];
    [self.land_TF resignFirstResponder];
}

-(void)pickerCancelClicked{
    
    [self.land_TF resignFirstResponder];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return nameArray.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.land_TF.text = [nameArray objectAtIndex:row];
    landTypeID = [idArray objectAtIndex:row];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return nameArray[row];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == self.land_TF){
        
        self.scroll_view.scrollEnabled = YES;
        [self.scroll_view setContentOffset:CGPointMake(0, textField.frame.origin.y+100) animated:YES];
        
        BOOL checkNetwork = [[SharedClass sharedInstance]connected];
        if(checkNetwork == false){
            
            NSLog(@"unreachable");
            
            //[[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Check Your Network Connection"] OnViewController:self completion:nil];
            
        }
        else{
            NSLog(@"reachable");
            [self ServiceCallTypeOfLand];
            self.land_TF.inputView = pickerView;
            
        }
        
    }
    
    return YES;
    
}
//MARK:- SERVICE CALL TYPE OF LAND
-(void)ServiceCallTypeOfLand{
    
    //http://volive.in/engineering/user_services/type_of_land
    //lang
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"type_of_land" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            NSArray * dataArray = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"data"]];
            
            nameArray = [[NSMutableArray alloc]init];
            idArray = [[NSMutableArray alloc]init];
            
            for (int i = 0; i<dataArray.count; i++) {
                
                [nameArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"name"]];
                [idArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"id"]];
            }
            
            //pickerViewItems_Array = [[NSMutableArray alloc]initWithArray:phoneCodeArray];
            //pickerViewItems_Array = [[NSMutableArray alloc]initWithArray:phonecode_idArray];
            
            
            NSLog(@"id array %@",nameArray);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [pickerView reloadAllComponents];
                [pickerView reloadInputViews];
                [SVProgressHUD dismiss];
            });
            
        }
        
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                // [[SharedClass sharedInstance]showProgressFor:[jsonDict objectForKey:@"message"]];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
            });
            
        }
        
    }];
    
}

-(void)mapsCalling
{
    dispatch_async(dispatch_get_main_queue(), ^{
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[NSString stringWithFormat:@"%@",self.lat] floatValue]
                                                                longitude:[[NSString stringWithFormat:@"%@",self.lng]floatValue]
                                                                     zoom:14];
        
        GMSMapView *mapView = [GMSMapView mapWithFrame:self.mapView.bounds camera:camera];
        mapView.myLocationEnabled = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [_mapView addSubview:mapView];
            
        });
        mapView.delegate= self;
        mapView.myLocationEnabled = YES;
        
        
        map=YES;
    });
    
    
}

/*-(void)loadMapView;{
    
    //    CLLocationCoordinate2D center;
    center.latitude = [self.lat doubleValue];
    center.longitude = [self.lng doubleValue];
    //camera = [GMSCameraPosition cameraWithLatitude:[self.lat doubleValue]  longitude:[self.lng doubleValue] zoom:16];
    camera = [GMSCameraPosition cameraWithLatitude:center.latitude longitude:center.longitude zoom:16];
    [_googleMapView setCamera:camera];
    _googleMapView.myLocationEnabled = YES;
    _googleMapView.delegate = self;
    
    //self.house_TF.text = @"7-1-28/4/6";
    
}*/


-(void)loadMapView{
    
    //    CLLocationCoordinate2D center;
    center.latitude = [latitudeStr doubleValue];
    center.longitude = [longitudeStr doubleValue];
    //camera = [GMSCameraPosition cameraWithLatitude:[self.lat doubleValue]  longitude:[self.lng doubleValue] zoom:16];
    camera = [GMSCameraPosition cameraWithLatitude:center.latitude longitude:center.longitude zoom:16];
    [_gmsGoogleMapView setCamera:camera];
    _gmsGoogleMapView.myLocationEnabled = YES;
    _gmsGoogleMapView.delegate = self;
    
    //self.house_TF.text = @"7-1-28/4/6";
    
}
/*
-(void)locationManager:(CLLocationManager* )manager didUpdateLocations:(NSArray<CLLocation *  > *)locations
{
    
    
    
    currentLocation = [locations lastObject];
    center = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
    camera = [GMSCameraPosition cameraWithLatitude:center.latitude longitude:center.longitude zoom:15];
    [locationManager stopUpdatingLocation];
    
    if (self.lat == NULL && self.lng == NULL)
    {
        
        self.lat = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
        self.lng = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
        
        NSLog(@"lat and long %@%@",self.lat,self.lng);
        if (map == NO)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self mapsCalling];
            });
        }else{
            
        }
        
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
             [self loadMapView];
        });
        
    }
}
*/


-(void)locationManager:(CLLocationManager* )manager didUpdateLocations:(NSArray<CLLocation *  > *)locations
{
    
    //    let userLocation:CLLocation = locations[0] as CLLocation
    //    self.currentLocation = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
    //    let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation.latitude, longitude:currentLocation.longitude, zoom: 15)
    //
    //    let position = CLLocationCoordinate2D(latitude:  currentLocation.latitude, longitude: currentLocation.longitude)
    //    print(position)
    //
    //    //self.setupLocationMarker(coordinate: position)
    //
    //    DispatchQueue.main.async {
    //
    //        self.gmapView.camera = camera
    //
    //    }
    //
    //    self.gmapView?.animate(to: camera)
    //    manager.stopUpdatingLocation()
    currentLocation = [locations lastObject];
    //    center = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
    //    camera = [GMSCameraPosition cameraWithLatitude:center.latitude longitude:center.longitude zoom:15];
    [locationManager stopUpdatingLocation];
    
    if (latitudeStr == NULL && longitudeStr == NULL)
    {
        
        latitudeStr = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
        longitudeStr = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
        
        NSLog(@"lat and long %@%@",latitudeStr,longitudeStr);
       //. dispatch_async(dispatch_get_main_queue(), ^{
            [self loadMapView];
        //});
        
    }
    else{
        
        
    
        
    }
}

/*
- (void) mapView: (GMSMapView *)mapView didChangeCameraPosition: (GMSCameraPosition *)position {
    NSLog(@"dichange mapview %@",position);
    double latitude = mapView.camera.target.latitude;
    double longitude = mapView.camera.target.longitude;
    
    latitudeStr = [NSString stringWithFormat:@"%f",mapView.camera.target.latitude];
    longitudeStr = [NSString stringWithFormat:@"%f",mapView.camera.target.longitude];
    
    CLLocationCoordinate2D addressCoordinates = CLLocationCoordinate2DMake(latitude,longitude);
    
    [[NSUserDefaults standardUserDefaults]setObject:latitudeStr forKey:@"lat"];
    [[NSUserDefaults standardUserDefaults]setObject:longitudeStr forKey:@"lang"];
    
    GMSGeocoder* coder = [[GMSGeocoder alloc] init];
    [coder reverseGeocodeCoordinate:addressCoordinates completionHandler:^(GMSReverseGeocodeResponse *results, NSError *error) {
        if (error) {
            
        } else {
            GMSAddress* address = [results firstResult];
            NSLog(@"address is %@",address);
            
            
            
            //            self.country = address.country ? address.country : @"";
            // self.house_TF.text = address.lines ? address.subLocality : @"";
            //            self.pinCode = address.postalCode ? address.postalCode : @"";
            //            self.state = address.administrativeArea ? address.administrativeArea : @"";
            NSArray *arr = [address valueForKey:@"lines"];
            NSString *str1 = [NSString stringWithFormat:@"%lu",(unsigned long)[arr count]];
            
            if ([str1 isEqualToString:@"0"]) {
                self.address_TV.text = @"";
            }
            else if ([str1 isEqualToString:@"1"]) {
                NSString *str2 = [arr objectAtIndex:0];
                self.address_TV.text = str2;
            }
            else if ([str1 isEqualToString:@"2"]) {
                NSString *str2 = [arr objectAtIndex:0];
                NSString *str3 = [arr objectAtIndex:1];
                if (str2.length > 1 ) {
                    self.address_TV.text = [NSString stringWithFormat:@"%@,%@",str2,str3];
                }
                else {
                    self.address_TV.text = [NSString stringWithFormat:@"%@",str3];
                }
                NSLog(@"address is %@",self.address_TV.text);
                //self.landMark_TF.text = address.subLocality;
                //self.house_TF.text = address.thoroughfare;
                //self.house_TF.text = str2;
            }
        }
    }];
}
*/

- (void) mapView: (GMSMapView *)mapView didChangeCameraPosition: (GMSCameraPosition *)position {
    
    double latitude = mapView.camera.target.latitude;
    double longitude = mapView.camera.target.longitude;
    
    latitudeStr = [NSString stringWithFormat:@"%f",mapView.camera.target.latitude];
    longitudeStr = [NSString stringWithFormat:@"%f",mapView.camera.target.longitude];
    
    CLLocationCoordinate2D addressCoordinates = CLLocationCoordinate2DMake(latitude,longitude);
    
    //[[NSUserDefaults standardUserDefaults]setObject:latitudeStr forKey:@"lat"];
    //[[NSUserDefaults standardUserDefaults]setObject:longitudeStr forKey:@"lang"];
    
    GMSGeocoder* coder = [[GMSGeocoder alloc] init];
    [coder reverseGeocodeCoordinate:addressCoordinates completionHandler:^(GMSReverseGeocodeResponse *results, NSError *error) {
        if (error) {
            
        } else {
            GMSAddress* address = [results firstResult];
            NSLog(@"address is %@",address);
            
            
            
            //            self.country = address.country ? address.country : @"";
            // self.house_TF.text = address.lines ? address.subLocality : @"";
            //            self.pinCode = address.postalCode ? address.postalCode : @"";
            //            self.state = address.administrativeArea ? address.administrativeArea : @"";
            NSArray *arr = [address valueForKey:@"lines"];
            NSString *str1 = [NSString stringWithFormat:@"%lu",(unsigned long)[arr count]];
            
            if ([str1 isEqualToString:@"0"]) {
                self.address_TV.text = @"";
            }
            else if ([str1 isEqualToString:@"1"]) {
                NSString *str2 = [arr objectAtIndex:0];
                self.address_TV.text = str2;
            }
            else if ([str1 isEqualToString:@"2"]) {
                NSString *str2 = [arr objectAtIndex:0];
                NSString *str3 = [arr objectAtIndex:1];
                if (str2.length > 1 ) {
                    self.address_TV.text = [NSString stringWithFormat:@"%@,%@",str2,str3];
                }
                else {
                    self.address_TV.text = [NSString stringWithFormat:@"%@",str3];
                }
                NSLog(@"address is %@",self.address_TV.text);
                //self.landMark_TF.text = address.subLocality;
                //self.house_TF.text = address.thoroughfare;
                //self.house_TF.text = str2;
            }
        }
    }];
}


- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btn_Back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_SendRequest:(id)sender {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self ServiceCallForSendRequest];
//    });
   // || latitudeStr != NULL || longitudeStr != NULL
   
        if(![_address_TV.text isEqualToString:@""]|| latitudeStr != NULL || longitudeStr != NULL){
            
            [self ServiceCallForSendRequest];
//            BOOL checkNetwork = [[SharedClass sharedInstance]connected];
//            if(checkNetwork == false){
//
//                NSLog(@"unreachable");
//
//                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Check Your Network Connection"] OnViewController:self completion:nil];
//
//            }
//            else{
//                NSLog(@"reachable");
//
//
//            }
            
        }else{
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Address Should Not Be Empty"] OnViewController:self completion:nil];
        }
    }
    


//service call for send req

//MARK:- SERVICE CALL TO SEND REQUEST
-(void)ServiceCallForSendRequest
{
    
    NSString *emailText = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSLog(@"email text %@",emailText);
    
   // if(!([emailText length] == 0))  {
        //http://volive.in/engineering/user_services/sending_request
        //       //lang, address, house, land_mark,  latitude, longitude, service_id, user_id, request_type, package_id, visits = [ {  visiting_time ,visiting_date, description  } ]
        
        // lang,address,house,land_mark,latitude,longitude,service_id,sp_user_id,user_id,type_of_land,description,requested_time,requested_date
        
        //NSString *reqTypeStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"typeStr"];
        
        NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
        
        if ([str isEqualToString:@"2"]) {
            langStr = @"ar";
            
        } else if([str isEqualToString:@"1"]){
            
            langStr = @"en";
            
        }
        else{
            
        }
        NSLog(@"reqdate  and request time %@ %@ %@ %@",self.reqTimeStr,self.reqDateStr,[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"],[[NSUserDefaults standardUserDefaults]objectForKey:@"SPUID"]);
        jsonBodyDict = @{
                         @"API-KEY":APIKEY,
                         @"house":@"",
                         @"address":self.address_TV.text,
                         @"land_mark":@"",
                         @"latitude":latitudeStr,
                         @"longitude":longitudeStr,
                         @"service_id":self.serIdStr,
                         @"description":self.descStr,
                         //@"visits":self.newfinalArr,
                         @"requested_time":self.reqTimeStr,
                         @"requested_date":self.reqDateStr,
                         @"sp_user_id":@"",
                         //self.spIDStr,
                         //[[NSUserDefaults standardUserDefaults]objectForKey:@"SPUID"],
                         @"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"],
                         @"lang":langStr,
                         @"type_of_land":@""
                         };
        
        
        
        NSLog(@"my dict is %@",jsonBodyDict);
        
        
        NSString *urlString1 = @"http://volive.in/aous/user_services/sending_request";
        //[self start];
        [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedString:@"Wait for service provider to accept your request"]];
        
        NSData *jsonBodyData = [NSJSONSerialization dataWithJSONObject:jsonBodyDict options:kNilOptions error:nil];
        
        NSMutableURLRequest *request = [NSMutableURLRequest new];
        request.HTTPMethod = @"POST";
        
        // for alternative 1:
        [request setURL:[NSURL URLWithString:urlString1]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setHTTPBody:jsonBodyData];
        
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config
                                                              delegate:nil
                                                         delegateQueue:[NSOperationQueue mainQueue]];
        
        
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData * _Nullable data,
                                                                    NSURLResponse * _Nullable response,
                                                                    NSError * _Nullable error) {
                                                   
                                                    
                                                    NSDictionary *statusDict = [[NSDictionary alloc]init];
                                                    statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                    
                                                    NSLog(@"Images from server %@", statusDict);
                                                    
                                                    NSString* status=[statusDict objectForKey:@"status"];
                                                    int result=[status intValue];
                                                    [SVProgressHUD dismiss];
                                                    
                                                    
                                                    NSString *flagstr;
                                                    NSLog(@"flag %@",flagstr);
                                                    if(result == 1){
                                                        
//                                                        [timer invalidate];
//                                                        [SVProgressHUD dismiss];
                                                      flagstr= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"pass_flag"]];
                                                         NSLog(@"flag sttt %@",flagstr);
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            [SVProgressHUD dismiss];
                                                            
                                                            TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                            
                                                            RESideMenu *sideMenuViewController;
                                                            
                                                            sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
                                                            
                                                            HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                            sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
                                                            sideMenuViewController.delegate = self;
                                                            
                                                            [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
                                                            
                                                            [self.navigationController pushViewController:tab animated:YES];
                                                            

                                                            
                                                            UIAlertController * alert = [UIAlertController
                                                                                         alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
                                                                                         message:[statusDict objectForKey:@"message"]
                                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                
                                                                if ([flagstr isEqualToString:@"1"]) {
                                                                    NSLog(@"update ur pwd");
                                                                    
                                                                    
                                                                    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@""
                                                                                                                                              message:[[SharedClass sharedInstance]languageSelectedString:@"Update Password"]
                                                                                                                                       preferredStyle:UIAlertControllerStyleAlert];
                                                                    
                                                                    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                                                        // textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Old Password"];
                                                                        textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Enter Password"];
                                                                        textField.textColor = [UIColor blackColor];
                                                                        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                                                                        textField.borderStyle = UITextBorderStyleRoundedRect;
                                                                        textField.secureTextEntry = YES;
                                                                        
                                                                    }];
                                                                    
                                                                    
                                                                    alertController.view.tintColor = [UIColor colorWithRed:106.0/255.0 green:189.0/255.0 blue:239.0/255.0 alpha:1];
                                                                    [alertController addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"SUBMIT"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                                        
                                                                        NSArray * textfields = alertController.textFields;
                                                                        
                                                                        nameField = textfields[0];
                                                                        
                                                                        NSLog(@"%@",nameField.text);
                                                                        
                                                                        [self forUpdatePassword];
                                                                        //                                                                    if (nameField.text.length>0) {
                                                                        //
                                                                        //                                                                        [self forUpdatePassword];
                                                                        //                                                                    }
                                                                        //                                                                    else{
                                                                        //
                                                                        //                                                                        NSLog(@"ghhgh");
                                                                        //
                                                                        //                                                                    }
                                                                    }]];
                                                                    
                                                                    alertController.view.tintColor = [UIColor colorWithRed:106.0/255.0 green:189.0/255.0 blue:239.0/255.0 alpha:1];
                                                                    [self presentViewController:alertController animated:YES completion:nil];
                                                                    
                                                                    
                                                                }
                                                                else{
                                                                    
                                                                }
                                                                
                                                            }];
                                                            
                                                            [alert addAction:yesButton];
                                                            
                                                            [self presentViewController:alert animated:YES completion:nil];
                                                            
                                                            
                                                        });
                                                    }
                                                    else{
                                                        
                                                        //[self start];
//                                                        [timer invalidate];
//                                                        [SVProgressHUD dismiss];
                                                        flagstr= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"pass_flag"]];
                                                        
                                                        TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];

                                                        RESideMenu *sideMenuViewController;

                                                        sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];

                                                        HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                        sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
                                                        sideMenuViewController.delegate = self;

                                                        [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;

                                                        [self.navigationController pushViewController:tab animated:YES];
                                                        
                                                        
                                                        
                                                        UIAlertController * alert = [UIAlertController
                                                                                     alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
                                                                                     message:[statusDict objectForKey:@"message"]
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                                        
                                                        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                            
                                                            if ([flagstr isEqualToString:@"1"]) {
                                                                NSLog(@"update ur pwd");
                                                                
                                                                
                                                                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@""
                                                                                                                                          message:[[SharedClass sharedInstance]languageSelectedString:@"Update Password"]
                                                                                                                                   preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                                                                    // textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Old Password"];
                                                                    textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Enter Password"];
                                                                    textField.textColor = [UIColor blackColor];
                                                                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                                                                    textField.borderStyle = UITextBorderStyleRoundedRect;
                                                                    textField.secureTextEntry = YES;
                                                                    
                                                                }];
                                                                

                                                                alertController.view.tintColor = [UIColor colorWithRed:106.0/255.0 green:189.0/255.0 blue:239.0/255.0 alpha:1];
                                                                [alertController addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"SUBMIT"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                                    
                                                                    NSArray * textfields = alertController.textFields;
                                                                    
                                                                    nameField = textfields[0];
                                                                   
                                                                    NSLog(@"%@",nameField.text);
                                                                    
                                                                    [self forUpdatePassword];
//                                                                    if (nameField.text.length>0) {
//
//                                                                        [self forUpdatePassword];
//                                                                    }
//                                                                    else{
//
//                                                                        NSLog(@"ghhgh");
//
//                                                                    }
                                                                }]];
                                                                
                                                                alertController.view.tintColor = [UIColor colorWithRed:106.0/255.0 green:189.0/255.0 blue:239.0/255.0 alpha:1];
                                                                [self presentViewController:alertController animated:YES completion:nil];
                                                                
                                                                
                                                            }
                                                            else{
                                                                
                                                            }
                                                            
                                                        }];
                                                        
                                                        [alert addAction:yesButton];
                                                        
                                                        [self presentViewController:alert animated:YES completion:nil];
                                                        
                                                        
                                                    }
                                                    
                                                }];
         
        [task resume];
    }

//pass_flag=1
//    else{
//        UIAlertController * alertController=   [UIAlertController
//                                                alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
//                                                message:[[SharedClass sharedInstance]languageSelectedString:@"Please Enter All Fields"]
//                                                preferredStyle:UIAlertControllerStyleAlert];
//
//
//        UIAlertAction *okAction = [UIAlertAction
//                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction *action)
//                                   {
//                                       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                                       AccountVC *controller=[storyboard instantiateViewControllerWithIdentifier:@"AccountVC"];
//                                       controller.textFromSendRequest = @"Email";
//                                       [self.navigationController pushViewController:controller animated:YES];
//                                   }];
//
//
//        [alertController addAction:okAction];
//        [self presentViewController:alertController animated:YES completion:nil];
//
//    }
    
//}


-(void)start
{
   timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    
}

-(void)timerFired
{
    if((currMinute>0 || currSeconds>=0) && currMinute>=0)
    {
        if(currSeconds==0)
        {
            currMinute-=1;
            currSeconds=59;
        }
        else if(currSeconds>0)
        {
            currSeconds-=1;
        }
        if(currMinute>-1)
            
            progress=[NSString stringWithFormat:@"%@%d%@%02d",@":",currMinute,@":",currSeconds];
        [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"%@ %@",[[SharedClass sharedInstance]languageSelectedString:@"Waiting for approval,Please Wait..."],progress]];
        //[self.view setUserInteractionEnabled:TRUE];
    }
    else
    {
       // [delegate.progTimer invalidate];
        [SVProgressHUD dismiss];
        [self.view setUserInteractionEnabled:TRUE];
        //[self ServiceCallForSendRequest];
        
    }
}


-(void)forUpdatePassword{
    
    //http://volive.in/aous/user_services/create_password
    // lang, password, user_id
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    //[[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    [valuesDictionary setObject:nameField.text forKey:@"password"];
    
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
    
    //[valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"language"] forKey:@"language"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
    } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"create_password" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)data];
        NSLog(@"json dict %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        NSLog(@"sta %@",status);
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:^{ }];
                // [[SharedClass sharedInstance]showProgressForSuccess:[jsonDict objectForKey:@"message"]];
                
            });
            
            
        } else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:^{ }];
                
                [SVProgressHUD dismiss];
                
            });
            
        }
        
    }];
    
    
}

- (IBAction)landBtnAction:(id)sender {
   /* NSString *emailText = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSLog(@"email text %@",emailText);
    
    if(!([emailText length] == 0))  {
        //http://volive.in/engineering/user_services/sending_request
        //       //lang, address, house, land_mark,  latitude, longitude, service_id, user_id, request_type, package_id, visits = [ {  visiting_time ,visiting_date, description  } ]
        
        // lang,address,house,land_mark,latitude,longitude,service_id,sp_user_id,user_id,type_of_land,description,requested_time,requested_date
        
        //NSString *reqTypeStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"typeStr"];
        
        NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
        
        if ([str isEqualToString:@"2"]) {
            langStr = @"ar";
            
        } else if([str isEqualToString:@"1"]){
            
            langStr = @"en";
            
        }
        else{
            
        }
        NSLog(@"reqdate  and request time %@ %@ %@ %@",self.reqTimeStr,self.reqDateStr,[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"],[[NSUserDefaults standardUserDefaults]objectForKey:@"SPUID"]);
        jsonBodyDict = @{
                         @"API-KEY":APIKEY,
                         @"house":@"",
                         @"address":self.address_TV.text,
                         @"land_mark":@"",
                         @"latitude":latitudeStr ,
                         @"longitude":longitudeStr,
                         @"service_id":self.serIdStr,
                         @"description":self.descStr,
                         //@"visits":self.newfinalArr,
                         @"requested_time":self.reqTimeStr,
                         @"requested_date":self.reqDateStr,
                         @"sp_user_id":@"",
                         //self.spIDStr,
                         //[[NSUserDefaults standardUserDefaults]objectForKey:@"SPUID"],
                         @"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"],
                         @"lang":langStr,
                         @"type_of_land":@""
                         };
        
        
        
        NSLog(@"my dict is %@",jsonBodyDict);
        
        
        NSString *urlString1 = @"http://volive.in/aous/user_services/sending_request";
        [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedString:@"Wait for service provider to accept your request"]];
        
        NSData *jsonBodyData = [NSJSONSerialization dataWithJSONObject:jsonBodyDict options:kNilOptions error:nil];
        
        NSMutableURLRequest *request = [NSMutableURLRequest new];
        request.HTTPMethod = @"POST";
        
        // for alternative 1:
        [request setURL:[NSURL URLWithString:urlString1]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setHTTPBody:jsonBodyData];
        
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config
                                                              delegate:nil
                                                         delegateQueue:[NSOperationQueue mainQueue]];
        
        
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData * _Nullable data,
                                                                    NSURLResponse * _Nullable response,
                                                                    NSError * _Nullable error) {
                                                    
                                                    
                                                    NSDictionary *statusDict = [[NSDictionary alloc]init];
                                                    statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                    
                                                    NSLog(@"Images from server %@", statusDict);
                                                    
                                                    NSString* status=[statusDict objectForKey:@"status"];
                                                    int result=[status intValue];
                                                    [SVProgressHUD dismiss];
                                                    
                                                    if(result == 1){
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            [SVProgressHUD dismiss];
                                                            
                                                            TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                            
                                                            RESideMenu *sideMenuViewController;
                                                            
                                                            sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
                                                            
                                                            HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                            sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
                                                            sideMenuViewController.delegate = self;
                                                            
                                                            [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
                                                            
                                                            [self.navigationController pushViewController:tab animated:YES];
                                                            
                                                        });
                                                    }
                                                    else{
                                                        TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                        
                                                        RESideMenu *sideMenuViewController;
                                                        
                                                        sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
                                                        
                                                        HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                                                        sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
                                                        sideMenuViewController.delegate = self;
                                                        
                                                        [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
                                                        
                                                        [self.navigationController pushViewController:tab animated:YES];
                                                        
                                                        
                                                        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[statusDict objectForKey:@"message"] OnViewController:self completion:nil];
                                                        [SVProgressHUD dismiss];
                                                    }
                                                    
                                                }];
        
        [task resume];
    }else{
        UIAlertController * alertController=   [UIAlertController
                                                alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
                                                message:[[SharedClass sharedInstance]languageSelectedString:@"Please Enter All Fields"]
                                                preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                       AccountVC *controller=[storyboard instantiateViewControllerWithIdentifier:@"AccountVC"];
                                       controller.textFromSendRequest = @"Email";
                                       [self.navigationController pushViewController:controller animated:YES];
                                   }];
        
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }*/
    
}
@end
