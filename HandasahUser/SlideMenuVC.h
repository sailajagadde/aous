//
//  SlideMenuVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSArray * menuImg;
    NSArray * menuNames;
}
@property (strong, nonatomic) IBOutlet UILabel *tomHanks;
@property (strong, nonatomic) IBOutlet UILabel *tomhanksMail;
@property (strong, nonatomic) IBOutlet UITableView *slideTableObj;
@property (weak, nonatomic) IBOutlet UILabel *name_label;
@property (weak, nonatomic) IBOutlet UILabel *email_label;
@property (weak, nonatomic) IBOutlet UIImageView *user_ImageView;

@end
