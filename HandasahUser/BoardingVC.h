//
//  BoardingVC.h
//  HandasahUser
//
//  Created by Suman Guntuka on 05/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BoardingVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collection_view;

//- (IBAction)back_btnAction:(id)sender;


@property (weak, nonatomic) IBOutlet UIPageControl *page_Control;
- (IBAction)nextAction:(id)sender;
- (IBAction)skipAction:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *skip_btn;
@property (weak, nonatomic) IBOutlet UIButton *next_btn;

@property (strong, nonatomic) UIWindow *window;


@end

NS_ASSUME_NONNULL_END
