//
//  MyRequestVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRequestVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideBtn;
@property (strong, nonatomic) IBOutlet UIButton *singleBtn;
@property (strong, nonatomic) IBOutlet UIButton *packageBtn;
//@property (strong, nonatomic) IBOutlet UITableView *myReqTableObj;
//- (IBAction)btn_Package:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *holder_view;

- (IBAction)single_btnAction:(id)sender;
- (IBAction)package_btnAction:(id)sender;


@property (weak, nonatomic) IBOutlet UITableView *request_tableView;

- (IBAction)makePayment_btnAction:(id)sender;
- (IBAction)viewDetails_btnAction:(id)sender;

- (IBAction)cancelBtnAction:(id)sender;

//@property NSString *checkDateStr;

@end
