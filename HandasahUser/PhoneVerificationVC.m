//
//  PhoneVerificationVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "PhoneVerificationVC.h"
#import "LogInVC.h"
#import "HomeViewController.h"
#import "RESideMenu.h"

@interface PhoneVerificationVC ()<RESideMenuDelegate>
{
    RESideMenu * sideMenu;
     NSString *str;
}



@end

@implementation PhoneVerificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self loadAttributes];
    
    [self.otpOne_TF becomeFirstResponder];
    [self.otpTwo_TF resignFirstResponder];
    [self.otpThree_TF resignFirstResponder];
    [self.otpFour_TF resignFirstResponder];
    
}


-(void)loadAttributes{
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Back-1"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    back.tintColor = [UIColor whiteColor];
    [self.navigationItem setLeftBarButtonItem:back];
    
    
    [self.resent_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"SUBMIT"] forState:UIControlStateNormal];
    self.pleaseEnterOtp_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Please enter your OTP"];
    self.receiveSms_label.text = [[SharedClass sharedInstance]languageSelectedString:@"You'll receive an SMS to verify,but we'll never spam"];
    self.phoneVerification_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Phone Verification"];
    
    self.countryCode_TF.text =@"";
    //[[NSUserDefaults standardUserDefaults]objectForKey:@"phone_code"];
    self.phoneNumber_TF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"phone_number"];
}

-(void)dismissKeyboard{
    
    [self.countryCode_TF resignFirstResponder];
    [self.phoneNumber_TF resignFirstResponder];
    [self.otpOne_TF resignFirstResponder];
    [self.otpTwo_TF resignFirstResponder];
    [self.otpThree_TF resignFirstResponder];
    [self.otpFour_TF resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField.text.length < 1) && (string.length > 0))
    {
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
       // [self otpServiceCalling];
        if (nextResponder){
            [nextResponder becomeFirstResponder];
        }
        else{
            [self otpServiceCalling];
        }
        return NO;
        
    }else if ((textField.text.length >= 1) && (string.length == 0)){
        // on deleteing value from Textfield
        
        NSInteger prevTag = textField.tag - 1;
        // Try to find prev responder
        UIResponder* prevResponder = [textField.superview viewWithTag:prevTag];
        if (! prevResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (prevResponder)
            // Found next responder, so set it.
            [prevResponder becomeFirstResponder];
        
        
        return NO;
    }
//    else if(textField.tag == 4){
//        [self otpServiceCalling];
//    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    if (textField == self.otpOne_TF) {
        [self.otpTwo_TF becomeFirstResponder];
    }
    else if (textField == self.otpTwo_TF){
        [self.otpThree_TF becomeFirstResponder];

    }
    else if (textField == self.otpThree_TF){
        [self.otpFour_TF becomeFirstResponder];
    }
    else if (textField == self.otpFour_TF){
        [self.otpFour_TF resignFirstResponder];

        [self otpServiceCalling];

    }
    [self.scroll_view setContentOffset:CGPointMake(0, -self.scroll_view.contentInset.top) animated:YES];
    return YES;

}

- (IBAction)resentAction:(id)sender {
//    LogInVC *logIn = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInVC"];
//    [self.navigationController pushViewController:logIn animated:YES];
    
}
-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


//Service Calls


-(void)otpServiceCalling{
    
    //http://volive.in/aous/user_services/phone_verification
    //lang, user_id ,otp = 1234
    
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
   
    NSString *str = [NSString stringWithFormat:@"%@%@%@%@",self.otpOne_TF.text,self.otpTwo_TF.text,self.otpThree_TF.text,self.otpFour_TF.text];
    
    [valuesDictionary setObject:str forKey:@"otp"];
    NSString *langStr = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([langStr isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
    } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    NSLog(@"otp verification parameters %@",valuesDictionary);
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"phone_verification" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)data];
        NSLog(@"otp verify json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                
                RESideMenu *sideMenuViewController;
                
                sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
                
                HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
                sideMenuViewController.delegate = self;
                
                [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
                
                [self.navigationController pushViewController:tab animated:YES];
//                LogInVC *logIn = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInVC"];
//                [self.navigationController pushViewController:logIn animated:YES];
                
                                UIAlertController * alert = [UIAlertController
                                                             alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
                                                             message:[jsonDict objectForKey:@"message"]
                                                             preferredStyle:UIAlertControllerStyleAlert];
                
                                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                }];
                
                                [alert addAction:yesButton];
                
                                [self presentViewController:alert animated:YES completion:nil];
                
            });
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                    UIAlertController * alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] message:[jsonDict objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction  *okAction = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleCancel handler:^(UIAlertAction*  _Nonnull action) {
                        
                        self.otpOne_TF.text=@"";
                        self.otpTwo_TF.text=@"";
                        self.otpThree_TF.text=@"";
                        self.otpFour_TF.text=@"";
                        [self.otpOne_TF becomeFirstResponder];
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                   [SVProgressHUD dismiss];

            });
            
        }
        
    }];
    
}


- (IBAction)submit_btnAction:(id)sender {
    
     
}
@end
