//
//  FirstVistCollectionViewCell.h
//  HandasahUser
//
//  Created by Apple on 5/13/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstVistCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *day_lbl;
@property (strong, nonatomic) IBOutlet UILabel *date_lbl;
@property (strong, nonatomic) IBOutlet UIView *backView;

@end
