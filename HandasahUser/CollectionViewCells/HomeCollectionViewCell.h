//
//  HomeCollectionViewCell.h
//  HandasahUser
//
//  Created by Apple on 5/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionViewCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UILabel *lisLbl;
@property (strong, nonatomic) IBOutlet UIImageView *listImg;
@end
