//
//  OnBoardingCell.h
//  HandasahUser
//
//  Created by Suman Guntuka on 05/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OnBoardingCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon_ImageView;

@property (weak, nonatomic) IBOutlet UILabel *name_label;
@property (weak, nonatomic) IBOutlet UILabel *description_label;



@end

NS_ASSUME_NONNULL_END
