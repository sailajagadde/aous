//
//  ChoosePackageDetailsVC.h
//  HandasahUser
//
//  Created by volivesolutions on 04/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePackageDetailsVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *packageImageView;
@property (weak, nonatomic) IBOutlet UILabel *packageNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *staticVisitsLabel;
@property (weak, nonatomic) IBOutlet UILabel *noOfVisitsLabel;
@property (weak, nonatomic) IBOutlet UILabel *staticPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *staticDescLabel;
@property (weak, nonatomic) IBOutlet UITextView *description_TV;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn_Outlet;
- (IBAction)closeBtn_Action:(id)sender;








@end
