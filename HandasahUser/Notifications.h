//
//  Notifications.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Notifications : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideMenuBtn;
@property (strong, nonatomic) IBOutlet UITableView *notificationTableObj;
//@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideMenu;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *back;
- (IBAction)btn_Back:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backbtn_outlet;

@property (strong,nonatomic)NSString *strCheck;

@end
