//
//  SurveyingTableViewCell.h
//  HandasahUser
//
//  Created by Apple on 5/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveyingTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *alhusainLbl;
@property (strong, nonatomic) IBOutlet UILabel *kiloMeterLbl;
@property (strong, nonatomic) IBOutlet UILabel *surveyingLbl;


@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UILabel *ratingLbl;
@property (strong, nonatomic) IBOutlet UIButton *viewBtn;

@end
