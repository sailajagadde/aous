//
//  CompleteOrderCell.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompleteOrderCell : UITableViewCell
//@property (strong, nonatomic) IBOutlet UILabel *alhusainLbl;
//@property (strong, nonatomic) IBOutlet UILabel *survyingLbl;
//@property (strong, nonatomic) IBOutlet UILabel *statusLbl;
//@property (strong, nonatomic) IBOutlet UILabel *pendingLbl;
//@property (strong, nonatomic) IBOutlet UILabel *serviceLbl;
//@property (strong, nonatomic) IBOutlet UILabel *surveLbl;
//@property (strong, nonatomic) IBOutlet UILabel *costLbl;
//@property (strong, nonatomic) IBOutlet UILabel *moneyLbl;
//@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
//@property (strong, nonatomic) IBOutlet UILabel *monthLbl;

//@property (strong, nonatomic) IBOutlet UIButton *checkPdfBtn;
//@property (strong, nonatomic) IBOutlet UIButton *viewDetailsBtn;

@property (weak, nonatomic) IBOutlet UIView *back_view;
@property (weak, nonatomic) IBOutlet UILabel *statusStatic_label;

@property (weak, nonatomic) IBOutlet UIImageView *userImageview;
@property (weak, nonatomic) IBOutlet UILabel *userName_label;
@property (weak, nonatomic) IBOutlet UILabel *service_label;
@property (weak, nonatomic) IBOutlet UILabel *status_label;

@property (weak, nonatomic) IBOutlet UILabel *serviceStatic_label;
@property (weak, nonatomic) IBOutlet UILabel *costStatic_label;
@property (weak, nonatomic) IBOutlet UILabel *dateStatic_label;

@property (weak, nonatomic) IBOutlet UILabel *serType_label;
@property (weak, nonatomic) IBOutlet UILabel *cost_label;
@property (weak, nonatomic) IBOutlet UILabel *date_label;
@property (strong, nonatomic) IBOutlet UIButton *ratingBtn;

@property (weak, nonatomic) IBOutlet UIButton *makePayment_btn;
@property (weak, nonatomic) IBOutlet UIButton *viewDetails_btn;
@property (strong, nonatomic) IBOutlet UITextView *serviceNameTV;

@end
