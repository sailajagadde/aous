//
//  ChoosePackageCell.m
//  HandasahUser
//
//  Created by MuraliKrishna on 12/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "ChoosePackageCell.h"

@implementation ChoosePackageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
