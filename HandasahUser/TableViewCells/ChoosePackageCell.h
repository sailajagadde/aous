//
//  ChoosePackageCell.h
//  HandasahUser
//
//  Created by MuraliKrishna on 12/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePackageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *check_imageView;
@property (weak, nonatomic) IBOutlet UILabel *packageName_label;
@property (weak, nonatomic) IBOutlet UILabel *packageCost_label;
@property (weak, nonatomic) IBOutlet UILabel *visit_label;


@end
