//
//  SPListCell.m
//  HandasahUser
//
//  Created by MuraliKrishna on 21/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "SPListCell.h"

@implementation SPListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
