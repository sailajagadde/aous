//
//  PackCell.h
//  HandasahUser
//
//  Created by MuraliKrishna on 13/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *visitLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UITextField *dateTF;
@property (weak, nonatomic) IBOutlet UITextField *timeTF;



@end
