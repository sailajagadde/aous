//
//  SPListCell.h
//  HandasahUser
//
//  Created by MuraliKrishna on 21/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *emailStatic;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileStatic;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;



@property (weak, nonatomic) IBOutlet UILabel *serviceStatic;
@property (weak, nonatomic) IBOutlet UILabel *costStatic;

@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end
