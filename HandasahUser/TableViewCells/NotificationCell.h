//
//  NotificationCell.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *loremLbl;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;

@end
