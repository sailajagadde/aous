//
//  PackageTVCell.h
//  HandasahUser
//
//  Created by MuraliKrishna on 22/06/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *visitName_label;
@property (weak, nonatomic) IBOutlet UILabel *visitDate_label;
@property (weak, nonatomic) IBOutlet UIView *back_view;

@end
