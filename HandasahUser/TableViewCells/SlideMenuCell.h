//
//  SlideMenuCell.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *slideImg;
@property (strong, nonatomic) IBOutlet UILabel *slideNames;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgLeadingConstarint;

@end
