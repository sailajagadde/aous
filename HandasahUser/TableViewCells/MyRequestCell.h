//
//  MyRequestCell.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRequestCell : UITableViewCell
//@property (strong, nonatomic) IBOutlet UILabel *namLbl;
//@property (strong, nonatomic) IBOutlet UILabel *surveyingLbl;
@property (strong, nonatomic) IBOutlet UILabel *statusLbl;
//@property (strong, nonatomic) IBOutlet UILabel *pendingLbl;
@property (weak, nonatomic) IBOutlet UILabel *statusChange_label;

@property (strong, nonatomic) IBOutlet UILabel *serviceLbl;


@property (weak, nonatomic) IBOutlet UIImageView *userImageview;



@property (strong, nonatomic) IBOutlet UILabel *surveLbl;
@property (strong, nonatomic) IBOutlet UILabel *costLbl;
@property (strong, nonatomic) IBOutlet UILabel *moneyLbl;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UILabel *monthLbl;

@property (weak, nonatomic) IBOutlet UILabel *name_label;
@property (weak, nonatomic) IBOutlet UILabel *serType_label;

@property (weak, nonatomic) IBOutlet UILabel *serType1_label;
@property (weak, nonatomic) IBOutlet UILabel *serCost_label;
@property (weak, nonatomic) IBOutlet UILabel *date_label;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UITextView *serviceNameTV;



@property (strong, nonatomic) IBOutlet UIButton *makePaymentBtn;
@property (strong, nonatomic) IBOutlet UIButton *viewDetailsBtn;

@property (weak, nonatomic) IBOutlet UIView *back_view;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelBtnHeightConstarint;



@end
