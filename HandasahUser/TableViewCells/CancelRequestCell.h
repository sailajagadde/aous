//
//  CancelRequestCell.h
//  HandasahUser
//
//  Created by MuraliKrishna on 23/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelRequestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *back_view;
@property (weak, nonatomic) IBOutlet UILabel *statusStatic_label;

@property (weak, nonatomic) IBOutlet UIImageView *userImageview;
@property (weak, nonatomic) IBOutlet UILabel *userName_label;
@property (weak, nonatomic) IBOutlet UILabel *service_label;
@property (weak, nonatomic) IBOutlet UILabel *status_label;

@property (weak, nonatomic) IBOutlet UILabel *serviceStatic_label;
@property (weak, nonatomic) IBOutlet UILabel *costStatic_label;
@property (weak, nonatomic) IBOutlet UILabel *dateStatic_label;

@property (weak, nonatomic) IBOutlet UILabel *serType_label;
@property (weak, nonatomic) IBOutlet UILabel *cost_label;
@property (weak, nonatomic) IBOutlet UILabel *date_label;

@property (weak, nonatomic) IBOutlet UIButton *makePayment_btn;
@property (weak, nonatomic) IBOutlet UIButton *viewDetails_btn;

@end
