//
//  ForgotPassword.h
//  HandasahUser
//
//  Created by MuraliKrishna on 25/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassword : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *email_TF;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)submitBtnAction:(id)sender;


@end
