//
//  RegisterVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterVC : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *name_TF;
@property (weak, nonatomic) IBOutlet UITextField *emailID_TF;
@property (weak, nonatomic) IBOutlet UITextField *password_TF;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumber_TF;
@property (weak, nonatomic) IBOutlet UITextField *countryCode_TF;

@property (weak, nonatomic) IBOutlet UIButton *register_btn;

@property (weak, nonatomic) IBOutlet UILabel *alreadyHaveAccount_label;
@property (weak, nonatomic) IBOutlet UIButton *login_btn;

- (IBAction)back_btnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll_view;
@property (weak, nonatomic) IBOutlet UILabel *makeStaticlabel;

@property NSString *countryID;

@property (weak, nonatomic) IBOutlet UIButton *checkBoxBtn_Outlet;
- (IBAction)checkBoxBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *termsBtn_Outlet;
- (IBAction)termsBtn_Action:(id)sender;




@end
