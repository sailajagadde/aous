//
//  ProceedPackagesVC.h
//  HandasahUser
//
//  Created by Apple on 5/21/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProceedPackagesVC : UIViewController<UITextFieldDelegate,UITextViewDelegate>
- (IBAction)submitAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *packageTV;
@property (weak, nonatomic) IBOutlet UIButton *submit_btn;
@property (weak, nonatomic) IBOutlet UILabel *staticDescLabel;
@property (weak, nonatomic) IBOutlet UITextView *description_TV;

@property (weak, nonatomic) IBOutlet UILabel *pleaseEnterDescLabel;

@property NSString *serIDStr;
@property NSString *packageIDStr;
@end
