//
//  AccountVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountVC : UIViewController<UIImagePickerControllerDelegate,UITextFieldDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menuBtn;

//@property (weak, nonatomic) IBOutlet UIImageView *profile_Image;
@property (weak, nonatomic) IBOutlet UIImageView *profile_Image;

@property (weak, nonatomic) IBOutlet UILabel *name_label;
@property (weak, nonatomic) IBOutlet UITextField *name_TF;
@property (weak, nonatomic) IBOutlet UILabel *email_label;
@property (weak, nonatomic) IBOutlet UITextField *email_TF;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumber_label;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumber_TF;

@property NSString *textFromSendRequest;
@property (weak, nonatomic) IBOutlet UIButton *changePwd_btn;
- (IBAction)changePwd_btnAction:(id)sender;
- (IBAction)save_btnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *save_btn;

- (IBAction)edit_btnAction:(id)sender;

@property UIImageView*pickin_img;

//url requests

@property NSURLSession*urlSession;
@property NSURLSessionDataTask*dataTask;
@property NSMutableURLRequest*urlRequest;
@property NSMutableDictionary*responseObj;

@end
