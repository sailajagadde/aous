//
//  SingleDetailsVC.m
//  HandasahUser
//
//  Created by MuraliKrishna on 08/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "SingleDetailsVC.h"
#import "VisitFirstVC.h"
#import "ServiceProviderListVC.h"

@interface SingleDetailsVC ()

@end

@implementation SingleDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _serName.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"sername"]];
    
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        self.descTV.textAlignment = NSTextAlignmentRight;
        
    } else{
        
        self.descTV.textAlignment = NSTextAlignmentLeft;
        
    }
    
    _descTV.text =  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"serdescription"]];
    dispatch_async(dispatch_get_main_queue(), ^{
        //[_serImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%s%@",basePath,[[NSUserDefaults standardUserDefaults]objectForKey:@"serimage"]]]];
        [_serImageView sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"serimage"]]];
    });
    
    //1 ==> Presence of sub services
    //0 ==> Absence of sub services
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"subSerFlag"] intValue] == 1){
        _price.text = @"";
        self.priceStatic.text = @"";
    }
    else{
        _price.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"sercost"]];
        self.priceStatic.text = [[SharedClass sharedInstance]languageSelectedString:@"Price:"];
    }
    
    self.descLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Description"];
    [_closeBtn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"CLOSE"] forState:UIControlStateNormal];
    [_selectBtn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"SELECT"] forState:UIControlStateNormal];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeBtnAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)selectBtnAction:(id)sender {
    
    
//        ServiceProviderListVC * forVisit = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceProviderListVC"];
//        forVisit.serID = [[NSUserDefaults standardUserDefaults]objectForKey:@"serID"];
//        [self.navigationController pushViewController:forVisit animated:YES];
    
    VisitFirstVC * forVisit = [self.storyboard instantiateViewControllerWithIdentifier:@"VisitFirstVC"];
    NSLog(@"serid %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"serID"]);
    forVisit.serIDStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"serID"];
    forVisit.subServiceFlagStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"subSerFlag"];
    //subSerFlag
    [self.navigationController pushViewController:forVisit animated:YES];

}
@end
