//
//  CancelRequests.h
//  HandasahUser
//
//  Created by MuraliKrishna on 23/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelRequests : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuBtn;

@property (weak, nonatomic) IBOutlet UIButton *singleBtn;
@property (weak, nonatomic) IBOutlet UIButton *packageBtn;
@property (weak, nonatomic) IBOutlet UITableView *cancelOrderTableView;

- (IBAction)viewDetailsBtnAction:(id)sender;
@end
