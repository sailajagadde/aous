//
//  FirstVisitViewController.h
//  HandasahUser
//
//  Created by Apple on 5/13/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FSCalendar/FSCalendar.h>

@interface FirstVisitViewController : UIViewController<FSCalendarDelegate,FSCalendarDataSource,FSCalendarDelegateAppearance,UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *visit_cv;
- (IBAction)btn_Back:(id)sender;
//- (IBAction)submitAction:(id)sender;
- (IBAction)submit_btnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submit_btn;


@property (weak, nonatomic) IBOutlet UITextField *time_TF;


@property (weak, nonatomic) IBOutlet FSCalendar *calender_view;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calenderHeightConstraint;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calenderheightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightcons;




@property (weak, nonatomic) IBOutlet UILabel *desc_label;
@property (weak, nonatomic) IBOutlet UITextView *desc_TV;

@property NSString *descStr;
@property NSString *serIDStr;
@property (weak, nonatomic) IBOutlet UILabel *PleaseEnterDescLabel;



@end
