//
//  SharedClass.h
//  Concierge
//
//  Created by volive solutions on 12/07/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>




@interface SharedClass : NSObject


typedef void (^ dataFromJSON) (NSData * data);



+(SharedClass *)sharedInstance;

-(void)postResponseFromServerWithMethod :(NSString*)methodName Dictionary:(NSDictionary*)valuesDictionary onViewController:(UIViewController*)viewController :(dataFromJSON)complitionHandler;

-(void)getResponseFromServerMethod : (NSString*)methodName Dictionary :(NSDictionary*)valuesDictionary onViewController :(UIViewController*)viewController :(dataFromJSON)complitionHandler;


//-(void)networkAlertMessage:(NSString*)message onViewController:(UIViewController*)viewController;


-(void)showAlertWithTitle : (NSString *)titile Message: (NSString *)message OnViewController: (UIViewController *)viewController completion:(void (^)(void))completionBlock;

-(void)showProgressFor:(NSString *)str;

-(void)showProgressForSuccess: (NSString*)str;

-(void)showProgressForError:(NSString *)str;
    
//-(NSString*)languageSelectedStringForKey:(NSString*) key;
-(NSString*)languageSelectedString:(NSString*) key;

- (BOOL)connected;
@end
