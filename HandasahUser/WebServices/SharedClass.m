
//
//  SharedClass.m
//  Concierge
//
//  Created by volive solutions on 12/07/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "SharedClass.h"
#import "Handasah_PrefixHeader.pch"

@implementation SharedClass {
    
    NSString *URLString;
    NSString *postString;
    
}


static SharedClass * sharedClassObj = nil;

+(SharedClass *)sharedInstance {
    
    if (sharedClassObj == nil) {
        sharedClassObj = [[super allocWithZone:NULL]init];
    }
    
    return sharedClassObj;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
    return sharedClassObj;
}

- (id)init
{
    self = [super init];
    
    if (self != nil)
    {
        
        
    }
    return self;
}


//POST

-(void)postResponseFromServerWithMethod :(NSString*)methodName Dictionary:(NSDictionary*)valuesDictionary onViewController:(UIViewController*)viewController :(dataFromJSON)complitionHandler {
    
    [self madeURLWithMethod:methodName ValuesDictionary:valuesDictionary];
    
    NSData *postData=[postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *urlReq=[[NSMutableURLRequest alloc]init];
    
    [urlReq setURL:[NSURL URLWithString:URLString]];
    [urlReq setHTTPMethod:@"POST"];
    [urlReq setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[postData length]] forHTTPHeaderField:@"Content-Length"];
    [urlReq setHTTPBody:postData];
    
    
    NSURLSession *urlSession=[NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionDataTask *sessionDataTask=[urlSession dataTaskWithRequest:urlReq completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        
        
        NSData * JSONData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

        complitionHandler(JSONData);
        
        }];
    
    [sessionDataTask resume];

}


-(void)madeURLWithMethod :(NSString*)methodName ValuesDictionary:(NSDictionary*)valuesDictionary{
    
    
    NSString* UDIDOfMyiPhone = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString * deviceToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"device_token"];
    //NSString *deviceTocken = @"ae1bcff3fd7b3c72db3d067a3dda9e28f81c19ecf2a1b64d1f6bdedf0c52b799";
    
    
    if ([methodName isEqualToString:@"registration"]) {
        
        //http://volive.in/aous/user_services/registration
       
        //lang, full_name, email, password,mobile,phonecode_id,API-KEY,device_type = ( Android or  iOS) ,  device_token
        
        URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
        postString = [NSString stringWithFormat:@"lang=%@&full_name=%@&email=%@&password=%@&mobile=%@&phonecode_id=%@&API-KEY=%@&device_token=%@&device_type=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"full_name"],[valuesDictionary objectForKey:@"email"],[valuesDictionary objectForKey:@"password"],[valuesDictionary objectForKey:@"mobile"],[valuesDictionary objectForKey:@"phonecode_id"],APIKEY,deviceToken,@"iOS"];
        
        URLString = [URLString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    else if ([methodName isEqualToString:@"phone_code"]) {
        
       //  http://volive.in/aous/user_services/phone_code
        //lang
        
        URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
        postString = [NSString stringWithFormat:@"lang=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],APIKEY];
        
        // URLString = [URLString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    
   else if ([methodName isEqualToString:@"login"]) {
        
        //http://volive.in/aous/user_services/login
        //lang, email, password,mobile
       //device_type = ( Android or  iOS) ,  device_token
        
        URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
        postString = [NSString stringWithFormat:@"lang=%@&mobile=%@&password=%@&API-KEY=%@&device_token=%@&device_type=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"mobile"],[valuesDictionary objectForKey:@"password"],APIKEY,deviceToken,@"iOS"];
        
    }
   else if ([methodName isEqualToString:@"home"]) {
       
         //http://volive.in/aous/user_services/home
         //lang
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&API-KEY=%@&user_id=%@",[valuesDictionary objectForKey:@"lang"],APIKEY,[valuesDictionary objectForKey:@"user_id"]];
       
   }
   else if ([methodName isEqualToString:@"phone_verification"]) {
       
       //http://volive.in/aous/user_services/phone_verification
       //lang, user_id ,otp = 1234
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&user_id=%@&otp=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"user_id"],[valuesDictionary objectForKey:@"otp"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"change_password"]) {
       
       //http://volive.in/engineering/user_services/change_password
        //lang, user_id, new_password, old_password
      // http://volive.in/aous/user_services/change_password
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&user_id=%@&new_password=%@&old_password=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"user_id"],[valuesDictionary objectForKey:@"new_password"],[valuesDictionary objectForKey:@"old_password"],APIKEY];
       
   }
    
   else if ([methodName isEqualToString:@"packages_popup"]) {
       
       //http://volive.in/engineering/user_services/packages_popup
       //lang
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"visiting_screen"]) {
       
       //http://volive.in/engineering/user_services/visiting_screen
       //service_id,package_id,request_type,lang,API-KEY
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&service_id=%@&package_id=%@&request_type=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"service_id"],[valuesDictionary objectForKey:@"package_id"],[valuesDictionary objectForKey:@"request_type"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"user_requests"]) {
       
       //http://volive.in/engineering/user_services/user_requests
       //http://volive.in/aous/user_services/user_requests
       //lang, user_id, request_status ( 1 for Pending , 3 for Completed , 4 for cancelled, 2 for Rejected  )
       //lang , user_id , request_type =  ( 1 for Single Visit  or  2 for Package Visit )
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&user_id=%@&request_status=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"user_id"],[valuesDictionary objectForKey:@"request_status"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"choose_sp"]) {
       
       // http://volive.in/aous/user_services/choose_sp
       //lang, service_id
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&service_id=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"service_id"],APIKEY];
       
   }
    
  
   else if ([methodName isEqualToString:@"completed_requests"]) {
       
       //http://volive.in/engineering/user_services/completed_requests
       //lang,user_id, request_type
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&user_id=%@&request_type=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"user_id"],[valuesDictionary objectForKey:@"request_type"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"request_details"]) {
       
        // http://volive.in/engineering/user_services/requests_details
       //http://volive.in/aous/user_services/request_details
       //lang, request_id,
       //request_type
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&request_id=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"request_id"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"visit_details"]) {
       
       //http://volive.in/engineering/user_services/visit_details
       //lang, request_id, visit_number
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&request_id=%@&visit_number=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"request_id"],[valuesDictionary objectForKey:@"visit_number"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"notifications"]) {
       
       //http://volive.in/engineering/user_services/notifications
       //lang, user_id
       //http://volive.in/aous/user_services/notifications
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&user_id=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"user_id"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"privacy"]) {
       
      // http://volive.in/engineering/user_services/privacy
       //lang
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"terms"]) {
       
       //http://volive.in/engineering/user_services/terms
       //lang
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"contact_us"]) {
       
     //http://volive.in/engineering/user_services/contact_us
       //lang
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"forgot_password"]) {
       
       //http://volive.in/engineering/user_services/forgot_password
       //lang,email
       //http://volive.in/aous/user_services/forgot_password
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&email=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"email"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"payment"]) {
       
       //http://volive.in/engineering/user_services/payment
       //lang , request_id
       //http://volive.in/aous/user_services/payment
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&request_id=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"request_id"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"type_of_land"]) {
       
       //http://volive.in/engineering/user_services/type_of_land
       //lang
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"request_review"]) {
       
       //http://volive.in/engineering/user_services/request_review
       //lang, request_id, rating, review
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&request_id=%@&rating=%@&review=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"request_id"],[valuesDictionary objectForKey:@"rating"],[valuesDictionary objectForKey:@"review"],APIKEY];
       
   }  else if ([methodName isEqualToString:@"cancel"]) {
       
       //http://volive.in/engineering/user_services/cancel
       //lang, user_id, request_id,reason
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&user_id=%@&request_id=%@&reason=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"user_id"],[valuesDictionary objectForKey:@"request_id"],[valuesDictionary objectForKey:@"reason"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"create_password"]) {
       
   //http://volive.in/aous/user_services/create_password
      // lang, password, user_id,email,username
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&password=%@&email=%@&user_id=%@&username=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"password"],[valuesDictionary objectForKey:@"email"],[valuesDictionary objectForKey:@"user_id"],[valuesDictionary objectForKey:@"username"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"get_sub_services"]) {
       
       //http://volive.in/aous/user_services/get_sub_services
       //lang, service_id
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&service_id=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"service_id"],APIKEY];
       
   }
   else if ([methodName isEqualToString:@"calculate_cost"]) {
       
       //http://volive.in/aous/user_services/calculate_cost
       //lang, quantity, cost
       
       URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
       postString = [NSString stringWithFormat:@"lang=%@&quantity=%@&cost=%@&API-KEY=%@",[valuesDictionary objectForKey:@"lang"],[valuesDictionary objectForKey:@"quantity"],[valuesDictionary objectForKey:@"cost"],APIKEY];
       
   }
    
    NSLog(@"URLStringFor %@:%@",methodName,URLString);
    NSLog(@"Post String For %@:%@",methodName,postString);
    
}

//GET

-(void)getResponseFromServerMethod : (NSString*)methodName Dictionary :(NSDictionary*)valuesDictionary onViewController :(UIViewController*)viewController :(dataFromJSON)complitionHandler{
    
    
    [self madeURL:methodName ValuesDictionary:valuesDictionary];
    
        
    NSMutableURLRequest *URLReq = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionDataTask *sessionDataTask = [urlSession dataTaskWithRequest:URLReq completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        
        NSData *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        complitionHandler(jsonData);
        
    }];
    
    [sessionDataTask resume];
    
}

-(void)madeURL :(NSString*)methodName ValuesDictionary:(NSDictionary*)valuesDictionary{
    
    NSString * deviceToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"dev_token"];
    
    //http://voliveafrica.com/beyond/api/Mobile_Services/customerLogin?email=user@gmail.com&pwd=user123&dev_type=IPHONE&dev_token=6822258
    
    //http://voliveafrica.com/beyond/api/Mobile_Services/login/?email=hessah@volive.me&password=3y6QQiaH&dev_type=iphone&dev_token=123459
    
    if ([methodName isEqualToString:@"login/?"]) {
        
        URLString = [NSString stringWithFormat:@"%@%@email=%@&password=%@&dev_type=ios&dev_token=%@",BaseURL,methodName,[valuesDictionary objectForKey:@"email"],[valuesDictionary objectForKey:@"password"],deviceToken];
        
    }  else if ([methodName isEqualToString:@"citys"]){
        
      //http://voliveafrica.com/beyond/api/Mobile_Services/citys
    
        URLString = [NSString stringWithFormat:@"%@%@",BaseURL,methodName];
    
    }  else if ([methodName isEqualToString:@"date_deatils/?"]){
        
        // http://voliveafrica.com/beyond/api/Mobile_Services/date_deatils/?user_id=57&date=2018-04-07
        URLString = [NSString stringWithFormat:@"%@%@user_id=%@&date=%@",BaseURL,methodName,[valuesDictionary objectForKey:@"user_id"],[valuesDictionary objectForKey:@"date"]];
    }  else if ([methodName isEqualToString:@"notification_user/?"]){
        
        // http://voliveafrica.com/beyond/api/Mobile_Services/notification_user/?user_id=34
        URLString = [NSString stringWithFormat:@"%@%@user_id=%@",BaseURL,methodName,[valuesDictionary objectForKey:@"user_id"]];
    }
    else if ([methodName isEqualToString:@"reminder/?"]){
        
        // http://voliveafrica.com/beyond/api/Mobile_Services/reminder/?user_id=82&order_id=10
        //http://voliveafrica.com/beyond/api/Mobile_Services/reminder/?user_id=82&order_id=10&status=2
        
        URLString = [NSString stringWithFormat:@"%@%@user_id=%@&order_id=%@&status=%@",BaseURL,methodName,[valuesDictionary objectForKey:@"user_id"],[valuesDictionary objectForKey:@"order_id"],[valuesDictionary objectForKey:@"status"]];
        
    }
    
    else if ([methodName isEqualToString:@"change_order_status/?"]){
        
        // http://voliveafrica.com/beyond/api/Mobile_Services/change_order_status/?order_id=120&status=0
        
        URLString = [NSString stringWithFormat:@"%@%@order_id=%@&status=%@",BaseURL,methodName,[valuesDictionary objectForKey:@"order_id"],[valuesDictionary objectForKey:@"status"]];
        
    }
  
    else if ([methodName isEqualToString:@"change_language/?"]){
        
       //http://voliveafrica.com/beyond/api/Mobile_Services/change_language/?user_id=111&language=en
         // http://voliveafrica.com/beyond/api/Mobile_Services/change_language/?user_id=7&language=ar&type=1
        
        URLString = [NSString stringWithFormat:@"%@%@user_id=%@&language=%@&type=%@",BaseURL,methodName,[valuesDictionary objectForKey:@"user_id"],[valuesDictionary objectForKey:@"language"],[valuesDictionary objectForKey:@"type"]];
        
    }
    
    else if ([methodName isEqualToString:@"secretary_notification/?"]){
        
        //http://voliveafrica.com/beyond/api/Mobile_Services/secretary_notification/?secretary_id=8
        URLString = [NSString stringWithFormat:@"%@%@secretary_id=%@",BaseURL,methodName,[valuesDictionary objectForKey:@"secretary_id"]];
    }
    
    else if ([methodName isEqualToString:@"check_promocode/?"]){
        
      //  http://voliveafrica.com/beyond/api/Mobile_Services/check_promocode/?coupon_code=3212&user_id=57
        URLString = [NSString stringWithFormat:@"%@%@coupon_code=%@&user_id=%@",BaseURL,methodName,[valuesDictionary objectForKey:@"coupon_code"],[valuesDictionary objectForKey:@"user_id"]];
    }
    
    else if ([methodName isEqualToString:@"forget_password/?"]){
        
        //  http://voliveafrica.com/beyond/api/Mobile_Services/forget_password/?email=mazharradiance@gmail.com
        URLString = [NSString stringWithFormat:@"%@%@email=%@",BaseURL,methodName,[valuesDictionary objectForKey:@"email"]];
    }
    
    
    
   
    NSLog(@"URLStringFor %@:%@",methodName,URLString);
}



-(void)showAlertWithTitle : (NSString *)titile Message: (NSString *)message OnViewController: (UIViewController *)viewController completion:(void (^)(void))completionBlock {
    
    UIAlertController * alert = [UIAlertController  alertControllerWithTitle:titile  message:message  preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        //completionBlock();
        
    }]];
    
//    [alert addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//
//        completionBlock();
//
//    }]];
    

    [viewController presentViewController:alert animated:YES completion:nil];
    
    
}

//Network checking

- (BOOL)connected
{
    Reachability *reach = [Reachability reachabilityForInternetConnection];

    if ([reach isReachable]) {
        NSLog(@"Device is connected to the internet");
        return TRUE;
    }
    else {
        NSLog(@"Device is not connected to the internet");
        return FALSE;
    }
}



-(void)showProgressFor:(NSString *)str{
    
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedString:@"Loading \n Please wait"]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showWithStatus:str];
    [SVProgressHUD show];
    
    
}

-(void)showProgressForSuccess:(NSString*)str{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showSuccessWithStatus:str];
    [SVProgressHUD dismissWithDelay:2.0];

}

-(void)showProgressForError:(NSString *)str{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showErrorWithStatus:str];
    [SVProgressHUD dismissWithDelay:2.0];

}

    
// for change language

-(NSString*)languageSelectedString:(NSString*) key
{

    NSBundle *path;
    NSString *selectedLanguage;
    NSString *str ;
    // NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"currentLanguage"]);

    selectedLanguage = [[NSUserDefaults standardUserDefaults]valueForKey:@"currentLanguage"];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"currentLanguage"]);
    if ([selectedLanguage isEqualToString:ARABIC_LANGUAGE])
    {

        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"ar" ofType:@"lproj"]];
        str = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(key, @"Localizable", path, nil)];
    }else{

        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
        str = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(key, @"english", path, nil)];

    }
    //NSBundle* languageBundle = [NSBundle bundleWithPath:path];

    // NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
    return str;
}



@end
