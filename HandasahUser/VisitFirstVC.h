//
//  VisitFirstVC.h
//  HandasahUser
//
//  Created by MuraliKrishna on 07/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FSCalendar/FSCalendar.h>
#import <QBImagePickerController/QBImagePickerController.h>


#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface VisitFirstVC : UIViewController<UITextViewDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,QBImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet FSCalendar *calView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;

@property (weak, nonatomic) IBOutlet UITextField *time_TF;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UITextView *desc_TV;

@property (weak, nonatomic) IBOutlet UIButton *request_Btn;
- (IBAction)reqBtnAction:(id)sender;

@property NSString *descStr;
@property NSString *serIDStr;

@property NSString *subServiceFlagStr;

@property NSString *spIDStr;
- (IBAction)back_Action:(id)sender;


@property (strong, nonatomic) NSString *lat;
@property (strong, nonatomic) NSString *lng;

@property (strong, nonatomic) IBOutlet GMSMapView *gmsGoogleMapView;
@property (strong, nonatomic) IBOutlet UITextView *adressTextview;
@property (strong, nonatomic) IBOutlet UITextField *chooseDate_TF;
@property (strong, nonatomic) IBOutlet UITextField *chooseTime_TF;
@property (strong, nonatomic) IBOutlet UITextField *chooseServiceTF;



- (IBAction)minusAction:(id)sender;
- (IBAction)plusAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *quantityLabel;
@property (strong, nonatomic) IBOutlet UILabel *costLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionStatic;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTV;

@property (strong, nonatomic) IBOutlet UIButton *uploadPhotoButton;
- (IBAction)uploadPhotoOrVideoAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *minusOutlet;
@property (strong, nonatomic) IBOutlet UIButton *plusOutlet;
@property (strong, nonatomic) IBOutlet UIStackView *stackViewIncrement;
@property (strong, nonatomic) IBOutlet UIButton *dropDownBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *chooseServiceTFTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *descTopConstraint;

@property (nonatomic, strong) PHImageRequestOptions *requestOptions;
@property (nonatomic, strong) PHVideoRequestOptions *mediarequestOptions;

@property (strong, nonatomic) IBOutlet UILabel *noteStatic;


@end
