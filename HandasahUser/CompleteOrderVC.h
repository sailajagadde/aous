//
//  CompleteOrderVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompleteOrderVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menuBtn;
@property (strong, nonatomic) IBOutlet UIButton *singleBtn;
@property (strong, nonatomic) IBOutlet UIButton *packageBtn;
//@property (strong, nonatomic) IBOutlet UITableView *completeOrderTableObj;

@property (weak, nonatomic) IBOutlet UITableView *completeOrd_tableview;

- (IBAction)single_btnAction:(id)sender;
- (IBAction)package_btnAction:(id)sender;

- (IBAction)ratingBtnAction:(id)sender;

- (IBAction)makePayment_btnAction:(id)sender;
- (IBAction)viewDetails_btnAction:(id)sender;

@property NSString *fileStr;

@end
