//
//  ServiceProviderListVC.h
//  HandasahUser
//
//  Created by MuraliKrishna on 21/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceProviderListVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn;
- (IBAction)backBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *listTV;
@property NSString *serID;
@end
