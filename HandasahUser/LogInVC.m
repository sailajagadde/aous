//
//  LogInVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "LogInVC.h"
#import "RESideMenu.h"
#import "HomeViewController.h"
#import "RegisterVC.h"
#import "PackagesVC.h"
#import "ForgotPassword.h"
#import "AppDelegate.h"
#import "TabBar.h"
@interface LogInVC ()<RESideMenuDelegate>
{
    RESideMenu * sideMenu;
    NSString *emailCheckStr;
    AppDelegate *appDelegate;
}
@end

@implementation LogInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    //emailCheckStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    [self loadAttributes];
    // Do any additional setup after loading the view.
}


-(void)loadAttributes{
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    self.emailID_TF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    self.password_TF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
    self.scroll_View.scrollEnabled = NO;
    
    NSString * selectedLanguage = [[NSUserDefaults standardUserDefaults]valueForKey:@"currentLanguage"];

    if([selectedLanguage isEqualToString:@"1"]){


    }
    else if ([selectedLanguage isEqualToString:@"2"]){


    }
    else{

        [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"currentLanguage"];
    }

    NSLog(@"lang %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"currentLanguage"]);
    [self setLanguage];
    
    

}

-(void)setLanguage{
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
        UITextView.appearance.semanticContentAttribute= UISemanticContentAttributeForceRightToLeft;
    } else{
        
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
        UITextView.appearance.semanticContentAttribute= UISemanticContentAttributeForceLeftToRight;
    }
    
    self.makeSerStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Makes your service easy & save your time with engineering works"];
    
    self.emailID_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Email Id/Mobile Number"];
    self.password_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Password"];
    [self.forgotPwd_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Forgot Your Password?"] forState:UIControlStateNormal];
    [self.signIn_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"SIGN IN"] forState:UIControlStateNormal];
    [self.signUP_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Sign Up"] forState:UIControlStateNormal];
    self.dontHaveAccount_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Don't have an account?"];
    [self.changeLanguageBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Change Language"] forState:UIControlStateNormal];
}



-(void)dismissKeyboard{
    
    [self.emailID_TF resignFirstResponder];
    [self.password_TF resignFirstResponder];
    self.scroll_View.scrollEnabled = YES;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    if (textField == self.emailID_TF) {
        [self.password_TF becomeFirstResponder];
    }
    else if (textField == self.password_TF){
        [self.password_TF resignFirstResponder];
        
    }
    self.scroll_View.scrollEnabled = YES;
    [self.scroll_View setContentOffset:CGPointMake(0, -self.scroll_view.contentInset.top) animated:YES];
    return YES;

}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signInBtn:(id)sender {
    
  
    
    
    [self loginServiceCall];
//    if ([emailCheckStr isEqualToString:@""]) {
//        UIAlertController * alert = [UIAlertController
//                                     alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
//                                     message:@"update profile"
//                                     preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//             [self loginServiceCall];
//        }];
//
//        [alert addAction:yesButton];
//
//        [self presentViewController:alert animated:YES completion:nil];
//    }
    
//    else{
//        [self loginServiceCall];
//    }
}
- (IBAction)signUpAction:(id)sender {
   
    RegisterVC *signUp = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
    [self.navigationController pushViewController:signUp animated:YES];

}


- (IBAction)forgotPassword_btnAction:(id)sender {
    ForgotPassword *forgot = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPassword"];
    [self.navigationController pushViewController:forgot animated:YES];
}

//Service Calls
//MARK:- LOGIN SERVICE CALL
-(void)loginServiceCall{
    
    // http://volive.in/aous/user_services/login
    //lang, email, password
    
    
    
    
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    NSString *deviceTocken = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"device_token"]];
   // NSString *deviceTocken = @"ae1bcff3fd7b3c72db3d067a3dda9e28f81c19ecf2a1b64d1f6bdedf0c52b799";
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    
  
    [valuesDictionary setObject:self.emailID_TF.text forKey:@"mobile"];
 
    [valuesDictionary setObject:self.password_TF.text forKey:@"password"];
    [valuesDictionary setObject:deviceTocken forKey:@"device_token"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
    } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"login" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)data];
        NSLog(@"json data %@",jsonDict);
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"email"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
           // NSString *str=@"checkemail";
            [[NSUserDefaults standardUserDefaults]setObject:str forKey:@"checking"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                [[NSUserDefaults standardUserDefaults]setObject:[[jsonDict objectForKey:@"data"]objectForKey:@"username"] forKey:@"username"];
                [[NSUserDefaults standardUserDefaults]setObject:[[jsonDict objectForKey:@"data"]objectForKey:@"user_id"] forKey:@"user_id"];
                [[NSUserDefaults standardUserDefaults]setObject:[[jsonDict objectForKey:@"data"]objectForKey:@"email"] forKey:@"email"];
                if ([[[jsonDict objectForKey:@"data"]objectForKey:@"email"] isEqualToString:@""]) {
                    [[NSUserDefaults standardUserDefaults]setObject:@"show" forKey:@"checking"];
                }
                else{
                     [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"checking"];
                }
                [[NSUserDefaults standardUserDefaults]setObject:self.password_TF.text forKey:@"password"];
                
                NSString *imageStr = [NSString stringWithFormat:@"%s%@",basePath,[[jsonDict objectForKey:@"data"]objectForKey:@"image"]];
                [[NSUserDefaults standardUserDefaults]setObject:imageStr forKey:@"image"];
                
                //[[NSUserDefaults standardUserDefaults]setObject:[[jsonDict objectForKey:@"image"]objectForKey:@"image"] forKey:@"image"];
                 [[NSUserDefaults standardUserDefaults]setObject:[[jsonDict objectForKey:@"data"]objectForKey:@"phone_number"] forKey:@"phone_number"];
                
                
                //appDelegate.selectTabItem =@"0";
              //  HomeViewController *home=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                
                
                
                TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                 appDelegate.selectTabItem=@"0";
                RESideMenu *sideMenuViewController;
                
                sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
                
                HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
                sideMenuViewController.delegate = self;
                
                [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
                
                [self.navigationController pushViewController:tab animated:YES];
                
                
                
                /*PackagesVC *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"PackagesVC"];
                [self.navigationController pushViewController:menu animated:YES];
                [[NSUserDefaults standardUserDefaults]setObject:imageStr forKey:@"image"];*/
                
            });
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                //NSDictionary *dic;
                NSString *noDataMessage;
                noDataMessage = jsonDict ? [jsonDict objectForKey:@"message"] : @"Server Slow";
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:noDataMessage OnViewController:self completion:^{  }];
                
            });
            
        }
        
    }];
   
}




- (IBAction)changeLanguageAction:(id)sender {
    
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] message:[[SharedClass sharedInstance]languageSelectedString:@"Select Language"] preferredStyle:UIAlertControllerStyleActionSheet];
    
    [ac addAction:[UIAlertAction actionWithTitle:@"English" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"currentLanguage"];
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
        [self setLanguage];
        
    }]];
    
    [ac addAction:[UIAlertAction actionWithTitle:@"عربى" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[NSUserDefaults standardUserDefaults]setValue:@"2" forKey:@"currentLanguage"];
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
        [self setLanguage];
    }]];
    
    [ac addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [ac dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:ac animated:YES completion:nil];
    
    
    
    
}
@end



