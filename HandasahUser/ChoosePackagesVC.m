//
//  ChoosePackagesVC.m
//  HandasahUser
//
//  Created by MuraliKrishna on 12/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "ChoosePackagesVC.h"
#import "ChoosePackageCell.h"
#import "ChoosePackageDetailsVC.h"
#import "AppDelegate.h"

@interface ChoosePackagesVC ()<RESideMenuDelegate>{
    
    NSMutableArray *packageNamesArray;
    NSMutableArray *packageCostArray;
    NSMutableArray *packageIdArray;
    NSMutableArray *noOfVisitsArray;
    NSMutableArray *descriptionArray;
    NSMutableArray *packageImageArray;
    ChoosePackageCell *cell;
    NSString * packageIdStr;
    
    NSMutableArray *checkArray;
    
    RESideMenu * sideMenu;
    BOOL checkBool;
    
    AppDelegate *appDelegate;
    NSString *checkStr;
    
}

@end

@implementation ChoosePackagesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    self.name_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Choose Packages"];
    [self.select_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"SELECT"] forState:UIControlStateNormal];
    checkBool = false;
    // Do any additional setup after loading the view.
 
    [self ServiceCallForPackages];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return packageNamesArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    cell = [self.choosePackage_Tableview dequeueReusableCellWithIdentifier:@"ChoosePackageCell" forIndexPath:indexPath];
  
    cell.packageName_label.text = [packageNamesArray objectAtIndex:indexPath.row];
    cell.packageCost_label.text = [packageCostArray objectAtIndex:indexPath.row];
    cell.visit_label.text = [noOfVisitsArray objectAtIndex:indexPath.row];
  
   
    
    if ([checkArray[indexPath.row]isEqualToString:@"YES"]) {
        
        cell.check_imageView.image = [UIImage imageNamed:@"check box"];
        packageIdStr = [NSString stringWithFormat:@"%@",[packageIdArray objectAtIndex:indexPath.row]];
        
        [[NSUserDefaults standardUserDefaults]setObject:packageIdStr forKey:@"packageID"];
        
        // packageIdStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"packageID"];
    }
    else{
        cell.check_imageView.image = [UIImage imageNamed:@"uncheck"];

    }
    
  
  
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    checkBool = true;
    
    for (int i=0; i<checkArray.count; i++) {
        
        if (i==indexPath.row) {
            
            [checkArray replaceObjectAtIndex:i withObject:@"YES"];
            
            [[NSUserDefaults standardUserDefaults]setObject:[packageNamesArray objectAtIndex:indexPath.row] forKey:@"name"];
            [[NSUserDefaults standardUserDefaults]setObject:[packageCostArray objectAtIndex:indexPath.row] forKey:@"cost"];
            [[NSUserDefaults standardUserDefaults]setObject:[noOfVisitsArray objectAtIndex:indexPath.row] forKey:@"visits"];
            [[NSUserDefaults standardUserDefaults]setObject:[descriptionArray objectAtIndex:indexPath.row] forKey:@"description"];
            [[NSUserDefaults standardUserDefaults]setObject:[packageImageArray objectAtIndex:indexPath.row] forKey:@"image"];
            
            ChoosePackageDetailsVC *payVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChoosePackageDetailsVC"];
            //payVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            payVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [payVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:payVC animated:NO completion:nil];

           
            
            
            
            
        }
        else{
            [checkArray replaceObjectAtIndex:i withObject:@"NO"];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.choosePackage_Tableview reloadData];
    });
  
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 65;
    
}



- (IBAction)selectBtn_action:(id)sender {
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.choosePackage_Tableview];
    NSIndexPath *indexPath = [self.choosePackage_Tableview indexPathForRowAtPoint:btnPosition];
    
    //[[NSUserDefaults standardUserDefaults]setObject:@"package" forKey:@"type"];
    
    if (checkBool == false) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Select Package"] OnViewController:self completion:nil];
    }
    else {
        //[[NSUserDefaults standardUserDefaults]setObject:@"2" forKey:@"typeStr"];
        
        appDelegate.selectTabItem =@"0";
        
        TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        RESideMenu *sideMenuViewController;
        sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
        
        HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
        sideMenuViewController.delegate = self;
        
        [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
        
        [self.navigationController pushViewController:tab animated:YES];

    }
    }


//Service call for choose packages

-(void)ServiceCallForPackages{
    
    //http://volive.in/engineering/user_services/packages_popup
    //lang
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
    } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"packages_popup" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            
            NSArray * packagesArray = [[NSArray alloc]initWithArray:[[jsonDict objectForKey:@"data"]objectForKey:@"packages"]];
            
            packageNamesArray = [[NSMutableArray alloc]init];
            packageCostArray = [[NSMutableArray alloc]init];
            packageIdArray = [[NSMutableArray alloc]init];
            noOfVisitsArray = [[NSMutableArray alloc]init];
            checkArray = [[NSMutableArray alloc]init];
            descriptionArray = [[NSMutableArray alloc]init];
            packageImageArray = [[NSMutableArray alloc]init];
            
            for (int i = 0; i<packagesArray.count; i++) {
                
                //NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
                
                [packageNamesArray addObject:[[packagesArray objectAtIndex:i]objectForKey:@"package_name"]];
                [packageCostArray addObject:[[packagesArray objectAtIndex:i]objectForKey:@"price"]];
                [packageIdArray addObject:[[packagesArray objectAtIndex:i]objectForKey:@"package_id"]];
                [noOfVisitsArray addObject:[[packagesArray objectAtIndex:i]objectForKey:@"num_visits"]];
                [descriptionArray addObject:[[packagesArray objectAtIndex:i]objectForKey:@"pack_description"]];
                [packageImageArray addObject:[[packagesArray objectAtIndex:i]objectForKey:@"pack_image"]];
                
                [checkArray addObject:@"NO"];
            }
            
            NSLog(@"check array %@",checkArray);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.choosePackage_Tableview reloadData];
                [SVProgressHUD dismiss];
            });
            
        }
        
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showProgressFor:[jsonDict objectForKey:@"message"]];
                //[[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:[[SharedClass sharedInstance]languageSelectedString:@"message"]] OnViewController:self completion:nil];
            });
            
        }
        
    }];
    
}



- (IBAction)close_btnAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
