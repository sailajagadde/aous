//
//  RatingView.h
//  HandasahUser
//
//  Created by MuraliKrishna on 09/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface RatingView : UIViewController<UITextViewDelegate>
- (IBAction)backBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *writeReview;
@property (weak, nonatomic) IBOutlet UITextView *reviewTV;
@property (weak, nonatomic) IBOutlet UIButton *submit;
- (IBAction)submitBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *successMsg;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *rateview;



- (IBAction)onClickRatingView:(id)sender;


@end
