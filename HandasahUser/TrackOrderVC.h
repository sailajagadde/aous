//
//  TrackOrderVC.h
//  HandasahUser
//
//  Created by Apple on 5/13/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
@interface TrackOrderVC : UIViewController<CLLocationManagerDelegate,GMSMapViewDelegate>
@property (strong, nonatomic) IBOutlet GMSMapView *googleMapView;
@property (strong, nonatomic) NSString *lat;
@property (strong, nonatomic) NSString *lng;
@property (weak, nonatomic) IBOutlet UILabel *tracklabel;

- (IBAction)btn_Bak1:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *homeBtn;
- (IBAction)homeBtnAction:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *ordeReq;
@property (weak, nonatomic) IBOutlet UILabel *orderAccept;
@property (weak, nonatomic) IBOutlet UILabel *paymentComplete;
@property (weak, nonatomic) IBOutlet UILabel *orderComplete;

@end
