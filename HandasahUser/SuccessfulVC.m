//
//  SuccessfulVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "SuccessfulVC.h"
#import "PaymentVC.h"
#import "TrackOrderVC.h"
@interface SuccessfulVC ()

@end

@implementation SuccessfulVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
   
    self.successLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Successful"];
    self.thankLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"THANK YOU"];
     self.yourPayLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Your payment was completed. Thank you for your order!"];
     self.pleaseCheckLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Please check the info below"];
     self.dateStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Date"];
     self.paymentStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Payment Type"];
     self.detailStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Details"];
     self.transNoStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Transaction No"];
    self.orderTotalStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Order Total"];
    
    
    [self.trackOrderBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"TRACK ORDER"] forState:UIControlStateNormal];
    
//    UINavigationBar *navibar =[[UINavigationBar alloc]init];
//    self.navigationController.navigationBar.tintColor= [UIColor colorWithRed:62.00/255.0 green:180.00/255.0 blue:213.00/255.0 alpha:1];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_Back:(id)sender {
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
//    PaymentVC *gotoVC =[self.storyboard instantiateViewControllerWithIdentifier:@"PaymentVC"];
//    [self.navigationController pushViewController:gotoVC animated:YES];
    
    
}

- (IBAction)btn_TrackOrder:(id)sender {
    TrackOrderVC *gotoVC =[self.storyboard instantiateViewControllerWithIdentifier:@"TrackOrderVC"];
    [self.navigationController pushViewController:gotoVC animated:YES];
    //[self presentViewController:gotoVC animated:YES completion:nil];
    
}
@end
