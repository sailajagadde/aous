//
//  TermsCondVC.m
//  HandasahUser
//
//  Created by MuraliKrishna on 12/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "TermsCondVC.h"

@interface TermsCondVC ()<RESideMenuDelegate>

@end

@implementation TermsCondVC

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:106.00/255.0 green:189.00/255.0 blue:239.00/255.0 alpha:1];
    //self.back_btn.tintColor=[UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Terms&Conditions"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
        UITextView.appearance.semanticContentAttribute= UISemanticContentAttributeForceRightToLeft;
    } else{
        
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
        UITextView.appearance.semanticContentAttribute= UISemanticContentAttributeForceLeftToRight;
    }
    
//    NSURL* url = [NSURL URLWithString:self.fileStr];
//    NSURLRequest* request = [NSURLRequest requestWithURL:url];
//    [self.webViewPdf loadRequest:request];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self ServiceCallForTC];
    
}
- (IBAction)back_action:(id)sender {
    
    if ([_sideStr isEqualToString:@"reg"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        self.back_btn.target =self;
        self.back_btn.action = @selector(presentLeftMenuViewController:);
    }
    
}

-(void)ServiceCallForTC{
    //http://volive.in/engineering/user_services/terms
    //lang
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"terms" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                //self.terms_TV.text = [[jsonDict objectForKey:@"data"]objectForKey:@"terms"];
                
                
                
                NSString *termsStr = [[[jsonDict objectForKey:@"data"]objectForKey:@"terms"]objectForKey:@"content_en"];
                
                NSAttributedString *attString = [[NSAttributedString alloc] initWithData:[termsStr dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
                
                NSLog(@"des str %@",termsStr);
                
                NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
                
                if ([str isEqualToString:@"2"]) {
                    self.terms_TV.textAlignment = NSTextAlignmentRight;
                } else{
                    self.terms_TV.textAlignment = NSTextAlignmentLeft;
                    
                }
                
                self.terms_TV.attributedText = attString;
                
            });
            
        }
        
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:@"Alert" Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}

@end
