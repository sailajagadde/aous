//
//  RegisterVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "RegisterVC.h"
#import "MobileNumberVC.h"
#import "PhoneVerificationVC.h"
#import "TermsCondVC.h"

//#import "SharedClass.h"

@interface RegisterVC (){
    BOOL isChecked;
    UIToolbar *pickerToolBar;
    UIPickerView *pickerView;
    NSMutableArray *phoneCodeArray;
    NSMutableArray *phonecode_idArray;
    NSMutableArray *pickerViewItems_Array;
   
    NSInteger row;
 
}

@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadAttributes];
}

-(void)loadAttributes{
    
    isChecked = false;
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
   
    [self setLanguage];
    [self loadPickerData];
}

-(void)setLanguage{
    
    self.makeStaticlabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Makes your service easy & save your time with engineering works"];
    
    self.name_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Full Name"];
    self.emailID_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Email Id"];
    self.password_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Password"];
    [self.register_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"REGISTER"] forState:UIControlStateNormal];
    [self.login_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"LogIn"] forState:UIControlStateNormal];
    self.alreadyHaveAccount_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Already have an account?"];
     [self.termsBtn_Outlet setTitle:[[SharedClass sharedInstance] languageSelectedString:@"I agree to Terms & Conditions"] forState:UIControlStateNormal];
    
}

-(void)dismissKeyboard{
    
    [self.name_TF resignFirstResponder];
    [self.emailID_TF resignFirstResponder];
    [self.password_TF resignFirstResponder];
    [self.mobileNumber_TF resignFirstResponder];
    
}


-(void)loadPickerData{
    
    pickerView = [[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    
    pickerView.showsSelectionIndicator = YES;
    
    [pickerView setFrame:CGRectMake(0, self.view.frame.size.height-162, self.view.frame.size.width, 162)];
    
    pickerToolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolBar.barStyle = UIBarStyleBlackOpaque;
    
    [pickerToolBar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc]init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Done"] style: UIBarButtonItemStyleDone target: self action: @selector(pickerDoneClicked)];
    
    [doneBtn setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style: UIBarButtonItemStyleDone target: self action: @selector(pickerCancelClicked)];
    
    [cancelBtn setTintColor:[UIColor whiteColor]];
    
    [barItems addObject:cancelBtn];
    [barItems addObject:flexSpace];
    [barItems addObject:doneBtn];
    
    [pickerToolBar setItems:barItems];
    
    [self.countryCode_TF setInputAccessoryView:pickerToolBar];
    //[self countryList];
    
    
}

-(void)pickerDoneClicked{
    
    self.countryCode_TF.text = [phoneCodeArray objectAtIndex:[pickerView selectedRowInComponent:0]];
    self.countryID = [phonecode_idArray objectAtIndex:[pickerView selectedRowInComponent:0]];
    [self.countryCode_TF resignFirstResponder];
}

-(void)pickerCancelClicked{
        
    [self.countryCode_TF resignFirstResponder];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return phoneCodeArray.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.countryCode_TF.text = [phoneCodeArray objectAtIndex:row];
    self.countryID = [phonecode_idArray objectAtIndex:row];
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return phoneCodeArray[row];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == self.countryCode_TF){
        
        self.scroll_view.scrollEnabled = YES;
        [self.scroll_view setContentOffset:CGPointMake(0, textField.frame.origin.y+100) animated:YES];
        
        
        BOOL checkNetwork = [[SharedClass sharedInstance]connected];
        if(checkNetwork == false){
            
            NSLog(@"unreachable");
           
            //[[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Check Your Network Connection"] OnViewController:self completion:nil];
            
        }
        else{
            NSLog(@"reachable");
           // [self countryList];
            self.countryCode_TF.inputView = pickerView;
       
        }
        
    }

    return YES;
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.name_TF) {
        [self.emailID_TF becomeFirstResponder];
    }
    if (textField == self.emailID_TF) {
        [self.password_TF becomeFirstResponder];
    }
    else if (textField == self.password_TF){
        [self.countryCode_TF becomeFirstResponder];
        
    }
    else if (textField == self.countryCode_TF){
        [self.mobileNumber_TF becomeFirstResponder];
        
    }
    else if (textField == self.mobileNumber_TF){
        [self.mobileNumber_TF resignFirstResponder];
        
    }
    [self.scroll_view setContentOffset:CGPointMake(0, -self.scroll_view.contentInset.top) animated:YES];
    return YES;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
}






- (IBAction)registerAction:(id)sender {
    
   // if (self.countryID!=nil) {
        if (isChecked == true) {
           [self registerServiceCalling];
        }
        else{
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Accept terms and conditions to Sign Up"] OnViewController:self completion:nil];
        }
    //}
//    else{
//        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Select Country ID"] OnViewController:self completion:nil];
//    }

}

- (IBAction)logInAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)back_btnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}


//Service Calls


-(void)registerServiceCalling{
    
   // http://volive.in/aous/user_services/registration
    //lang, full_name, email, password,phonecode_id,mobile
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"email"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"username"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"image"];
    NSString *deviceTocken = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"device_token"]];
   // NSString *deviceTocken = @"ae1bcff3fd7b3c72db3d067a3dda9e28f81c19ecf2a1b64d1f6bdedf0c52b799";
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    //    self.types = [[NSUserDefaults standardUserDefaults]objectForKey:@"type"];
    //    //valuesDictionary = [[NSMutableDictionary alloc]init];
    //
    //    if ([self.types isEqualToString:@"1"]) {
    //        [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:self.oidStr] forKey:@"order_id"];
    //    }
    //    else if([self.types isEqualToString:@"2"]){
    //        [valuesDictionary setObject:@"" forKey:@"order_id"];
    //    }
    
//     NSString * nameutfStr = [self.name_TF.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   
//    [valuesDictionary setObject:nameutfStr forKey:@"full_name"];
//    [valuesDictionary setObject:self.emailID_TF.text forKey:@"email"];
    //[valuesDictionary setObject:self.password_TF.text forKey:@"password"];
    [valuesDictionary setObject:@"" forKey:@"phonecode_id"];
    [valuesDictionary setObject:self.mobileNumber_TF.text forKey:@"mobile"];
    [valuesDictionary setObject:deviceTocken forKey:@"device_token"];
    
    //NSString * airlineutfStr = [self.airlinesName_TF.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //[valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"sec_id"] forKey:@"secretary_id"];
    
    //NSLog(@"%@ nameutf",nameutfStr);
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
    } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"registration" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)data];
        NSLog(@"json data for register user %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                
                [[NSUserDefaults standardUserDefaults]setObject:[[jsonDict objectForKey:@"user_info"]objectForKey:@"phone_code"] forKey:@"phone_code"];
                [[NSUserDefaults standardUserDefaults]setObject:[[jsonDict objectForKey:@"user_info"]objectForKey:@"phone_number"] forKey:@"phone_number"];
                [[NSUserDefaults standardUserDefaults]setObject:[[jsonDict objectForKey:@"user_info"]objectForKey:@"user_id"] forKey:@"user_id"];
                
                PhoneVerificationVC *mobile = [self.storyboard instantiateViewControllerWithIdentifier:@"PhoneVerificationVC"];
                [self.navigationController pushViewController:mobile animated:YES];
                
            });
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                
            });
            
        }
        
    }];
    
}


-(void)countryList{

    //  http://volive.in/aous/user_services/phone_code
    //lang
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];

    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    [valuesDictionary setObject:@"en" forKey:@"lang"];
//    if ([str isEqualToString:@"2"]) {
//
//        [valuesDictionary setObject:@"ar" forKey:@"lang"];
//
//    } else{
//
//        [valuesDictionary setObject:@"en" forKey:@"lang"];
//
//    }
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"phone_code" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){

        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);

        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];

        if([status isEqualToString:@"1"]){

            NSArray * dataArray = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"data"]];

           phoneCodeArray = [[NSMutableArray alloc]init];
           phonecode_idArray = [[NSMutableArray alloc]init];

            for (int i = 0; i<dataArray.count; i++) {

                [phoneCodeArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"phonecode"]];
                [phonecode_idArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"phonecode_id"]];
            }

            NSLog(@"id array %@",phonecode_idArray);
            dispatch_async(dispatch_get_main_queue(), ^{

                [pickerView reloadAllComponents];
                [pickerView reloadInputViews];
                [SVProgressHUD dismiss];
            });

        }

        else{

            dispatch_async(dispatch_get_main_queue(), ^{

                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
            });

        }

    }];

}


- (IBAction)checkBoxBtn_Action:(id)sender {
    isChecked = !isChecked;
    
    if (isChecked == true) {
        
        [_checkBoxBtn_Outlet setImage:[UIImage imageNamed:@"check status"] forState:UIControlStateNormal];
        
    } else {
        
        [_checkBoxBtn_Outlet setImage:[UIImage imageNamed:@"check statuss"] forState:UIControlStateNormal];
        
    }
}

- (IBAction)termsBtn_Action:(id)sender {
    
    TermsCondVC *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsCondVC"];
    termsVC.sideStr = @"reg";
    [self.navigationController pushViewController:termsVC animated:YES];
    
}
@end
