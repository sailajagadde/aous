//
//  ChoosePackageDetailsVC.m
//  HandasahUser
//
//  Created by volivesolutions on 04/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "ChoosePackageDetailsVC.h"

@interface ChoosePackageDetailsVC ()

@end

@implementation ChoosePackageDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
        _packageNameLabel.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"name"]];
        _priceLabel.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"cost"]];
        _noOfVisitsLabel.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"visits"]];
        _description_TV.text =  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"description"]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [_packageImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%s%@",basePath,[[NSUserDefaults standardUserDefaults]objectForKey:@"image"]]]];
    });
    
    
    self.staticVisitsLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"No Of Visits:"];
    self.staticPriceLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Price:"];
    self.staticDescLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Description"];
    [_closeBtn_Outlet setTitle:[[SharedClass sharedInstance]languageSelectedString:@"CLOSE"] forState:UIControlStateNormal];

    // Do any additional setup after loading the view.
}

- (IBAction)closeBtn_Action:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
