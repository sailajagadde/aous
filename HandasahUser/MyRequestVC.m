//
//  MyRequestVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "MyRequestVC.h"
#import "MyRequestCell.h"
#import "RESideMenu.h"
#import "AppDelegate.h"
#import "PackageVC.h"
#import "OrderDetailsVC.h"
#import "DetailesCompleted_VC.h"

#import "Single.h"
#import "Packages.h"

#import "PaymentVC.h"

#import "CancelPopupVC.h"

@interface MyRequestVC ()<RESideMenuDelegate>
{
     AppDelegate *appDelegate;
    Single *single;
    Packages *packages;
    MyRequestCell *cell;
    NSString *checkStr;
    
    
    NSString *reqStatusStr;
    NSString *paymentStatusStr;
    
    NSMutableArray *userNameArray;
    NSMutableArray *imageArray;
    NSMutableArray *costArray;
    NSMutableArray *serviceNameArray;
    NSMutableArray *dateArray;
    NSMutableArray *timeArray;
    NSMutableArray *reqIdArray;
    NSMutableArray *reqTypeArray;
    NSMutableArray *reqStatusArray;
     NSMutableArray *fileArray;
    NSMutableArray *paymentStatusArray;
    NSString *checkBtnStr;
}
@end

@implementation MyRequestVC

- (void)viewDidLoad {

    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"My Requests"];
    [self.singleBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Single"] forState:UIControlStateNormal];
    [self.packageBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Package"] forState:UIControlStateNormal];
    
    self.packageBtn.layer.cornerRadius = 4;
       
    self.packageBtn.layer.masksToBounds = YES;
    self.packageBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    self.packageBtn.layer.borderWidth = 1.0;
    self.packageBtn.layer.cornerRadius =20;
    
    [self.packageBtn setTitleColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1] forState:UIControlStateNormal];
    
    _singleBtn.backgroundColor=[UIColor colorWithRed:2.00/255.0 green:178.00/255.0 blue:209.00/255.0 alpha:1];
    [_singleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _singleBtn.layer.cornerRadius=20;
    
    checkStr = @"single";
    
    
    CGSize screenSize = [[UIScreen mainScreen]bounds].size;
    NSLog(@"%f",screenSize.height);
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [super viewDidLoad];
   
    if ([appDelegate.strCheek isEqualToString:@"check"]) {
        self.sideBtn.target =self;
        self.sideBtn.action = @selector(presentLeftMenuViewController:);
    }else{
        self.sideBtn.target =self;
        self.sideBtn.action = @selector(back);
    }
  
    //[self.navigationController.navigationBar
    // setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"FrutigerLTArabic-55Roman" size:18] }];
   
    [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Home"]];
    
    [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"My Requests"]];
    
    [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Completed Orders"]];
    
    [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Account"]];
    
}

-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:106.00/255.0 green:189.00/255.0 blue:239.00/255.0 alpha:1];
    
    checkStr = @"single";
    dispatch_async(dispatch_get_main_queue(), ^{
         [self ServiceCallForReq];
    });
    
    [self.singleBtn setBackgroundColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]];
    [self.singleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.packageBtn setBackgroundColor:[UIColor whiteColor]];
    [self.packageBtn setTitleColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1] forState:UIControlStateNormal];
  
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return costArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    cell = [_request_tableView dequeueReusableCellWithIdentifier:@"MyRequestCell" forIndexPath:indexPath];
    
    [cell.cancelBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"CANCEL"] forState:UIControlStateNormal];
    [cell.makePaymentBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"MAKE PAYMENT"] forState:UIControlStateNormal];
    [cell.viewDetailsBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"VIEW DETAILS"] forState:UIControlStateNormal];
    
    NSString *requestType;
//    if ([checkStr isEqualToString:@"package"]) {
//        cell.noOfServicesStatic.hidden = NO;
//        cell.noOfSerLeft.text = [NSString stringWithFormat:@"%@",[visitLeftArray objectAtIndex:indexPath.row]];
//        cell.noOfServicesStatic.text = [[SharedClass sharedInstance] languageSelectedString:@"Visits Left"];
//
//    }
//    else if ([checkStr isEqualToString:@"single"]){
//        cell.noOfSerLeft.text = [NSString stringWithFormat:@"%@",[visitLeftArray objectAtIndex:indexPath.row]];
//        cell.noOfServicesStatic.hidden = YES;
//    }
    
    
    
    cell.serviceLbl.text = [[SharedClass sharedInstance] languageSelectedString:@"Service"];
    cell.costLbl.text = [[SharedClass sharedInstance] languageSelectedString:@"Cost"];
    cell.dateLbl.text = [[SharedClass sharedInstance] languageSelectedString:@"Date & Time"];
    cell.statusLbl.text = [[SharedClass sharedInstance] languageSelectedString:@"Status"];
    
    
    reqStatusStr = [NSString stringWithFormat:@"%@",[reqStatusArray objectAtIndex:indexPath.row]];
    paymentStatusStr = [NSString stringWithFormat:@"%@",[paymentStatusArray objectAtIndex:indexPath.row]];
    
    if ([reqStatusStr isEqualToString:@"0"]||[reqStatusStr isEqualToString:@"5"]) {
        cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Waiting"];
        cell.makePaymentBtn.hidden = YES;
        
        [cell.cancelBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"CANCEL"] forState:UIControlStateNormal];
        
        cell.cancelBtn.hidden = NO;
        cell.cancelBtnHeightConstarint.constant = 40;
    }
    else if ([reqStatusStr isEqualToString:@"1"]){
        if([paymentStatusStr isEqualToString:@"0"]){
            cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Accepted"];
            cell.makePaymentBtn.hidden = NO;
            [cell.makePaymentBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"MAKE PAYMENT"] forState:UIControlStateNormal];
            [cell.cancelBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"CANCEL"] forState:UIControlStateNormal];
            //            [cell.makePaymentBtn setTitle:@"Paid" forState:UIControlStateNormal];
            cell.makePaymentBtn.enabled = YES;
            
            cell.cancelBtn.hidden = NO;
            cell.cancelBtnHeightConstarint.constant = 40;
        }else if ([paymentStatusStr isEqualToString:@"1"]){
            cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Accepted"];
            cell.makePaymentBtn.hidden = NO;
            [cell.makePaymentBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Paid"] forState:UIControlStateNormal];
            cell.makePaymentBtn.enabled = NO;
            [cell.cancelBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"CANCEL"] forState:UIControlStateNormal];
            cell.cancelBtn.hidden = YES;
            cell.cancelBtnHeightConstarint.constant = 0;
        }
        //        cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Accepted"];
        //        cell.makePaymentBtn.hidden = NO;
    }
    else if ([reqStatusStr isEqualToString:@"3"]){
        cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Completed"];
    }
     else if ([reqStatusStr isEqualToString:@"6"]){
         cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Under Repair"];
         cell.makePaymentBtn.hidden = NO;
         [cell.makePaymentBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Paid"] forState:UIControlStateNormal];
         cell.makePaymentBtn.enabled = NO;
         [cell.cancelBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"CANCEL"] forState:UIControlStateNormal];
         cell.cancelBtn.hidden = YES;
         cell.cancelBtnHeightConstarint.constant = 0;
     }
   cell.name_label.text = [userNameArray objectAtIndex:indexPath.row];
    
//    cell.serCost_label.text = [NSString stringWithFormat:@"%@%@",@": ",[costArray objectAtIndex:indexPath.row]];
    cell.serType_label.text = [NSString stringWithFormat:@"%@",[serviceNameArray objectAtIndex:indexPath.row]];
    
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        cell.serviceNameTV.text = [NSString stringWithFormat:@"%@%@",@": ",[serviceNameArray objectAtIndex:indexPath.row]];
        cell.serCost_label.text = [NSString stringWithFormat:@"%@%@",[costArray objectAtIndex:indexPath.row],@": "];
         cell.date_label.text = [NSString stringWithFormat:@"%@%@%@%@",[dateArray objectAtIndex:indexPath.row],@",",[timeArray objectAtIndex:indexPath.row],@": "];
        //cell.serType1_label.textAlignment = NSTextAlignmentRight;
    } else{
        
        cell.serviceNameTV.text = [NSString stringWithFormat:@"%@%@",@": ",[serviceNameArray objectAtIndex:indexPath.row]];
        cell.serCost_label.text = [NSString stringWithFormat:@"%@%@",@": ",[costArray objectAtIndex:indexPath.row]];
         cell.date_label.text = [NSString stringWithFormat:@"%@%@%@%@",@": ",[dateArray objectAtIndex:indexPath.row],@",",[timeArray objectAtIndex:indexPath.row]];
       // cell.serType1_label.textAlignment = NSTextAlignmentLeft;
    }
    
  
//    cell.date_label.text = [NSString stringWithFormat:@"%@%@%@%@",[dateArray objectAtIndex:indexPath.row],@",",[timeArray objectAtIndex:indexPath.row],@": "];
  
    
    
    [cell.userImageview sd_setImageWithURL:[NSURL URLWithString:[imageArray objectAtIndex:indexPath.row]]];
    
    cell.userImageview.layer.cornerRadius = cell.userImageview.frame.size.width/2.0;
    
    cell.userImageview.clipsToBounds = YES;
    cell.back_view.layer.cornerRadius = 10;
    cell.back_view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.back_view.clipsToBounds = YES;
    
    
    cell.makePaymentBtn.layer.cornerRadius = 4;
    cell.makePaymentBtn.layer.masksToBounds = YES;
    cell.makePaymentBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    cell.makePaymentBtn.layer.borderWidth = 1.0;
    cell.makePaymentBtn.layer.cornerRadius =20;
    
    cell.viewDetailsBtn.layer.cornerRadius = 4;
    cell.viewDetailsBtn.layer.masksToBounds = YES;
    cell.viewDetailsBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    cell.viewDetailsBtn.layer.borderWidth = 1.0;
    cell.viewDetailsBtn.layer.cornerRadius =20;
    
    cell.cancelBtn.layer.cornerRadius = 4;
    cell.cancelBtn.layer.masksToBounds = YES;
    cell.cancelBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    cell.cancelBtn.layer.borderWidth = 1.0;
    cell.cancelBtn.layer.cornerRadius =20;
    
    return cell;
    
    
    /*cell = [_request_tableView dequeueReusableCellWithIdentifier:@"MyRequestCell" forIndexPath:indexPath];
   
    
    [cell.makePaymentBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"MAKE PAYMENT"] forState:UIControlStateNormal];
    [cell.viewDetailsBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"VIEW DETAILS"] forState:UIControlStateNormal];
    
    
    
    cell.serviceLbl.text = [[SharedClass sharedInstance] languageSelectedString:@"Service"];
    cell.costLbl.text = [[SharedClass sharedInstance] languageSelectedString:@"Cost"];
    cell.dateLbl.text = [[SharedClass sharedInstance] languageSelectedString:@"Date & Time"];
    cell.statusLbl.text = [[SharedClass sharedInstance] languageSelectedString:@"Status"];
    
    
    NSString *reqStatusStr = [NSString stringWithFormat:@"%@",[reqStatusArray objectAtIndex:indexPath.row]];
    NSString *paymentStatusStr = [NSString stringWithFormat:@"%@",[paymentStatusArray objectAtIndex:indexPath.row]];
    
    if ([reqStatusStr isEqualToString:@"0"]) {
        cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Waiting"];
        cell.makePaymentBtn.hidden = YES;
        
        
    }
    else if ([reqStatusStr isEqualToString:@"1"]){
        if([paymentStatusStr isEqualToString:@"0"]){
            cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Accepted"];
            cell.makePaymentBtn.hidden = NO;
            [cell.makePaymentBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"MAKE PAYMENT"] forState:UIControlStateNormal];
//            [cell.makePaymentBtn setTitle:@"Paid" forState:UIControlStateNormal];
            cell.makePaymentBtn.enabled = YES;
        }else if ([paymentStatusStr isEqualToString:@"1"]){
            cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Accepted"];
            cell.makePaymentBtn.hidden = NO;
            [cell.makePaymentBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Paid"] forState:UIControlStateNormal];
            cell.makePaymentBtn.enabled = NO;
        }
//        cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Accepted"];
//        cell.makePaymentBtn.hidden = NO;
    }
    else if ([reqStatusStr isEqualToString:@"3"]){
        cell.statusChange_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Completed"];
    }
    cell.name_label.text = [userNameArray objectAtIndex:indexPath.row];
    cell.serCost_label.text = [NSString stringWithFormat:@"%@%@",@": ",[costArray objectAtIndex:indexPath.row]];
    cell.serType_label.text = [NSString stringWithFormat:@"%@",[serviceNameArray objectAtIndex:indexPath.row]];
    
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        cell.serType1_label.text = [NSString stringWithFormat:@"%@%@",[serviceNameArray objectAtIndex:indexPath.row],@" :"];
    } else{
        
        cell.serType1_label.text = [NSString stringWithFormat:@"%@%@",@": ",[serviceNameArray objectAtIndex:indexPath.row]];
    }
    
    //cell.serType1_label.text = [NSString stringWithFormat:@"%@%@",@": ",[serviceNameArray objectAtIndex:indexPath.row]];
    cell.date_label.text = [NSString stringWithFormat:@"%@%@%@%@",@": ",[dateArray objectAtIndex:indexPath.row],@",",[timeArray objectAtIndex:indexPath.row]];
   // cell.statusChange_label.text =
    //[NSString stringWithFormat:@"%@%@",@": ",[reqStatusArray objectAtIndex:indexPath.row]];
    
    
    [cell.userImageview sd_setImageWithURL:[NSURL URLWithString:[imageArray objectAtIndex:indexPath.row]]];
    
    cell.userImageview.layer.cornerRadius = cell.userImageview.frame.size.width/2.0;
    
    cell.userImageview.clipsToBounds = YES;
    cell.back_view.layer.cornerRadius = 10;
    cell.back_view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.back_view.clipsToBounds = YES;
    
    
    cell.makePaymentBtn.layer.cornerRadius = 4;
    cell.makePaymentBtn.layer.masksToBounds = YES;
    cell.makePaymentBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    cell.makePaymentBtn.layer.borderWidth = 1.0;
    cell.makePaymentBtn.layer.cornerRadius =20;
   
    cell.viewDetailsBtn.layer.cornerRadius = 4;
    cell.viewDetailsBtn.layer.masksToBounds = YES;
    cell.viewDetailsBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    cell.viewDetailsBtn.layer.borderWidth = 1.0;
    cell.viewDetailsBtn.layer.cornerRadius =20;
    
    return cell; */
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([reqStatusStr isEqualToString:@"1"]){
        if([paymentStatusStr isEqualToString:@"0"]){
            return 351;
        }
        else{
            //return 276;
            return 300;
        }
    }else if ([reqStatusStr isEqualToString:@"6"]){
        //return 276;
        return 300;
    }
    else{
        return 351;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)single_btnAction:(id)sender {
    
    checkStr = @"single";
    [self.singleBtn setBackgroundColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]];
    [self.singleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.packageBtn setBackgroundColor:[UIColor whiteColor]];
    [self.packageBtn setTitleColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1] forState:UIControlStateNormal];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self ServiceCallForReq];
        [self.request_tableView reloadData];
    });
    
}

- (IBAction)package_btnAction:(id)sender {
    
    checkStr = @"package";
    [self.packageBtn setBackgroundColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]];
    [self.packageBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.singleBtn setBackgroundColor:[UIColor whiteColor]];
    self.singleBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    self.singleBtn.layer.borderWidth = 1.0;
    self.singleBtn.layer.cornerRadius =20;
    [self.singleBtn setTitleColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1] forState:UIControlStateNormal];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self ServiceCallForReq];
        [self.request_tableView reloadData];
    });
  
}

-(void)ServiceCallForReq{
    //http://volive.in/aous/user_services/user_requests --lang , user_id , request_type =  ( 1 for Single Visit  or  2 for Package Visit )
    
    //lang, user_id, request_status ( 1 for Pending , 3 for Completed , 4 for cancelled, 0 for Waiting )
    
//    NSString *requestType;
//    if ([checkStr isEqualToString:@"package"]) {
//        requestType=@"2";
//    }else if([checkStr  isEqualToString:@"single"]){
//        requestType=@"1";
//    }
    
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
    //[valuesDictionary setObject:@"154" forKey:@"user_id"];
    [valuesDictionary setObject:@"0" forKey:@"request_status"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
        if ([str isEqualToString:@"2"]) {
    
            [valuesDictionary setObject:@"ar" forKey:@"lang"];
    
        } else{
    
            [valuesDictionary setObject:@"en" forKey:@"lang"];
    
        }
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];

    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"user_requests" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        userNameArray = [[NSMutableArray alloc]init];
        imageArray = [[NSMutableArray alloc]init];
        costArray = [[NSMutableArray alloc]init];
        serviceNameArray = [[NSMutableArray alloc]init];
        dateArray = [[NSMutableArray alloc]init];
        timeArray = [[NSMutableArray alloc]init];
        reqIdArray = [[NSMutableArray alloc]init];
        reqTypeArray = [[NSMutableArray alloc]init];
        reqStatusArray = [[NSMutableArray alloc]init];
        paymentStatusArray = [[NSMutableArray alloc]init];
        
        if([status isEqualToString:@"1"]){
            
            [SVProgressHUD dismiss];
            NSArray * dataArray = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"data"]];
            
            
            if (dataArray.count == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"No Data Found"] OnViewController:self completion:nil];
                    [self.request_tableView reloadData];
                });
            } else{
                    
                    for (int i = 0; i<dataArray.count; i++) {
                        
                        [userNameArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"username"]];
                        [costArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"request_cost"]];
                        [dateArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"requested_date"]];
                        [timeArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"requested_time"]];
                        [serviceNameArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"service_type"]];
                        [reqIdArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"request_id"]];
                        [reqTypeArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"request_type"]];
                        [reqStatusArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"request_status"]];
                        [paymentStatusArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"payment_status"]];
                        [imageArray addObject:[NSString stringWithFormat:@"%s%@",basePath,[[dataArray objectAtIndex:i]objectForKey:@"image"]]];
                        
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.request_tableView reloadData];
                        [SVProgressHUD dismiss];
                    });
                    
                }
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                [self.request_tableView reloadData];
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}


- (IBAction)makePayment_btnAction:(id)sender {
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.request_tableView];
    NSIndexPath *indexPath = [self.request_tableView indexPathForRowAtPoint:btnPosition];
    
    [[NSUserDefaults standardUserDefaults]setObject:[costArray objectAtIndex:indexPath.row] forKey:@"cost"];
    [[NSUserDefaults standardUserDefaults]setObject:[reqIdArray objectAtIndex:indexPath.row] forKey:@"reqID"];
    
    PaymentVC *payVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentVC"];
    payVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [payVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:payVC animated:YES completion:nil];
  
}

- (IBAction)viewDetails_btnAction:(id)sender {
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.request_tableView];
    NSIndexPath *indexPath = [self.request_tableView indexPathForRowAtPoint:btnPosition];
    
//    self.checkDateStr=@"reqDet";
//    [[NSUserDefaults standardUserDefaults]setObject:self.checkDateStr forKey:@"show"];
    
    if ([checkStr isEqualToString:@"package"]) {
        Packages *packages = [self.storyboard instantiateViewControllerWithIdentifier:@"Packages"];
        packages.reqIdStr = [reqIdArray objectAtIndex:indexPath.row];
        packages.reqTypeStr = [reqTypeArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:packages animated:YES];
    }
    else{
        OrderDetailsVC *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailsVC"];
        detailsVC.reqIdStr = [reqIdArray objectAtIndex:indexPath.row];
        detailsVC.reqTypeStr = [reqTypeArray objectAtIndex:indexPath.row];
        //detailsVC.fileStr = [fileArray objectAtIndex:indexPath.row];
        //detailsVC.checkBtnString = @"no";
        [self.navigationController pushViewController:detailsVC animated:YES];
        
    }
 }

- (IBAction)cancelBtnAction:(id)sender {
    
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.request_tableView];
    NSIndexPath *indexPath = [self.request_tableView indexPathForRowAtPoint:btnPosition];
    
    //[[NSUserDefaults standardUserDefaults]setObject:[costArray objectAtIndex:indexPath.row] forKey:@"cost"];
    [[NSUserDefaults standardUserDefaults]setObject:[reqIdArray objectAtIndex:indexPath.row] forKey:@"reqID"];
    
    CancelPopupVC *cancelVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CancelPopupVC"];
    cancelVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [cancelVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:cancelVC animated:YES completion:nil];
    
}



@end
