//
//  CompleteOrderVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "CompleteOrderVC.h"
#import "CompleteOrderCell.h"
#import "RESideMenu.h"
#import "DetailesCompleted_VC.h"
#import "DetailsVC.h"
#import "AppDelegate.h"

#import "Packages.h"
#import "OrderDetailsVC.h"
#import "RatingView.h"

@interface CompleteOrderVC ()<RESideMenuDelegate>{
    AppDelegate *appDelegate;
}
//CompleteOrderCell
@end

@implementation CompleteOrderVC{
     NSString *checkStr;
    NSMutableArray *userNameArray;
    NSMutableArray *costArray;
    NSMutableArray *serviceNameArray;
    NSMutableArray *dateArray;
    NSMutableArray *timeArray;
    NSMutableArray *reqIdArray;
    NSMutableArray *reqTypeArray;
    NSMutableArray *reqStatusArray;
    NSMutableArray *imageArray;
    NSMutableArray *fileArray;
    
    NSArray * dataArray;
    NSString *checkbtnStr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.menuBtn.target =self;
//    self.menuBtn.action = @selector(presentLeftMenuViewController:);
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Completed Orders"];
    
    [self.singleBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Single"] forState:UIControlStateNormal];
    [self.packageBtn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"Package"] forState:UIControlStateNormal];
    
    self.packageBtn.layer.cornerRadius = 4;
    //  self.english_Btn.layer.shadowOffset = CGSizeMake(1, 1);
    // self.english_Btn.layer.shadowRadius = 3.0;
    // self.english_Btn.layer.shadowOpacity = 0.6;
    self.packageBtn.layer.masksToBounds = YES;
    self.packageBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    self.packageBtn.layer.borderWidth = 1.0;
    self.packageBtn.layer.cornerRadius =20;
    
    [self.packageBtn setTitleColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1] forState:UIControlStateNormal];
    
    _singleBtn.backgroundColor=[UIColor colorWithRed:2.00/255.0 green:178.00/255.0 blue:209.00/255.0 alpha:1];
    [_singleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _singleBtn.layer.cornerRadius=20;
    
    checkStr = @"single";
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if ([appDelegate.strCheek isEqualToString:@"check"]) {
        self.menuBtn.target =self;
        self.menuBtn.action = @selector(presentLeftMenuViewController:);
    }else{
        self.menuBtn.target =self;
        self.menuBtn.action = @selector(back);
    }
    [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Home"]];
    
    [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"My Requests"]];
    
    [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Completed Orders"]];
    
    [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Account"]];
    
}

-(void)back{
    
    
    //    self.sideBtn.target =self;
    //    self.sideBtn.action = @selector(presentLeftMenuViewController:);
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:106.00/255.0 green:189.00/255.0 blue:239.00/255.0 alpha:1];
    
    checkStr = @"single";
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self ServiceCallForCompleteReq];
    });
    [self.completeOrd_tableview reloadData];
    
    [self.singleBtn setBackgroundColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]];
    [self.singleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.packageBtn setBackgroundColor:[UIColor whiteColor]];
    [self.packageBtn setTitleColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1] forState:UIControlStateNormal];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return costArray.count;
   
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CompleteOrderCell *cell = [self.completeOrd_tableview dequeueReusableCellWithIdentifier:@"CompleteOrderCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[CompleteOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CompleteOrderCell"];
    }

    
    [cell.makePayment_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"MAKE PAYMENT"] forState:UIControlStateNormal];
    [cell.viewDetails_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"VIEW DETAILS"] forState:UIControlStateNormal];
        
    cell.serviceStatic_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Service"];
    cell.costStatic_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Cost"];
    cell.dateStatic_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Date & Time"];
    cell.statusStatic_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Status"];
    
    NSString *reqStatusStr = [NSString stringWithFormat:@"%@",[reqStatusArray objectAtIndex:indexPath.row]];
    
    if ([reqStatusStr isEqualToString:@"0"]) {
        cell.status_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Waiting"];
        cell.makePayment_btn.hidden = YES;
        
        
    }
    else if ([reqStatusStr isEqualToString:@"1"]){
        cell.status_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Accepted"];
        cell.makePayment_btn.hidden = NO;
    }
    else if ([reqStatusStr isEqualToString:@"3"]){
        cell.status_label.text = [[SharedClass sharedInstance] languageSelectedString:@"Completed"];
        cell.makePayment_btn.hidden = YES;
    }
    cell.userName_label.text = [userNameArray objectAtIndex:indexPath.row];
//    cell.cost_label.text = [NSString stringWithFormat:@"%@%@",@": ",[costArray objectAtIndex:indexPath.row]];
    cell.service_label.text = [NSString stringWithFormat:@"%@",[serviceNameArray objectAtIndex:indexPath.row]];
    
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        cell.serviceNameTV.text = [NSString stringWithFormat:@"%@%@",@": ",[serviceNameArray objectAtIndex:indexPath.row]];
        cell.cost_label.text = [NSString stringWithFormat:@"%@%@",[costArray objectAtIndex:indexPath.row],@": "];
        cell.date_label.text = [NSString stringWithFormat:@"%@%@%@%@",[dateArray objectAtIndex:indexPath.row],@",",[timeArray objectAtIndex:indexPath.row],@": "];
        
    } else{
        
        cell.serviceNameTV.text = [NSString stringWithFormat:@"%@%@",@": ",[serviceNameArray objectAtIndex:indexPath.row]];
        cell.cost_label.text = [NSString stringWithFormat:@"%@%@",@": ",[costArray objectAtIndex:indexPath.row]];
        cell.date_label.text = [NSString stringWithFormat:@"%@%@%@%@",@": ",[dateArray objectAtIndex:indexPath.row],@",",[timeArray objectAtIndex:indexPath.row]];
    }
    
    //cell.serType_label.text = [NSString stringWithFormat:@"%@%@",@": ",[serviceNameArray objectAtIndex:indexPath.row]];
//    cell.date_label.text = [NSString stringWithFormat:@"%@%@%@%@",@": ",[dateArray objectAtIndex:indexPath.row],@",",[timeArray objectAtIndex:indexPath.row]];
    [cell.userImageview sd_setImageWithURL:[NSURL URLWithString:[imageArray objectAtIndex:indexPath.row]]];
    
    cell.userImageview.layer.cornerRadius = cell.userImageview.frame.size.width/2.0;
    cell.userImageview.clipsToBounds = YES;
    cell.ratingBtn.tag = indexPath.row;
    [cell.ratingBtn addTarget:self
                 action:@selector(yourButtonClicked:)
       forControlEvents:UIControlEventTouchUpInside];
    
    cell.makePayment_btn.layer.cornerRadius = 4;
    cell.makePayment_btn.layer.masksToBounds = YES;
    cell.makePayment_btn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    cell.makePayment_btn.layer.borderWidth = 1.0;
    cell.makePayment_btn.layer.cornerRadius =20;
    
    cell.viewDetails_btn.layer.cornerRadius = 4;
    cell.viewDetails_btn.layer.masksToBounds = YES;
    cell.viewDetails_btn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    cell.viewDetails_btn.layer.borderWidth = 1.0;
    cell.viewDetails_btn.layer.cornerRadius =20;
        
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 304;
    
}
-(void)yourButtonClicked:(UIButton*)sender
{
    NSLog(@"btn tag%ld",(long)sender.tag);
    if (sender.tag == 0)
    {
        
    }
}
-(void)gotoViewDetails
{
    DetailesCompleted_VC*completedetails = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailesCompleted_VC"];
    [self.navigationController pushViewController:completedetails animated:YES];
}
-(void)gotoViewDetails1
{
    DetailsVC*details = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsVC"];
    [self.navigationController pushViewController:details animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)single_btnAction:(id)sender {
    
    checkStr = @"single";
    [self.singleBtn setBackgroundColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]];
    [self.singleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.packageBtn setBackgroundColor:[UIColor whiteColor]];
    [self.packageBtn setTitleColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self ServiceCallForCompleteReq];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self ServiceCallForCompleteReq];
//        //[self.completeOrd_tableview reloadData];
//    });
    
}

- (IBAction)package_btnAction:(id)sender {
    
    checkStr = @"package";
    [self.packageBtn setBackgroundColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]];
    [self.packageBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.singleBtn setBackgroundColor:[UIColor whiteColor]];
    self.singleBtn.layer.borderColor =[[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1]CGColor];
    self.singleBtn.layer.borderWidth = 1.0;
    self.singleBtn.layer.cornerRadius =20;
    [self.singleBtn setTitleColor:[UIColor colorWithRed:82.0/255.0 green:180.0/255.0 blue:213.0/255.0 alpha:1] forState:UIControlStateNormal];
    
    [self ServiceCallForCompleteReq];
    
    //[self.completeOrd_tableview reloadData];
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//        //[self.completeOrd_tableview reloadData];
//    });
    
}

- (IBAction)ratingBtnAction:(id)sender {
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.completeOrd_tableview];
    NSIndexPath *indexPath = [self.completeOrd_tableview indexPathForRowAtPoint:btnPosition];
    
    NSLog(@"index %@",indexPath);
//    [[NSUserDefaults standardUserDefaults]objectForKey:@"nameStr"];
//    self.successMsg.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"msgStr"];
//    [self.profileImg sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"imageStr"]]];
    
    
    RatingView *payVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingView"];
    
    payVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [payVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
//    payVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:payVC animated:YES];
    
   // [self.navigationController presentViewController:payVC animated:YES completion:nil];
    [self presentViewController:payVC animated:YES completion:nil];
    // detailsVC.checkBtnString = @"yes";
    NSString *reqStr = [reqIdArray objectAtIndex:indexPath.row];
    NSString *sp_username = [userNameArray objectAtIndex:indexPath.row];
    NSString *spUserImage =[imageArray objectAtIndex:indexPath.row];
    [[NSUserDefaults standardUserDefaults]setObject:reqStr forKey:@"reqID"];
    [[NSUserDefaults standardUserDefaults]setObject:sp_username forKey:@"nameStr"];
    [[NSUserDefaults standardUserDefaults]setObject:spUserImage forKey:@"imageStr"];
    NSString *msgString =[[SharedClass sharedInstance]languageSelectedString:@"Service Successfully Completed"];
    [[NSUserDefaults standardUserDefaults]setObject:msgString forKey:@"msgStr"];
    //[[NSUserDefaults standardUserDefaults]setObject:@"PUSH" forKey:@"CHECK"];
     NSLog(@"req str %@%@%@",reqStr,sp_username,spUserImage);
    
    //[self.navigationController pushViewController:detailsVC animated:YES];
    
}

-(void)ServiceCallForCompleteReq{
    
    ////http://volive.in/aous/user_services/user_requests
    //lang,user_id, request_type
    
//    NSString *requestType;
//    if ([checkStr isEqualToString:@"package"]) {
//        requestType=@"2";
//    }else if([checkStr isEqualToString:@"single"]){
//        requestType=@"1";
//    }
    
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
    //[valuesDictionary setObject:@"154" forKey:@"user_id"];
    [valuesDictionary setObject:@"3" forKey:@"request_status"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"user_requests" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
         dataArray = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"data"]];
       
        userNameArray = [[NSMutableArray alloc]init];
        costArray = [[NSMutableArray alloc]init];
        serviceNameArray = [[NSMutableArray alloc]init];
        dateArray = [[NSMutableArray alloc]init];
        timeArray = [[NSMutableArray alloc]init];
        reqIdArray = [[NSMutableArray alloc]init];
        reqTypeArray = [[NSMutableArray alloc]init];
        reqStatusArray = [[NSMutableArray alloc]init];
        imageArray = [[NSMutableArray alloc]init];
        fileArray = [[NSMutableArray alloc]init];
        
        if([status isEqualToString:@"1"]){
            [SVProgressHUD dismiss];
//           dataArray = [[NSArray alloc]initWithArray:[jsonDict objectForKey:@"data"]];
            
            
//            if (dataArray.count == 0) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"No Data Found"] OnViewController:self completion:nil];
//                    [self.completeOrd_tableview reloadData];
//                });
//            } else{
            
                for (int i = 0; i<dataArray.count; i++) {
                    
                    [userNameArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"username"]];
                    [costArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"request_cost"]];
                    [dateArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"requested_date"]];
                    [timeArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"requested_time"]];
                    [serviceNameArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"service_type"]];
                    [reqIdArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"request_id"]];
                    [reqTypeArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"request_type"]];
                    [reqStatusArray addObject:[[dataArray objectAtIndex:i]objectForKey:@"request_status"]];
                    [imageArray addObject:[NSString stringWithFormat:@"%s%@",basePath,[[dataArray objectAtIndex:i]objectForKey:@"image"]]];
                    [fileArray addObject:[NSString stringWithFormat:@"%s%@",basePath,[[dataArray objectAtIndex:i]objectForKey:@"sp_file"]]];
                }
                
                NSLog(@"file arry %@",fileArray);
                    dispatch_async(dispatch_get_main_queue(), ^{
                    [self.completeOrd_tableview reloadData];
                        [SVProgressHUD dismiss];
                });
                
           // }
            
            }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.completeOrd_tableview reloadData];
                    [SVProgressHUD dismiss];
                });
            });
        }
        
    }];
}



- (IBAction)makePayment_btnAction:(id)sender {
    
    
    
}
     

- (IBAction)viewDetails_btnAction:(id)sender {
    
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.completeOrd_tableview];
    NSIndexPath *indexPath = [self.completeOrd_tableview indexPathForRowAtPoint:btnPosition];
    
    if ([checkStr isEqualToString:@"package"]) {
        Packages *packages = [self.storyboard instantiateViewControllerWithIdentifier:@"Packages"];
        packages.reqIdStr = [reqIdArray objectAtIndex:indexPath.row];
        packages.reqTypeStr = [reqTypeArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:packages animated:YES];
    }
    else{
        
        OrderDetailsVC *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailsVC"];
       // detailsVC.checkBtnString = @"yes";
        detailsVC.reqIdStr = [reqIdArray objectAtIndex:indexPath.row];
        detailsVC.reqTypeStr = [reqTypeArray objectAtIndex:indexPath.row];
        detailsVC.fileStr = [fileArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:detailsVC animated:YES];
    }
    
}
@end
