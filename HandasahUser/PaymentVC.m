//
//  PaymentVC.m
//  HandasahUser
//
//  Created by Suman Guntuka on 15/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "PaymentVC.h"
#import "SuccessfulVC.h"
#import "DetailsVC.h"
#import "OrderDetailsVC.h"

@interface PaymentVC (){
    BOOL isChecked;
 
}

@end

@implementation PaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    self.costLabel.text = [NSString stringWithFormat:@"%@%@",@"Payment ",[[NSUserDefaults standardUserDefaults]objectForKey:@"cost"]];
    
    
    self.yourPayableStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Your Payable Amount"];
    self.cardNameLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Name On Card"];
    self.cardNumberLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Card Number"];
    self.cardExpiryStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Card Expiry"];
    self.cvvStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"CVV"];
     self.secureLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Securely Save my card for Future Use"];
    
    isChecked = false;
        
    [self.payNowBtn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"PAY NOW"] forState:UIControlStateNormal];
    // Do any additional setup after loading the view.
    
}

-(void)dismissKeyboard{
    [self.promocode_TF resignFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.hidden = YES;

}
-(void)viewWillDisappear:(BOOL)animated
{
     self.tabBarController.tabBar.hidden = NO;
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_Payment:(id)sender {
    
    [self ServiceCallForPayment];
    
}

- (IBAction)btn_Close:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)ServiceCallForPayment{
    //http://volive.in/engineering/user_services/payment
    //lang , request_id
 
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
   
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"reqID"] forKey:@"request_id"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"payment" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
 
                SuccessfulVC *gotoVC =[self.storyboard instantiateViewControllerWithIdentifier:@"SuccessfulVC"];
                [self.navigationController pushViewController:gotoVC animated:YES];
              
            });
            
        }
        
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}



- (IBAction)promocodeBtnAction:(id)sender {
    
//    if (isChecked == true) {
//        //[self registerServiceCalling];
//    }
//    else{
//        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Accept terms and conditions to Sign Up"] OnViewController:self completion:nil];
//    }
    
}

/*
 
 isChecked = !isChecked;
 
 if (isChecked == true) {
 
 [_checkBoxBtn_Outlet setImage:[UIImage imageNamed:@"check status"] forState:UIControlStateNormal];
 
 } else {
 
 [_checkBoxBtn_Outlet setImage:[UIImage imageNamed:@"check statuss"] forState:UIControlStateNormal];
 
 }
 https://i.diawi.com/xSh41y user ipa
 */

@end
