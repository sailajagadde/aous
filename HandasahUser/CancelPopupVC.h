//
//  CancelPopupVC.h
//  HandasahUser
//
//  Created by MuraliKrishna on 23/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelPopupVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *cancelStatic;

@property (weak, nonatomic) IBOutlet UITextView *reasonForCancelTV;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
//- (IBAction)closeBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
//- (IBAction)selectBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *reasonForStaticLabel;
- (IBAction)selectBtnAction:(id)sender;
- (IBAction)closeBtnAction:(id)sender;

@end
