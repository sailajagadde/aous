//
//  HomeViewController.m
//  HandasahUser
//
//  Created by Apple on 5/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeCollectionViewCell.h"
#import "RESideMenu.h"
#import "FirstVisitViewController.h"
#import "ProceedPackagesVC.h"

//#import "SurveyingList.h"
#import "Notifications.h"
#import "VisitFirstVC.h"

#import "SingleDetailsVC.h"
#import "AppDelegate.h"

@interface HomeViewController ()<RESideMenuDelegate >{
    
    NSMutableArray *serviceNamesArray;
    NSMutableArray *serviceImagesArray;
    NSMutableArray *serviceIdArray;
    NSMutableArray *serviceCostArray;
    NSMutableArray *serviceDescArray;
    NSMutableArray *subserviceFlagArray;
    TabBar * tab ;
     AppDelegate *appDelegate;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"HOME"];
    self.listLbl.text = [[SharedClass sharedInstance]languageSelectedString:@"List of Services"];
    self.searchTF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Search for a service"];
    
    
    
    _menuBtn.target =self;
    _menuBtn.action = @selector(presentLeftMenuViewController:);
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Home"]];
        
        [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"My Requests"]];
        
        [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Completed Orders"]];
        
        [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Account"]];
    });
    [self homeService];
    
    
    
    // Do any additional setup after loading the view.
}



-(void)viewWillAppear:(BOOL)animated
{
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"packageID"] != nil) {
        
        NSString * str = [[NSUserDefaults standardUserDefaults]objectForKey:@"packageID"];
        NSLog(@"package id  %@",str);
        self.packIDStr = str;
        
    }
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return serviceNamesArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"HomeCell" forIndexPath:indexPath];
    
    cell.lisLbl.text = [serviceNamesArray objectAtIndex:indexPath.row];
    [cell.listImg sd_setImageWithURL:[NSURL URLWithString:[serviceImagesArray objectAtIndex:indexPath.row]]];
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * packageStr = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"typeStr"]];
    NSLog(@"string %@",packageStr);
    
    [[NSUserDefaults standardUserDefaults]setObject:[serviceNamesArray objectAtIndex:indexPath.row] forKey:@"sername"];
    [[NSUserDefaults standardUserDefaults]setObject:[serviceCostArray objectAtIndex:indexPath.row] forKey:@"sercost"];
    [[NSUserDefaults standardUserDefaults]setObject:[serviceDescArray objectAtIndex:indexPath.row] forKey:@"serdescription"];
    [[NSUserDefaults standardUserDefaults]setObject:[serviceImagesArray objectAtIndex:indexPath.row] forKey:@"serimage"];
    [[NSUserDefaults standardUserDefaults]setObject:[serviceIdArray objectAtIndex:indexPath.row] forKey:@"serID"];
    //[NSString stringWithFormat:@"%@"
    [[NSUserDefaults standardUserDefaults]setObject:[subserviceFlagArray objectAtIndex:indexPath.row] forKey:@"subSerFlag"];
    NSString *newCostStr = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"subSerFlag"]];
    
    SingleDetailsVC *singleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleDetailsVC1"];
    singleVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [singleVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:singleVC animated:YES completion:nil];
    
   /* if ([packageStr isEqualToString:@"2"]) {
     
        ProceedPackagesVC * proceed = [self.storyboard instantiateViewControllerWithIdentifier:@"ProceedPackagesVC"];
        proceed.serIDStr = [NSString stringWithFormat:@"%@",[serviceIdArray objectAtIndex:indexPath.row]];
        proceed.packageIDStr = self.packIDStr;
        [self.navigationController pushViewController:proceed animated:YES];
     
    }else if([packageStr isEqualToString:@"1"])
    {
        
        [[NSUserDefaults standardUserDefaults]setObject:[serviceNamesArray objectAtIndex:indexPath.row] forKey:@"sername"];
        [[NSUserDefaults standardUserDefaults]setObject:[serviceCostArray objectAtIndex:indexPath.row] forKey:@"sercost"];
        [[NSUserDefaults standardUserDefaults]setObject:[serviceDescArray objectAtIndex:indexPath.row] forKey:@"serdescription"];
        [[NSUserDefaults standardUserDefaults]setObject:[serviceImagesArray objectAtIndex:indexPath.row] forKey:@"serimage"];
        [[NSUserDefaults standardUserDefaults]setObject:[serviceIdArray objectAtIndex:indexPath.row] forKey:@"serID"];
        
        SingleDetailsVC *singleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleDetailsVC1"];
        singleVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [singleVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:singleVC animated:YES completion:nil];
        
//        VisitFirstVC * forVisit = [self.storyboard instantiateViewControllerWithIdentifier:@"VisitFirstVC"];
//        forVisit.serIDStr = [NSString stringWithFormat:@"%@",[serviceIdArray objectAtIndex:indexPath.row]];
//        [self.navigationController pushViewController:forVisit animated:YES];
        
//        FirstVisitViewController * first = [self.storyboard instantiateViewControllerWithIdentifier:@"FirstVisitViewController12"];
//        first.descStr = [NSString stringWithFormat:@"%@",[serviceDescArray objectAtIndex:indexPath.row]];
//        first.serIDStr = [NSString stringWithFormat:@"%@",[serviceIdArray objectAtIndex:indexPath.row]];
//        [self.navigationController pushViewController:first animated:YES];
    }*/

}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat padding =0;
    CGFloat cellSize = self.homeCollectionObj.frame.size.width - padding;
    return CGSizeMake(cellSize/3.0 , 122);
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)homeAction:(id)sender {
    
    Notifications *noti = [self.storyboard instantiateViewControllerWithIdentifier:@"Notification"];
    
    noti.strCheck =@"Yes";
    
    [self.navigationController pushViewController:noti animated:YES];
}

//Service call for home

-(void)homeService{
    
        //http://volive.in/aous/user_services/home
    
        NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
        NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
        if ([str isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
        }
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
     [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
        [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"home" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
   
            NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
            NSLog(@"json data %@",jsonDict);
    
            NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
    
            NSString *emailFlagStr = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"email_flag"]];
            
            if([status isEqualToString:@"1"]){
    
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"checking"] isEqualToString:@"show"]) {

                    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"email"] isEqualToString:@""]) {
                        UIAlertController * alert = [UIAlertController
                                                     alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
                                                     message:[[SharedClass sharedInstance]languageSelectedString:@"Please Update Email In Profile"]
                                                     preferredStyle:UIAlertControllerStyleAlert];

                        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            //[self loginServiceCall];
                            //[[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"checking"];
                        }];

                        [alert addAction:yesButton];

                        [self presentViewController:alert animated:YES completion:nil];
                        //}

                    }
                }
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"checking"];
                
                NSArray * servicesArray = [[NSArray alloc]initWithArray:[[jsonDict objectForKey:@"data"]objectForKey:@"services"]];
    
                serviceNamesArray = [[NSMutableArray alloc]init];
                serviceImagesArray = [[NSMutableArray alloc]init];
                serviceIdArray = [[NSMutableArray alloc]init];
                serviceCostArray = [[NSMutableArray alloc]init];
                serviceDescArray = [[NSMutableArray alloc]init];
                subserviceFlagArray = [[NSMutableArray alloc]init];
                
                for (int i = 0; i<servicesArray.count; i++) {
        
                    [serviceNamesArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"service_type"]];
                    [serviceImagesArray addObject:[NSString stringWithFormat:@"%s%@",basePath,[[servicesArray objectAtIndex:i]objectForKey:@"icon"]]];
                    [serviceIdArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"service_id"]];
                    [serviceCostArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"service_cost"]];
                    [serviceDescArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"description"]];
                    [subserviceFlagArray addObject:[[servicesArray objectAtIndex:i]objectForKey:@"sub_services_flag"]];
                    
                }
    
                
                dispatch_async(dispatch_get_main_queue(), ^{
    
                    [self.homeCollectionObj reloadData];
                    [SVProgressHUD dismiss];
                    
                    
                 /*   if ([emailFlagStr intValue]==0) {
                        
                        
                        UIAlertController * alert = [UIAlertController
                                                     alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"]
                                                     message:[[SharedClass sharedInstance]languageSelectedString:@"You must update profile to send request"]
                                                     preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            
                            
                            tab = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                            appDelegate.selectTabItem=@"3";
                            [self.sideMenuViewController setContentViewController:tab];
                            [self.sideMenuViewController hideMenuViewController];
                            
                            
                            
                            
                        }];
                        
                        [alert addAction:yesButton];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                        
                     
                        
                 
                    }
                    else{
                        
                    }*/
                });
    
                
                
            }
    
            else{
    
                dispatch_async(dispatch_get_main_queue(), ^{
    
                    [SVProgressHUD dismiss];
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                });
    
            }
    
        }];
    
    }





@end
