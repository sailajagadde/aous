//
//  SuccessfulVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuccessfulVC : UIViewController
- (IBAction)btn_Back:(id)sender;
- (IBAction)btn_TrackOrder:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *successLabel;

@property (weak, nonatomic) IBOutlet UILabel *thankLabel;
@property (weak, nonatomic) IBOutlet UILabel *yourPayLabel;
@property (weak, nonatomic) IBOutlet UILabel *pleaseCheckLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *transNoStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderTotalStaticLabel;


@property (weak, nonatomic) IBOutlet UIButton *trackOrderBtn;
@end
