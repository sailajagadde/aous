//
//  ProceedPackagesVC.m
//  HandasahUser
//
//  Created by Apple on 5/21/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "ProceedPackagesVC.h"
#import "AdressViewController.h"
#import "PackCell.h"


@interface ProceedPackagesVC (){
    
    PackCell *cell;
    UIDatePicker *datePicker;
    UIToolbar *toolbar;
    
    NSString *dateFormatString;
    NSString *todayDate;
    
    
    
    UITextField * current_TF;
    NSMutableArray *visitNoArray;
    NSString * tagValue;
    NSMutableArray*checkdateArr;
    NSMutableArray*checkTimeArr;
    
    NSMutableArray*visitDateArr;
    NSMutableArray*visitTimeArr;
    
    NSMutableArray *descArray;
    
}

@end

@implementation ProceedPackagesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Packages"];
    [self.submit_btn setTitle:[[SharedClass sharedInstance] languageSelectedString:@"SUBMIT"]
                     forState:UIControlStateNormal];
    _staticDescLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Description"];
    _pleaseEnterDescLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Please Enter Description"];
    
    
    self.description_TV.layer.borderColor = [UIColor grayColor].CGColor;
    self.description_TV.layer.borderWidth = 1.0;
    self.description_TV.layer.cornerRadius = 2;
    
    UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_wh"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    back.tintColor = [UIColor whiteColor];
    [self.navigationItem setLeftBarButtonItem:back];
    
    [self dateAndTime];
    [self ServiceCallForVisits];
    
    // Do any additional setup after loading the view.
}

-(void)dismissKeyboard{
    
    [self.description_TV resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    self.pleaseEnterDescLabel.hidden = YES;
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return visitNoArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    cell = [self.packageTV dequeueReusableCellWithIdentifier:@"PackCell" forIndexPath:indexPath];
   
    cell.visitLbl.text = [visitNoArray objectAtIndex:indexPath.row];
    cell.dateTF.tag = indexPath.row;
    cell.timeTF.tag = indexPath.row;
    cell.dateTF.text=visitDateArr[indexPath.row];
    cell.timeTF.text=visitTimeArr[indexPath.row];
    
    cell.dateLbl.text = [[SharedClass sharedInstance]languageSelectedString:@"Date"];
    cell.timeLbl.text = [[SharedClass sharedInstance]languageSelectedString:@"Time"];
    
    tagValue = [NSString stringWithFormat:@"%ld",(long)cell.dateTF.tag];
    NSLog(@"tag value %@",tagValue);
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 132;
    
}

-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}



- (IBAction)submitAction:(id)sender {
    
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.packageTV];
    NSIndexPath *indexPath = [self.packageTV indexPathForRowAtPoint:btnPosition];
    
    
    if ([checkTimeArr containsObject:@"NO"] || [checkdateArr containsObject:@"NO"]) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Enter All Fields"] OnViewController:self completion:nil];
    }
    else{
         NSMutableArray*visitingArr=[NSMutableArray new];
       
    
    for (int i=0; i<visitNoArray.count; i++) {
        
        
         NSMutableDictionary*visatDict=[NSMutableDictionary new];
        
       // "visits":[{"visitingdate":"17-7-2018","visitingtime":"3:20"}
        
        [visatDict setObject:visitDateArr[i] forKey:@"visitingdate"];
         [visatDict setObject:visitTimeArr[i] forKey:@"visitingtime"];
       
        [visitingArr addObject:visatDict];
        
    }
        
        NSLog(@"vis arr %@",visitingArr);
    
    AdressViewController * adress = [self.storyboard instantiateViewControllerWithIdentifier:@"AdressViewController"];
    adress.packageFinalArr = visitingArr;
    adress.packIdStr = self.packageIDStr;
    adress.serIdStr = self.serIDStr;
    adress.descStr = _description_TV.text;
    [self.navigationController pushViewController:adress animated:YES];
    }
}


-(void)ServiceCallForVisits{
    
    //http://volive.in/engineering/user_services/visiting_screen
    //service_id,package_id,request_type,lang,API-KEY
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    [[SharedClass sharedInstance]showProgressFor:[[SharedClass sharedInstance]languageSelectedString:@"Please Wait"]];
    
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
    } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    [valuesDictionary setObject:self.serIDStr forKey:@"service_id"];
    [valuesDictionary setObject:self.packageIDStr forKey:@"package_id"];
    [valuesDictionary setObject:@"2" forKey:@"request_type"];
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"visiting_screen" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            
            NSArray * visitsArray = [[NSArray alloc]initWithArray:[[jsonDict objectForKey:@"data"]objectForKey:@"visits"]];
            NSArray * serDetailsArray = [[NSArray alloc]initWithArray:[[jsonDict objectForKey:@"data"]objectForKey:@"service_details"]];
            
            
            visitNoArray = [[NSMutableArray alloc]init];
            
            checkTimeArr = [[NSMutableArray alloc]init];
            checkdateArr = [[NSMutableArray alloc]init];
            
            visitDateArr = [[NSMutableArray alloc]init];
            visitTimeArr = [[NSMutableArray alloc]init];
            
            descArray = [[NSMutableArray alloc]init];
            
            for (int i = 0; i<visitsArray.count; i++) {
                
                //NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
                
                [visitNoArray addObject:[[visitsArray objectAtIndex:i]objectForKey:@"visit_no"]];
                [checkdateArr addObject:@"NO"];
                [checkTimeArr addObject:@"NO"];
                
                [visitDateArr addObject:@""];
                [visitTimeArr addObject:@""];
                
            }
            
            for (int i = 0; i<serDetailsArray.count; i++) {
                
                //NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
                
                [descArray addObject:[[serDetailsArray objectAtIndex:i]objectForKey:@"description"]];
                
                
            }
            
            
            NSLog(@"check array %@",visitNoArray);
            NSLog(@"des array %@",descArray);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [self.packageTV reloadData];
                [SVProgressHUD dismiss];
            });
            
        }
        
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                //[[SharedClass sharedInstance]showProgressFor:[jsonDict objectForKey:@"message"]];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
            });
            
        }
        
    }];
    
}


-(void)dateAndTime{
    
    datePicker = [[UIDatePicker alloc]init];
    
    toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, 320, 44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    

    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Done"] style: UIBarButtonItemStyleDone target: self action: @selector(doneBtn:)];
    [doneButton setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"] style: UIBarButtonItemStyleDone target: self action: @selector(cancelBtnClicked)];
    [cancelButton setTintColor:[UIColor whiteColor]];
    
    toolbar.items = [NSArray arrayWithObjects:cancelButton,flexibleSpace,doneButton, nil];
    
}

-(void)doneBtn:(UIDatePicker*)datepicker{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormatString];
    
    if(current_TF == cell.dateTF){
        
        //cell.dateTF.text = [dateFormatter stringFromDate:datePicker.date];
        NSLog(@"date picker tag is %d",datePicker.tag);
        [visitDateArr replaceObjectAtIndex:datePicker.tag withObject:[dateFormatter stringFromDate:datePicker.date]];
        [checkdateArr replaceObjectAtIndex:datePicker.tag withObject:@"YES"];
        
    } else if (current_TF == cell.timeTF){
        
        NSString *stringForTime = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:datePicker.date]];
        if([stringForTime isEqualToString:@"00:00"]){
            
        //    cell.timeTF.text = @"12:00";
            [visitTimeArr replaceObjectAtIndex:datePicker.tag withObject:@"12:00"];
        }
        else{
            //cell.timeTF.text = [dateFormatter stringFromDate:datePicker.date];
             [visitTimeArr replaceObjectAtIndex:datePicker.tag withObject:[dateFormatter stringFromDate:datePicker.date]];
        }
         [checkTimeArr replaceObjectAtIndex:datePicker.tag withObject:@"YES"];
        NSLog(@"date picker tag is %d",datePicker.tag);
    }
    NSLog(@"visit dates arr %@",visitDateArr);
     NSLog(@"visit times arr %@",visitTimeArr);
    NSLog(@"chekc dates arr %@",checkdateArr);
    NSLog(@"chekc times arr %@",checkTimeArr);
    
    [cell.dateTF resignFirstResponder];
    [cell.timeTF resignFirstResponder];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.packageTV reloadData];
    });
    
}

-(void)cancelBtnClicked{
    [cell.dateTF resignFirstResponder];
    [cell.timeTF resignFirstResponder];
}

-(void)dismissKeyboardMethod{
    
    [cell.dateTF resignFirstResponder];
    [cell.timeTF resignFirstResponder];
    
}
//Textfield delegates
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//
//    datePicker.datePickerMode = UIDatePickerModeDate;
//
//            NSDate *date = [NSDate date];
//            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierIndian];
//            NSDateComponents *components = [[NSDateComponents alloc] init];
//            components.day = 0;
//            NSDate *newDate = [calendar dateByAddingComponents:components toDate:date options:0];
//
//            datePicker.minimumDate = newDate;
//            dateFormatString = @"YYYY-MM-dd";
//            textField.inputView = datePicker;
//            [textField setInputAccessoryView:toolbar];
//
//    textField.inputView = datePicker;
//    [self dateAndTime];
//    return YES;
//}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    cell = [[PackCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PackCell"];

    CGPoint position = [textField convertPoint:CGPointZero toView:self.packageTV];
    NSIndexPath *indexPath = [self.packageTV indexPathForRowAtPoint:position];
    cell = (PackCell*)[self.packageTV cellForRowAtIndexPath:indexPath];
    
    if(textField == cell.dateTF){
        
        datePicker.datePickerMode = UIDatePickerModeDate;
        
        NSDate *date = [NSDate date];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierIndian];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        components.day = 0;
        NSDate *newDate = [calendar dateByAddingComponents:components toDate:date options:0];
        
        datePicker.minimumDate = newDate;
        datePicker.tag=textField.tag;
        dateFormatString = @"YYYY-MM-dd";
        textField.inputView = datePicker;
        [textField setInputAccessoryView:toolbar];
        
        //self.scroll_view.scrollEnabled = YES;
        
    } else if(textField == cell.timeTF){
        
        datePicker.datePickerMode = UIDatePickerModeTime;
        dateFormatString = @"hh:mm a";
        datePicker.tag=textField.tag;
        textField.inputView = datePicker;
        [textField setInputAccessoryView:toolbar];
        
        //self.scroll_view.scrollEnabled = YES;
        
    }
    current_TF = textField;
    
}

/*-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    cell = [[PackCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PackCell"];
    if(textField == cell.dateTF){

        datePicker.datePickerMode = UIDatePickerModeDate;

        NSDate *date = [NSDate date];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierIndian];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        components.day = 0;
        NSDate *newDate = [calendar dateByAddingComponents:components toDate:date options:0];

        datePicker.minimumDate = newDate;
        dateFormatString = @"YYYY-MM-dd";
        textField.inputView = datePicker;
        [textField setInputAccessoryView:toolbar];

        //self.scroll_view.scrollEnabled = YES;

    } else if(textField == cell.timeTF){

        datePicker.datePickerMode = UIDatePickerModeTime;
        dateFormatString = @"hh:mm a";
        textField.inputView = datePicker;
        [textField setInputAccessoryView:toolbar];

        //self.scroll_view.scrollEnabled = YES;

    }
    current_TF = textField;
    return YES;
}
*/
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if(textField == cell.dateTF){
        
        //self.scroll_view.scrollEnabled = NO;
        
    }
    else if (textField == cell.timeTF){
        
        //self.scroll_view.scrollEnabled = NO;
        
    }
    
    //[self.scroll_view setContentOffset:CGPointMake(0, -self.scroll_view.contentInset.top) animated:YES];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    BOOL returnValue = NO;
    
    if (textField == cell.dateTF) {
        
        [cell.dateTF becomeFirstResponder];
        
        returnValue = YES;
        
    }
    else if (textField == cell.dateTF) {
        
        [cell.timeTF becomeFirstResponder];
        //[self.scroll_view setContentOffset:CGPointMake(0, textField.frame.origin.y+140) animated:YES];
        //[self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];
        
        returnValue = YES;
    }
    
    //[self.scroll_view setContentOffset:CGPointMake(0, -self.scroll_view.contentInset.top) animated:YES];
    return returnValue;
}


//user ipa https://i.diawi.com/4ia58g
//sp ipa https://i.diawi.com/zRE74z

@end
