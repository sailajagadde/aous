//
//  ChoosePackagesVC.h
//  HandasahUser
//
//  Created by MuraliKrishna on 12/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePackagesVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *name_label;

@property (weak, nonatomic) IBOutlet UIButton *select_btn;

- (IBAction)selectBtn_action:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *choosePackage_Tableview;
- (IBAction)close_btnAction:(id)sender;

@end
