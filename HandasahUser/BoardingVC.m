//
//  BoardingVC.m
//  HandasahUser
//
//  Created by Suman Guntuka on 05/11/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "BoardingVC.h"
#import "OnBoardingCell.h"
#import "AppDelegate.h"
#import "LogInVC.h"

@interface BoardingVC (){
    
    
    OnBoardingCell *cell;
    NSMutableArray *namesArray;
    NSMutableArray *imagesArray;
    
    
    NSMutableArray *descArray;
    int indexVisibleCell;
    CGFloat width;
    
    AppDelegate *appDel;
}

@end

@implementation BoardingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadAttributes];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    // Do any additional setup after loading the view.
}

-(void)loadAttributes{
    
    namesArray = [[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedString:@"HIRE PERSONAL SECRETARY"],[[SharedClass sharedInstance]languageSelectedString:@"CHOOSE A PACKAGE"],[[SharedClass sharedInstance]languageSelectedString:@"SEND VOICE MESSAGE"],[[SharedClass sharedInstance]languageSelectedString:@"BEYOND SERVICES"], nil];
    imagesArray = [[NSMutableArray alloc]initWithObjects:@"Group 654",@"Group 655",@"Group 656",@"Group 657", nil];
    
    descArray = [[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedString:@"Select Any Service Which We Provide in the Technique App"],[[SharedClass sharedInstance]languageSelectedString:@"Select Loaction,Date And Time Then Select Sub-Services"],[[SharedClass sharedInstance]languageSelectedString:@"Upload Your Problem through Image or Video"],[[SharedClass sharedInstance]languageSelectedString:@"Service Completed From Our Service Provider"], nil];
    
    NSString * selectedLanguage = [[NSUserDefaults standardUserDefaults]valueForKey:@"currentLanguage"];
    
    if([selectedLanguage isEqualToString:@"1"]){
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceLeftToRight;
        UITextView.appearance.semanticContentAttribute= UISemanticContentAttributeForceLeftToRight;
    }
    else if ([selectedLanguage isEqualToString:@"2"]){
        UIView.appearance.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
        UITextView.appearance.semanticContentAttribute= UISemanticContentAttributeForceRightToLeft;
    }
    [self setlanguage];
//    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
//
//    if ([str isEqualToString:@"2"]) {
//        langStr = @"ar";
//
//    } else if([str isEqualToString:@"1"]){
//
//        langStr = @"en";
//
//    }
    
}
-(void)setlanguage{
    [self.skip_btn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"SKIP"] forState:UIControlStateNormal];
    [self.next_btn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"NEXT"] forState:UIControlStateNormal];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return descArray.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    cell = [self.collection_view dequeueReusableCellWithReuseIdentifier:@"OnBoardingCell" forIndexPath:indexPath];
    //cell.name_label.text = [namesArray objectAtIndex:indexPath.row];
    cell.description_label.text = [descArray objectAtIndex:indexPath.row];
    
    //[[SharedClass sharedInstance]languageSelectedString:@"Lorem ipsum dolor sit er elit  adipisicing pecu, sed do tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim"];
    cell.icon_ImageView.image = [UIImage imageNamed:[imagesArray objectAtIndex:indexPath.row]];
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    indexVisibleCell = [indexPath indexAtPosition:[indexPath length] - 1];
    NSLog(@"visible cell %d",indexVisibleCell);
    self.page_Control.currentPage = indexPath.row;
    
}

- (CGSize)collectionView:(UICollectionView* )collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath* )indexPath {
    
    //  width= (CGFloat) (self.collection_view.frame.size.width);
    //  hieght= (CGFloat) (self.collection_view.frame.size.width);
    
    
    return CGSizeMake(self.collection_view.frame.size.width,self.collection_view.frame.size.height);
    
    
    
}


- (IBAction)nextAction:(id)sender {
    
    NSUInteger indexOfScrolledCell;
    indexOfScrolledCell = indexVisibleCell;
    
    if (indexVisibleCell < 3) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:indexOfScrolledCell+1 inSection:0];
        [self.collection_view scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
        self.page_Control.currentPage = path.row;
    }
    
    else{
        
        LogInVC *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInVC"];
        [self.navigationController pushViewController:loginVC animated:YES];
    }
    
}

- (IBAction)skipAction:(id)sender {
    LogInVC *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInVC"];
    [self.navigationController pushViewController:loginVC animated:YES];
}
@end
