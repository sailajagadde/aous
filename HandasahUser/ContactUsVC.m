//
//  ContactUsVC.m
//  HandasahUser
//
//  Created by MuraliKrishna on 12/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "ContactUsVC.h"

@interface ContactUsVC ()

@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Contact Us"];
    
    self.mobileNumberStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Mobile Number"];
    self.emailIDStaticLabel.text = [[SharedClass sharedInstance]languageSelectedString:@"Email Id"];
    self.addressStaticalbel.text = [[SharedClass sharedInstance]languageSelectedString:@"Address"];
    self.mobileNumber_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Mobile Number"];
    self.emailID_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Email Id"];
    self.address_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Address"];
    
    
    [self SetTextFieldBorder:self.emailID_TF];
    [self SetTextFieldBorder:self.mobileNumber_TF];
    [self SetTextFieldBorder:self.address_TF];
    
    [self ServiceCallForContactUs];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)SetTextFieldBorder :(UITextField *)textField{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.8;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
}

- (IBAction)back_action:(id)sender {
    
    self.back_btn.target =self;
    self.back_btn.action = @selector(presentLeftMenuViewController:);
    
}


-(void)ServiceCallForContactUs{
    //http://volive.in/engineering/user_services/contact_us
    //lang
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"contact_us" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                self.mobileNumber_TF.text = [[[jsonDict objectForKey:@"data"]objectForKey:@"contact_details"]objectForKey:@"phone_number"];
                self.emailID_TF.text = [[[jsonDict objectForKey:@"data"]objectForKey:@"contact_details"]objectForKey:@"email"];
                self.address_TF.text = [[[jsonDict objectForKey:@"data"]objectForKey:@"contact_details"]objectForKey:@"address"];
            });
            
        }
        
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:@"Alert" Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}

@end
