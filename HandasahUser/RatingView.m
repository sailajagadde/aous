//
//  RatingView.m
//  HandasahUser
//
//  Created by MuraliKrishna on 09/08/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "RatingView.h"
#import "RESideMenu.h"
#import "HomeViewController.h"

@interface RatingView ()<RESideMenuDelegate>
{
    RESideMenu * sideMenu;
    NSString*ratingStr;
}

@end

@implementation RatingView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.titleLbl.text =[[SharedClass sharedInstance]languageSelectedString:@"Review"];
   
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Review"];
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    [self.submit setTitle:[[SharedClass sharedInstance]languageSelectedString:@"SUBMIT"] forState:UIControlStateNormal];
    
    self.writeReview.text = [[SharedClass sharedInstance]languageSelectedString:@"Write Review"];
//
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"nameStr"]!=nil){
        self.nameLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"nameStr"];
        self.successMsg.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"msgStr"];
        [self.profileImg sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"imageStr"]]placeholderImage:[UIImage imageNamed:@"profile"]];
    }
    else{
        //self.successMsg.text = @"";
    }
    
//    self.nameLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"nameStr"];
//    self.successMsg.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"msgStr"];
//    [self.profileImg sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"imageStr"]]placeholderImage:[UIImage imageNamed:@"profile"]];
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
    
    
//    if([[NSUserDefaults standardUserDefaults]objectForKey:@"nameStr"]!=nil){
//        self.nameLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"nameStr"];
//        self.successMsg.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"msgStr"];
//        [self.profileImg sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"imageStr"]]placeholderImage:[UIImage imageNamed:@"profile"]];
//    }
//    else{
//        self.successMsg.text = @"";
//    }
}
- (void)viewWillDisappear:(BOOL)animated{
     self.navigationController.navigationBar.hidden = NO;
}
-(void)dismissKeyboard{
    [self.reviewTV resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)ServiceCallForRating{
    
    //http://volive.in/engineering/user_services/request_review
    //lang, request_id, rating, review
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"reqID"] forKey:@"request_id"];
    [valuesDictionary setObject:ratingStr forKey:@"rating"];
    [valuesDictionary setObject:self.reviewTV.text forKey:@"review"];
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"request_review" Dictionary:valuesDictionary onViewController:self :^(NSData* data) {
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary *)data];
        NSLog(@"json data %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];

                HomeViewController * home=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                TabBar* tab=[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                
                RESideMenu *sideMenuViewController;
                
                sideMenu=[self.storyboard instantiateViewControllerWithIdentifier:@"SlideMenu"];
                
                HomeViewController *menu =[self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                sideMenuViewController = [[RESideMenu alloc] initWithContentViewController: menu                                             leftMenuViewController:sideMenu  rightMenuViewController:nil];
                sideMenuViewController.delegate = self;
                
                [[UIApplication sharedApplication] delegate].window.rootViewController = sideMenuViewController;
                
                [self.navigationController pushViewController:tab animated:YES];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                
            });
             [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
            
        }
        
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:nil];
                [SVProgressHUD dismiss];
            });
        }
        
    }];
}

-(void)viewDidDisappear:(BOOL)animated{
    //[[NSUserDefaults standardUserDefaults]removeObjectForKey:@"CHECK"];
}

- (IBAction)backBtn:(id)sender {
     // [[NSUserDefaults standardUserDefaults]setObject:@"PUSH" forKey:@"CHECK"];
//    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"CHECK"]  isEqual: @"PUSH"]){
//        [self.navigationController popViewControllerAnimated:YES];
//    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
   // }
}

- (IBAction)submitBtn:(id)sender {
   // [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"CHECK"];
    [self ServiceCallForRating];
}
- (IBAction)onClickRatingView:(id)sender {
    
   ratingStr = [NSString stringWithFormat:@"%0.00f",self.rateview.value];
    
    
}
@end
