//
//  AccountVC.m
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import "AccountVC.h"
#import "RESideMenu.h"

@interface AccountVC ()<RESideMenuDelegate>{
    
    UIImagePickerController * picker_Profile_Pic;
    UIImage*profile_pic;
    UITextField *nameField,*passwordField;
    
}


@end

@implementation AccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.menuBtn.target =self;
    if ([_textFromSendRequest  isEqual: @"Email"] ){
        UIBarButtonItem * back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_wh"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
        back.tintColor = [UIColor whiteColor];
        [self.navigationItem setLeftBarButtonItem:back];
    }else{
    self.menuBtn.action = @selector(presentLeftMenuViewController:);
    [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Home"]];
    
    [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"My Requests"]];
    
    [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Completed Orders"]];
    
    [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Account"]];
    }
    [self loadAttributes];
    // Do any additional setup after loading the view.
}
-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)loadAttributes{
    
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:gesture];
    
    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedString:@"Account"];
    self.save_btn.title = [[SharedClass sharedInstance]languageSelectedString:@"Save"];
    

    self.name_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Name"];
      self.email_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Email"];
      self.mobileNumber_label.text = [[SharedClass sharedInstance]languageSelectedString:@"Mobile Number"];
    
    self.name_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Name"];
    self.email_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Email"];
    self.mobileNumber_TF.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Mobile Number"];

    [self.changePwd_btn setTitle:[[SharedClass sharedInstance]languageSelectedString:@"Change Password"] forState:UIControlStateNormal];
    
    self.pickin_img=[[UIImageView alloc]init];
    
    self.name_TF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"username"];
    self.email_TF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"] ;
    self.mobileNumber_TF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"phone_number"];
    
    [self.profile_Image sd_setImageWithURL:[[NSUserDefaults standardUserDefaults] objectForKey:@"image"] placeholderImage:[UIImage imageNamed:@"profile"]];
    //[self.profile_Image sd_setImageWithURL:[[NSUserDefaults standardUserDefaults] objectForKey:@"image"] placeholderImage:[UIImage imageNamed:@"User Profilepic"]];
    
}
-(void)dismissKeyboard{
    
    [self.name_TF resignFirstResponder];
    [self.email_TF resignFirstResponder];
    [self.mobileNumber_TF resignFirstResponder];
}
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //[self.navigationController setNavigationBarHidden:YES animated:YES];   //it hides
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //Animation
        
        [UIView animateWithDuration:0 animations:^{
            
            self.profile_Image.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
        }completion:^(BOOL finished){
            
            // Finished scaling down imageview, now resize it
            
            [UIView animateWithDuration:0.4 animations:^{
                
                self.profile_Image.transform = CGAffineTransformIdentity;
                
                self.profile_Image.layer.cornerRadius = self.profile_Image.frame.size.height/2.0;
                self.profile_Image.clipsToBounds = YES;
                
            }completion:^(BOOL finished){
                
                
            }];
            
            
        }];
        
    });
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)changePwd_btnAction:(id)sender {
    
    /*
     Old Password
     New Password
Please Enter Old and New Password
     */
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@""
                                                                              message:[[SharedClass sharedInstance]languageSelectedString:@"Change Password"]
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
       [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
       // textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Old Password"];
        textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"Old Password"];
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.secureTextEntry = YES;
        
    }];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = [[SharedClass sharedInstance]languageSelectedString:@"New Password"];
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.secureTextEntry = YES;
    }];
     alertController.view.tintColor = [UIColor colorWithRed:106.0/255.0 green:189.0/255.0 blue:239.0/255.0 alpha:1];
    [alertController addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Save"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSArray * textfields = alertController.textFields;
        
        nameField = textfields[0];
        passwordField = textfields[1];
        NSLog(@"%@",nameField.text);
        NSLog(@"%@",passwordField.text);
        
        if (nameField.text.length>0&&passwordField.text.length>0) {
            
//            BOOL checkNetwork = [[SharedClass sharedInstance]connected];
//            if(checkNetwork == false){
//
//                NSLog(@"unreachable");
//
//                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Check Your Network Connection"] OnViewController:self completion:nil];
//                //self.scrollView.hidden = YES;
//            }
//            else{
//                NSLog(@"reachable");
//                [self forChangePassword];
//            }
            [self forChangePassword];
        }
        else{

            NSLog(@"ghhgh");
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedString:@"Please Enter Old and New Password"] OnViewController:self completion:nil];
        }
    }]];

    alertController.view.tintColor = [UIColor colorWithRed:106.0/255.0 green:189.0/255.0 blue:239.0/255.0 alpha:1];
    [self presentViewController:alertController animated:YES completion:nil];

}

- (IBAction)save_btnAction:(id)sender {
    
    
    [self profileUpdate];
    
}


//change Password
-(void)forChangePassword{
    
    //http://volive.in/engineering/user_services/change_password
    //lang, user_id, new_password, old_password
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
    [valuesDictionary setObject:nameField.text forKey:@"old_password"];
    [valuesDictionary setObject:passwordField.text forKey:@"new_password"];
    
    [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
    
    //[valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"language"] forKey:@"language"];
    
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];

    if ([str isEqualToString:@"2"]) {
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
    } else{
        [valuesDictionary setObject:@"en" forKey:@"lang"];

    }
    
    [[SharedClass sharedInstance]postResponseFromServerWithMethod:@"change_password" Dictionary:(NSDictionary*)valuesDictionary onViewController:self :^(NSData *data){
        
        NSDictionary *jsonDict = [[NSDictionary alloc]initWithDictionary:(NSDictionary*)data];
        NSLog(@"json dict %@",jsonDict);
        
        NSString *status = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"status"]];
        NSLog(@"sta %@",status);
        
        if([status isEqualToString:@"1"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:^{ }];
               // [[SharedClass sharedInstance]showProgressForSuccess:[jsonDict objectForKey:@"message"]];
            
            });
            
            
        } else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Alert"] Message:[jsonDict objectForKey:@"message"] OnViewController:self completion:^{ }];
                
                [SVProgressHUD dismiss];
                
            });
            
        }
        
    }];
    
    
}


-(void)profileUpdate{
    
    //http://volive.in/engineering/user_services/update_profile
    //lang, user_id, phone_number, email, name, file
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc]init];
    
  //  if (self.pickin_img.image!=nil || self.name_TF.text.length>0 || self.email_TF.text.length>0 || self.mobileNumber_TF.text.length>0 ) {
        
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
        [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedString:@"Sending......"]];
        
    NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentLanguage"]];
    
    if ([str isEqualToString:@"2"]) {
        
        [valuesDictionary setObject:@"ar" forKey:@"lang"];
        
    } else{
        
        [valuesDictionary setObject:@"en" forKey:@"lang"];
        
    }
        [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"user_id"] forKey:@"user_id"];
        [valuesDictionary setObject:self.name_TF.text forKey:@"name"];
        
        
        //[valuesDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"image"] forKey:@"user_image"];
        [valuesDictionary setObject:self.mobileNumber_TF.text forKey:@"phone_number"];
        [valuesDictionary setObject:self.email_TF.text forKey:@"email"];
        //[valuesDictionary setObject:@"en" forKey:@"lang"];
        [valuesDictionary setObject:@"963254" forKey:@"API-KEY"];
    
        //[valuesDictionary setObject:self.pickin_img forKey:@"file"];
    
        self.urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://volive.in/aous/user_services/update_profile"]];
        
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        NSString* FileParamConstant = @"file";
        
        
        [self.urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [self.urlRequest setHTTPShouldHandleCookies:NO];
        [self.urlRequest setTimeoutInterval:30];
        [self.urlRequest setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [self.urlRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in valuesDictionary) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [valuesDictionary objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            [self.urlRequest setHTTPBody:body];
        }
        
        // add image data
        NSData *imageData = UIImageJPEGRepresentation(self.profile_Image.image,1.0);
        if (imageData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [self.urlRequest setHTTPBody:body];
        }
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [self.urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        [[session dataTaskWithRequest:_urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            NSDictionary *statusDict = [[NSDictionary alloc]init];
            statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"Images from server %@", statusDict);
            
            NSString* status=[statusDict objectForKey:@"status"];
            int result=[status intValue];
            [SVProgressHUD dismiss];
            
            if (result==1) {
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * view=   [UIAlertController
                                                 alertControllerWithTitle:[statusDict objectForKey:@"message"]
                                                 message:nil
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* OkAction = [UIAlertAction
                                               actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"]
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   //Do some thing here
                                                   
                                                   
                                                   //[[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[statusDict objectForKey:@"image"]]forKey:@"image"];
                                                   //[[NSUserDefaults standardUserDefaults]setObject:self.email_TF.text forKey:@"email"];
                                                   
                                                   [[NSUserDefaults standardUserDefaults]setObject:[[statusDict objectForKey:@"data"] objectForKey:@"username"] forKey:@"username"];
                                                   
                                                   [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%s%@",basePath,[[statusDict objectForKey:@"data"] objectForKey:@"image"]]forKey:@"image"];
                                                   [[NSUserDefaults standardUserDefaults]setObject:[[statusDict objectForKey:@"data"] objectForKey:@"phone_number"] forKey:@"phone_number"];
                                                   [[NSUserDefaults standardUserDefaults]setObject:[[statusDict objectForKey:@"data"] objectForKey:@"email"] forKey:@"email"];
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       
                                                       //[self.profile_Image sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"image"]]];
                                                       [self.profile_Image sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"profile"]];
                                                   });
                                                   
                                                   [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
                                                   
                                                   NSLog(@"statusDict %@",statusDict);
                                                   
                                               }];
                    [view addAction:OkAction];
                    //[view addAction:camera];
                    [self presentViewController:view animated:YES completion:nil];
                    
                    
                });
                
            }
            else
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * view=   [UIAlertController
                                                 alertControllerWithTitle:[statusDict objectForKey:@"message"]
                                                 message:nil
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* OkAction = [UIAlertAction
                                               actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"]
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   
                                               }];
                    
                    [view addAction:OkAction];
                    //[view addAction:camera];
                    [self presentViewController:view animated:YES completion:nil];
                    
                    
                    
                });
                
                
            }
            
        }] resume];
        //}
        //    else{
        //        UIAlertController *alertController2 = [UIAlertController  alertControllerWithTitle:@"Enter the Fields"  message:nil  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        [alertController2 addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        //
        //            //[self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];
        //
        //
        //
        //        }]];
        //        [self presentViewController:alertController2 animated:YES completion:nil];
    //}
    
    
}




- (IBAction)edit_btnAction:(id)sender {
    
    UIAlertController * view =   [UIAlertController
                                  alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Pick The Image"]
                                  message:[[SharedClass sharedInstance]languageSelectedString:@"From"]
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* PhotoLibrary = [UIAlertAction
                                   actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Photo Library"]
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //Do some thing here
                                       picker_Profile_Pic = [[UIImagePickerController alloc] init];
                                       picker_Profile_Pic.delegate = self;
                                       [picker_Profile_Pic setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                       [self presentViewController:picker_Profile_Pic
                                                          animated:YES completion:NULL];
                                       
                                       [view dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
    UIAlertAction* camera = [UIAlertAction
                             actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Camera"]
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 picker_Profile_Pic = [[UIImagePickerController alloc] init];
                                 picker_Profile_Pic.delegate = self;
                                 [picker_Profile_Pic setSourceType:UIImagePickerControllerSourceTypeCamera];
                                 [self presentViewController:picker_Profile_Pic
                                                    animated:YES completion:NULL];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"Cancel"]
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [view addAction:PhotoLibrary];
    [view addAction:camera];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];

}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    
//    BOOL checkNetwork = [[SharedClass sharedInstance]connected];
//    if(checkNetwork == false){
//        
//        NSLog(@"unreachable");
//        
//        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert"] Message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please Check Your Network Connection"] OnViewController:self completion:nil];
//        
//    }
   // else{
        NSLog(@"reachable");
        
        if (picker == picker_Profile_Pic)
        {
            self.profile_Image.image=image;
            self.pickin_img.image=image;
            
            [self profileUpdate];
            
        }
        else{
            self.profile_Image.image=nil;
            self.pickin_img.image=nil;
            
        }
        
        [picker dismissViewControllerAnimated:YES completion:nil];
   // }
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}


/*-(void)ProfilePicUpdate{
    
    //http://voliveafrica.com/beyond/api/Mobile_Services/user_profile_image
    
    if (self.pickin_img.image!=nil) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
        [SVProgressHUD showWithStatus:@"Sending......"];
        
        NSDictionary *params =  @{@"uid":[[NSUserDefaults standardUserDefaults]objectForKey:@"uid"],};
        
        //self.urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://voliveafrica.com/beyond/api/Mobile_Services/user_profile_image"]];
        self.urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://beyond-ksa.com/api/Mobile_Services/user_profile_image"]];
        
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        NSString* FileParamConstant = @"user_image";
        
        [self.urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [self.urlRequest setHTTPShouldHandleCookies:NO];
        [self.urlRequest setTimeoutInterval:30];
        [self.urlRequest setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [self.urlRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            [self.urlRequest setHTTPBody:body];
        }
        
        // add image data
        NSData *imageData = UIImageJPEGRepresentation(self.pickin_img.image,1.0);
        //NSData *imageData = UIImageJPEGRepresentation(self.profile_imageview.image,1.0);
        if (imageData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [self.urlRequest setHTTPBody:body];
        }
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [self.urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        [[session dataTaskWithRequest:_urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            NSDictionary *statusDict = [[NSDictionary alloc]init];
            statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"Images from server %@", statusDict);
            
            NSString* status=[statusDict objectForKey:@"status"];
            
            //int result=[status intValue];
            [SVProgressHUD dismiss];
            
            if ([status boolValue]==true) {
                
                UIAlertController * view=   [UIAlertController
                                             alertControllerWithTitle:[statusDict objectForKey:[[SharedClass sharedInstance]languageSelectedString:@"message"]]
                                             message:nil
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* OkAction = [UIAlertAction
                                           actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"]
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action)
                                           {
                                               
                                           }];
                NSString *pathStr = [NSString stringWithFormat:@"%@%@",[statusDict objectForKey:@"path"],[statusDict objectForKey:@"image"]];
                
                [[NSUserDefaults standardUserDefaults] setObject:pathStr forKey:@"image"];
                
                [view addAction:OkAction];
                //[view addAction:camera];
                [self presentViewController:view animated:YES completion:nil];
            }
            else
            {
                UIAlertController * view=   [UIAlertController
                                             alertControllerWithTitle:[statusDict objectForKey:[[SharedClass sharedInstance]languageSelectedString:@"message"]]
                                             message:nil
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* OkAction = [UIAlertAction
                                           actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"]
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action)
                                           {
                                           }];
                
                [view addAction:OkAction];
                //[view addAction:camera];
                [self presentViewController:view animated:YES completion:nil];
                
            }
            
        }] resume];
    }
    else{
        UIAlertController *alertController2 = [UIAlertController  alertControllerWithTitle:@"Enter the Fields"  message:nil  preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController2 addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedString:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
        }]];
        [self presentViewController:alertController2 animated:YES completion:nil];
    }
}
*/




@end
