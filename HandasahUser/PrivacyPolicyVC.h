//
//  PrivacyPolicyVC.h
//  HandasahUser
//
//  Created by MuraliKrishna on 11/07/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyPolicyVC : UIViewController


@property (weak, nonatomic) IBOutlet UIBarButtonItem *back_btn;
- (IBAction)back_action:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *privacy_TV;

@end
