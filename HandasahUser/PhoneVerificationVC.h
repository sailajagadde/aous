//
//  PhoneVerificationVC.h
//  HandasahUser
//
//  Created by Apple on 5/12/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneVerificationVC : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *phoneVerification_label;
@property (weak, nonatomic) IBOutlet UILabel *pleaseEnterOtp_label;


@property (weak, nonatomic) IBOutlet UITextField *countryCode_TF;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber_TF;
@property (weak, nonatomic) IBOutlet UITextField *enterPin_TF;

@property (weak, nonatomic) IBOutlet UITextField *otpOne_TF;
@property (weak, nonatomic) IBOutlet UITextField *otpTwo_TF;
@property (weak, nonatomic) IBOutlet UITextField *otpThree_TF;
@property (weak, nonatomic) IBOutlet UITextField *otpFour_TF;


- (IBAction)submit_btnAction:(id)sender;



@property (weak, nonatomic) IBOutlet UIButton *resent_btn;

@property (weak, nonatomic) IBOutlet UILabel *receiveSms_label;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_view;

@end
