//
//  PaymentVC.h
//  HandasahUser
//
//  Created by Suman Guntuka on 15/05/18.
//  Copyright © 2018 volivesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentVC : UIViewController
- (IBAction)btn_Payment:(id)sender;
- (IBAction)btn_Close:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UILabel *yourPayableStaticLabel;

@property (weak, nonatomic) IBOutlet UILabel *cardNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *cardName_TF;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *cardNumber_TF;

@property (weak, nonatomic) IBOutlet UILabel *cardExpiryStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *cvvStaticLabel;

@property (weak, nonatomic) IBOutlet UILabel *secureLabel;
@property (weak, nonatomic) IBOutlet UIButton *payNowBtn;

@property (weak, nonatomic) IBOutlet UITextField *promocode_TF;
- (IBAction)promocodeBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;


@property NSString *costStr;
@property NSString *reqIdStr;


@end
